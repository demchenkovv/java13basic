create table booking_n
(
    id             INT,
    name           VARCHAR(50),
    surname        VARCHAR(50),
    departure_from VARCHAR(50),
    arriving_to    VARCHAR(50),
    price          INT NOT NULL
);

select max(price)
from booking_n;

select min(price)
from booking_n;

-- найти самые дешевые билеты
select departure_from, arriving_to, min(price)
from booking_n
group by departure_from, arriving_to
order by min(price);

-- найти среднюю сумму бронирования
select avg(price)
from booking_n;

-- сумма всех бронирований
select sum(price)
from booking_n;

-- найти самые дорогие билеты
select departure_from, max(price)
from booking_n
group by departure_from
order by max(price) DESC;

-- узнать точное время
select now();

-- узнать дату
select now()::date;

-- дата 10 лет назад
select now() - interval '10 years';

-- дата через 25 дней
select now() + interval '25 days';

-- команда extract позволяет вытащить нужную информацию из даты
select extract(year from now());
select extract(month from now());

-- узнать сколько лет студентам из таблицы student_auto
select name, surname, age(now(), birthday) as age
from student_auto;

-- Команда UPDATE имя_таблицы SET имя_столбца = выражение WHERE логическое_утверждение
-- позволит вносить изменения в уже существующие записи внутри таблицы данных.
select * from booking_n where id = 5;
update booking_n set name = 'Murik' where id = 5;
update booking_n set departure_from = 'Peru', arriving_to = 'Japan'
where id = 5;

-- Команда ON CONFLICT(имя_столбца) DO команда
-- позволяет задать действие, заменяющее возникновение ошибки при нарушении ограничения
-- уникальности или ограничения-исключения. Эта команда сработает только в том случае,
-- если у столбца есть правило ограничения.
INSERT INTO booking_n (id,name,surname,departure_from,arriving_to,price)
VALUES (1,'John','Doe','USA','UK','200') ON CONFLICT (id) DO NOTHING;

INSERT INTO booking_n (id,name,surname,departure_from,arriving_to,price)
VALUES (1,'John','Doe','USA','UK','200') ON CONFLICT (id) DO UPDATE SET departure_from = 'EXCLUDED.departure_from';


