create table student_auto
(
    id       INT,
    name     VARCHAR(50),
    surname  VARCHAR(50),
    gender   VARCHAR(50),
    mobile   VARCHAR(50),
    birthday DATE
);

-- Команда OFFSET [целое_число] пропускает количество записей в таблице с начала, равное значению числа.
-- Команда LIMIT [целое_число] ограничивает количество выводимых строк до значения числа
select id, surname
from student_auto
offset 100 limit 50;

-- Команда FETCH FIRST [целое_число] ROW ONLY показывает только количество первых строк, равное значению числа
select id, surname
from student_auto
offset 100 fetch first 50 row only;

-- Команда ORDER BY [имя_столбца]  показывает выборку, отсортированную по выбранному столбцу.
-- С помощью приписки ASC или DESC можно расположить выборку в возрастающем или убывающем порядке соответственно.
select *
from student_auto
order by name DESC;

-- distinct - убирает дубликаты (показывает уникальные значения)
select distinct birthday
from student_auto
order by birthday asc;

-- Команда WHERE логическое_выражение сравнивает значения в строках таблицы с выбранной логикой.
-- В зависимости от верности или не верности логического выражения, команда включит строку в выборку.
select *
from student_auto
where gender = 'Female'
  and name = 'Cosmo';

