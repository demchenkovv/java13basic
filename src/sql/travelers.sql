create table travelers
(
    id      INT,
    name    VARCHAR(50),
    surname VARCHAR(50),
    email   VARCHAR(50),
    gender  VARCHAR(50),
    country VARCHAR(50)
);

-- найти конкретные страны
select *
from travelers
where country IN ('Russia', 'Kongo', 'Israel');

-- страны, начинающиеся на R
select *
from travelers
where country LIKE ('R%');

-- страны, начинающиеся с букв A и D
select *
from travelers
where country BETWEEN 'A' and 'D';

-- email, заканчивающиеся на edu
select *
from travelers
where email LIKE ('%.edu');

-- Два условия
-- email, заканчивающиеся на edu и страны, начинающиеся с букв A и D
select *
from travelers
where (email LIKE ('%.edu'))
  and (country between 'A' and 'D');

-- посчитать количество одинаковых значений в столбце name count(*)
-- group by name - группировка по имени
-- order by name ASC - сортировка в лексикографическом порядке
select name, count(*)
from travelers
group by name
order by name ASC;

-- Вывести все страны, которые встречаются более 15 раз
-- Отсортировать их в порядке начиная с наибольшего значения
-- Команда HAVING очень похожа на команду WHERE.
-- Её основное отличие в том, что она применяется сразу к строкам, сгруппированным командой GROUP BY().
select country, count(*)
from travelers
group by country
HAVING count(*) > 15
ORDER BY count(*) DESC;

-- Команда ALTER TABLE [имя_таблицы] ADD/DROP  [имя_столбца] позволит внести изменения в таблицу.
-- ADD и DROP добавляют и удаляют столбец соответственно.
ALTER TABLE travelers ADD login VARCHAR(25);
ALTER TABLE travelers DROP login;

-- Команда ALTER TABLE имя_таблицы DROP CONSTRAINT (имя_ограничителя)
-- поможет избавиться от ограничений, наложенных на таблицу
-- ALTER TABLE booking_n DROP CONSTRAINT booking_n_pkey;


-- Команда ALTER TABLE имя_таблицы ADD CONSTRAINT имя_ограничения UNIQUE(имя_столбца)
-- создаст ограничение, согласно которому в таблицу смогут быть добавлены только записи
-- с уникальными значениями внутри указанного столбца.
ALTER TABLE travelers ADD CONSTRAINT id_unique UNIQUE(id);

-- ADD CONSTRAINT CHECK
-- Команда ALTER TABLE имя_таблицы ADD CONSTRAINT имя_ограничения CHECK(логическое_выражение)
-- создаст ограничение, согласно которому внутри столбцов смогут быть только определенные значения.
ALTER TABLE travelers ADD CONSTRAINT gender_check CHECK(gender = 'Female' OR gender = 'Male');

SELECT * from travelers where id < 50;

SELECT EXTRACT (YEAR from now());
