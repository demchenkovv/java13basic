-- Методы для работы со строками (Character)

-- s - строка, n - конечная длина текста, p - текст для заполнения
-- trimstring - текст, который надо срезать

-- Все буквы строчные
select *
from customers
where lower(customer_name) like '%ов';
-- Все буквы заглавные
select upper(customer_name)
from customers;

-- initcap(s) каждое слово с большой буквы
select initcap('приВет, каК деЛа, все ХОРОШО');

select customer_name
from customers
where initcap(customer_name) like 'Сем%';

-- concat(s) объединение строк
select concat('hello, ', 'world');
select concat(55, ' world');
select concat('today is ', now());

-- length(s)возвращает длину каждого значения, включая пробелы
select length(customer_name)
from customers;
-- сортировка по длине значения в порядке возрастания
select customer_name
from customers
order by length(customer_name);

-- lpad(s,n,p) и rpad(s,n,p) : строка/длина/чем заполнить слева l/справа r
select lpad('Заур', 7, '#');
select rpad('Заур', 7, '#');

select now(),
       rpad(customer_name, 20, '#'),
       lpad(phone_number, 14, 'ru')
from customers;

select rpad(phone_number, 20, '.') ||
       lpad(customer_name, 10, '.') as full_info
from customers;

-- удаление ОДНОГО символа (trailing - после слова, leading - до слова, both - с начала и с конца текста)
-- trimstring - текст, который надо срезать, from s - откуда надо срезать текст
-- trim({trailing, leading, both} trimstring from s)
select trim(trailing 'w' from 'wwZaurwwww');
select trim(leading '*' from '*****Zaur**');
select trim(both '*' from '*****Zaur**');
select trim('*' from '*****Zaur**'); -- экв. both
select trim(' ' from '    Zaur    Tregulov    '); -- удаление пробелов с начала и с конца
select trim('    Zaur    Tregulov    '); -- удаление пробелов с начала и с конца
select trim(both '7' from '7775857');

-- s - строка, текст;
-- search position - искомый текст; start position - позиция для начала работы;
-- Nth occurrence - N-ое появление; number of characters - количество символов;
-- search item - искомый элемент; replace item - заменяющий элемент.

-- instr(str, ch, instr(str, sub), 1);                               --Oracle
-- strpos(substr(str, strpos(str, sub)), ch) + strpos(str, sub) - 1; --Postgres

