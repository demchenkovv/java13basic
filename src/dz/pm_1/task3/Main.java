package dz.pm_1.task3;

import java.io.*;
import java.util.Scanner;

/**
 * Метод main
 */
public class Main {
    private static final String PKG_DIRECTORY = "\\Users\\HOME\\IdeaProjects\\Java13Basic\\src\\dz\\pm_1\\task3";
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) {
        overwriteFile(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
    }

    public static void overwriteFile(File file) {
        if (file.length() == 0) {
            return;
        }
        try (
                Scanner input = new Scanner(file);
                Writer wr = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME)) {
            while (input.hasNextLine()) {
                wr.write(input.nextLine().toUpperCase() + "\n");
            }
            System.out.println("Overwrite successful!");
        } catch (IOException ex) {
            System.out.println("Main#main!error: " + ex.getMessage());
        }
    }
}
