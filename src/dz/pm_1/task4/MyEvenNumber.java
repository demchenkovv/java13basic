package dz.pm_1.task4;

import java.util.InputMismatchException;

public class MyEvenNumber {
    private int number;

    public MyEvenNumber(int number) {
        if (isEvenNumber(number)) {
            this.number = number;
        } else {
            throw new IllegalArgumentException("Число " + number + " нечетное!");
        }
    }

    public int getNumber() {
        return number;
    }

    public static boolean isEvenNumber(int value) {
        if (value == 0) {
            throw new IllegalArgumentException("Число 0 является четным числом.");
        }
        return value % 2 == 0;
    }
}
