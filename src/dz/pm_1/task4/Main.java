package dz.pm_1.task4;

public class Main {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        // Четное число - объект создан
        MyEvenNumber myEvenNumber1 = new MyEvenNumber(6);
        System.out.println(myEvenNumber1.getNumber());
        // Нечетное число - выкидывает исключение
        MyEvenNumber myEvenNumber2 = new MyEvenNumber(1);
        System.out.println(myEvenNumber2.getNumber());
    }
}
