package dz.pm_1.task6;

public enum Gender {
    MALE("MALE"),
    FEMALE("FEMALE");

    private String gender;

    Gender() {
    }

    Gender(String gender) {
        this.gender = gender;
    }
}
