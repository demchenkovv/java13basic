package dz.pm_1.task6;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;

/*
Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
очень зол и ему придется написать свои проверки, а также кидать исключения,
если проверка провалилась. Помогите Пете написать класс FormValidator со
СТАТИЧЕСКИМИ методами проверки. На вход всем методам подается String str.
a. public void checkName(String str) — длина имени должна быть от 2 до 20
символов, первая буква заглавная.
b. public void checkBirthdate(String str) — дата рождения должна быть не
раньше 01.01.1900 и не позже текущей даты.
c. public void checkGender(String str) — пол должен корректно матчится в
enum Gender, хранящий Male и Female значения.
d. public void checkHeight(String str) — рост должен быть положительным
числом и корректно конвертироваться в double.
 */
public class FormValidator {

    /**
     * Метод main
     */
    public static void main(String[] args) {

        try {
            checkName("Тим");
            checkBirthdate("08.06.1955");
            checkGender("Male");
            checkHeight("1.87");
        } catch (InputMismatchException | IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void checkName(String str) {
        if (str.matches("^[A-ZА-Я][a-zа-я]{1,18}")) {
            System.out.println("checkName \"" + str + "\": validation is successful");
        } else
            throw new InputMismatchException("FormValidator#checkName!error: \"" + str + "\" invalid format entered.");
    }

    public static void checkBirthdate(String str) {
        if (!(str.matches("^\\d{2}\\.\\d{2}\\.\\d{4}$"))) {
            throw new IllegalArgumentException("FormValidator#checkBirthdate!error: \"" + str + "\" invalid format entered.");
        }

        String[] strDate = str.split("\\.");
        int[] numDate = new int[3];
        for (int i = 0; i < 3; i++) {
            numDate[i] = Integer.parseInt(strDate[i]);
        }

        Calendar birthday = new GregorianCalendar(numDate[2], (numDate[1] - 1), numDate[0]);
        Calendar minDate = new GregorianCalendar(1900, 00, 01);
        Calendar maxDate = new GregorianCalendar();

        if (birthday.before(minDate) || birthday.after(maxDate)) {
            throw new IllegalArgumentException("FormValidator#checkBirthdate!error: invalid date of birth.");
        } else {
            System.out.println("checkBirthdate \"" + str + "\": validation is successful");
        }
    }

    public static void checkGender(String str) {
        Gender gender = null;
        try {
            gender = Gender.valueOf(str.toUpperCase());
            System.out.println("checkBirthdate \"" + str + "\": validation is successful");
        } catch (IllegalArgumentException ex) {
            System.out.println("FormValidator#checkGender!error: invalid gender.");
        }
    }

    public static void checkHeight(String str) {
        try {
            double height = Double.parseDouble(str);
            if (height > 0) {
                System.out.println("checkHeight \"" + str + "\": validation is successful");
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("FormValidator#checkHeight!error: invalid height.");
        }
    }
}
