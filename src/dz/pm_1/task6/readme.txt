    /**
     * вложенный static класс Builder, который
     * позволяет обеспечить конструирование объекта (new FormValidator)
     */
    public static Builder builder() {
        return new Builder();
    }
    public static class Builder {
        private FormValidator obj = new FormValidator();

        public Builder setName(String name) {
            obj.name = name;
            return this;
        }

        public Builder setBirthday(String birthday){
            obj.birthdate = birthday;
            return this;
        }


        public Builder setGender(String gender){
            obj.gender = gender;
            return this;
        }


        public Builder setHeight(String height){
            obj.height = height;
            return this;
        }

// допилить
        public FormValidator build() {
            if (obj.name == null || obj.birthdate == null) {
                throw new IllegalArgumentException("Name or birthday is null.");
            }
            return obj;
        }
    }