package dz.pm_1.task2;

public class MyUncheckedException
        extends RuntimeException {

    public MyUncheckedException() {
        super("условие некорректно.");
    }

    public MyUncheckedException(String message) {
        super(message + " номер индекса некорректен.");
    }
}
