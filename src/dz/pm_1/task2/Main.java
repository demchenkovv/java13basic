package dz.pm_1.task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int[] arr = new int[5];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        for (int i : arr) {
            System.out.print(i + " ");
        }

        try {
            getValueByIndex(arr);
        } catch (MyUncheckedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void getValueByIndex(int[] arr) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("\nВведите номер индекса: ");
            int temp = input.nextInt();
            System.out.print("Число под индексом " + temp + " равно " + arr[temp]);

        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MyUncheckedException("Main#getValueByIndex!error:");
        }
    }
}
