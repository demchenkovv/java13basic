package dz.pm_1.task1;

import java.io.File;
import java.util.Scanner;

/**
 * Метод main
 */
public class Main {
    public static void main(String[] args) {

        File file = new File("\\Users\\HOME\\IdeaProjects\\Java13Basic\\src\\dz\\pm_1\\task1\\input.txt");
        try {
            readFromFile(file);
        } catch (MyCheckedException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static void readFromFile(File file)
            throws MyCheckedException {

        Scanner input = null;
        try {
            input = new Scanner(file);
            while (input.hasNext()) {
                System.out.println(input.nextLine());
            }
            System.out.println("\nРабота программы завершена успешно.");
        } catch (Exception e) {
            throw new MyCheckedException("Main#main!error:");
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }
}
