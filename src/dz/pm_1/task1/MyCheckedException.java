package dz.pm_1.task1;

public class MyCheckedException
        extends Exception {

    public MyCheckedException() {
        super("файл не найден.");
    }
    public MyCheckedException (String message) {
        super(message + " файл не найден.");
    }
}
