package dz.pm_2.task2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String str1 = "клоака";
        String str2 = "околка";

        System.out.println(isAnagram(str1, str2));

//        System.out.println(isAnagram(s, t));
    }

    // Через Set НЕ рабочий вариант
//    private static boolean isAnagram(String s, String t) {
//
//        if (s.length() != t.length()) {
//            return false;
//        }
//
//        Set<Character> setFirst = new LinkedHashSet<>();
//        for (char ch : s.toCharArray()) {
//            setFirst.add(ch);
//        }
//
//        Set<Character> setSecond = new LinkedHashSet<>();
//        for (char ch : t.toCharArray()) {
//            setSecond.add(ch);
//        }
//
//        return setFirst.equals(setSecond);
//    }

    // Через Map рабочий вариант
    // От Ясеницкий Степан всем 07:49 PM
    // когда в Map добавляешь пару ключ-значение, а такой ключ
    // в этой Map уже есть, то старое значение этого ключа в Map
    // меняется на новое. А общее кол-во пар остается прежним.

    // клоака - околка


    private static boolean isAnagram(String str1, String str2) {

        if (str1.length() != str2.length()) {
            return false;
        }

        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char element : str1.toLowerCase().toCharArray()) {
            s1.put(element, s1.getOrDefault(element, 0) + 1);
        }
        System.out.println(s1.entrySet());

        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char element : str2.toLowerCase().toCharArray()) {
            s2.put(element, s2.getOrDefault(element, 0) + 1);
        }
        System.out.println(s2);

        return s1.equals(s2);
    }
}
