package dz.pm_2.task3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();

        Set<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(0, 1, 2, 4));

        System.out.println("intersection: " + powerfulSet.intersection(set1, set2));
        System.out.println("union: " + powerfulSet.union(set1, set2));
        System.out.println("relativeComplement: " + powerfulSet.relativeComplement(set1, set2));
    }
}
