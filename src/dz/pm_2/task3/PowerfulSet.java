package dz.pm_2.task3;

import java.util.*;

public class PowerfulSet {
/** возвращает пересечение двух наборов. */
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> intersectionSet = new HashSet<>(set1);
        intersectionSet.retainAll(set2);
        return intersectionSet;
    }

/** возвращает объединение двух наборов */
    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> union = new HashSet<>(set1);
        union.addAll(set2);
        return union;
    }

/** возвращает элементы первого набора без тех, которые находятся также и во втором наборе. */
    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> relativeComplementSet = new HashSet<>(set1);
        relativeComplementSet.removeAll(set2);
        return relativeComplementSet;
    }
}
