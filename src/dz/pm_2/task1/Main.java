package dz.pm_2.task1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> intList = new ArrayList<>();
        intList.add(5);
        intList.add(10);
        intList.add(20);
        intList.add(10);
        System.out.println(fromArrayListToSet(intList));


        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("I");
        stringList.add("Love");
        stringList.add("Java");
        stringList.add("Love");
        System.out.println(fromArrayListToSet(stringList));
    }

    private static <T> Set<T> fromArrayListToSet(ArrayList<T> myList) {
        return new TreeSet<>(myList);
//        Set<T> set = new HashSet<>(myList);
//        return set;
    }
}

