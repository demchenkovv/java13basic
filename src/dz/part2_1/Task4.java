package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
Например
6
7 7 7 10 26 26
->
3 7
1 10
2 26

2
-5 7
->
1 -5
1 7
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        int n = input.nextInt(); // длина массива
        int[] arr = new int[n]; // массив с длиной n

        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

        int number = arr[0];
        int sum = 1;
        for (int i = 1; i < n; i++) {
            if (number == arr[i]) {
                sum++;
            } else {
                System.out.println(sum + " " + number);
                number = arr[i];
                sum = 1;
            }
        }
        System.out.println(sum + " " + number);
    }
}
