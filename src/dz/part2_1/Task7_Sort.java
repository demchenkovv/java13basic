package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
6
-10 -5 1 3 3 8 --> 1 9 9 25 64 100

2
-7 7 --> 49 49
 */

import java.util.Arrays;
import java.util.Scanner;

public class Task7_Sort {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

        int[] m = new int[n];
        for (int i = 0; i < n; i++) {
            m[i] = (int) Math.pow(arr[i], 2);
        }
        Arrays.sort(m);

        for (int e : m) {
            System.out.print(e + " ");
        }
    }
}

