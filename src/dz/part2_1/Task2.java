package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
Пример:
Входные данные
7
1 2 3 4 5 6 7
7
1 2 3 4 5 6 7
Выходные данные
true
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean isTrue = true;

        System.out.print("Длина массива arr1: ");
        int n = input.nextInt(); // длина первого массива
        int[] arr1 = new int[n]; // создан массив длинной n
        System.out.print("Введите значения в массив arr1: ");
        for (int i = 0; i < n; i++) {
            arr1[i] = input.nextInt();
        }

        System.out.print("Длина массива arr2: ");
        int m = input.nextInt(); // длина второго массива
        int[] arr2 = new int[m]; // создан массив длинной m
        System.out.print("Введите значения в массив arr2: ");
        for (int i = 0; i < m; i++) {
            arr2[i] = input.nextInt();
        }

        // Проверка условия равенства длинн
        if (arr1.length == arr2.length) {
            // если длинна массива arr1 равна длинне массива arr2,
            // запускается цикл for для проверки равенства значений элементов
            for (int i = 0; i < n; i++) {
                if (arr1[i] != arr2[i]) {
                    isTrue = false;
                    break;
                }
            }
        } else {
            isTrue = false;
        }
        // Выводим полученный результат true или false
        System.out.println(isTrue);
    }
}
