package dz.part2_1;

import java.util.Scanner;

// посмотри пятую неделю практики - таск 7
/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
4
hello
java
hi
java
--------> java
7
today
is
the
most
most
special
day
---------> most
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        String[] words = new String[n];
        for (int i = 0; i < n; i++) {
            words[i] = input.next();
        }
        String temp = null;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j)
                    continue;
                if (words[i].equals(words[j])) {
                    temp = words[i];
                    break;
                }
            }
        }
        System.out.println(temp);
    }
}
