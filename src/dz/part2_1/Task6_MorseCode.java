package dz.part2_1;
/*
(1 балл)
На вход подается строка S, состоящая только из русских заглавных
букв (без Ё).
Необходимо реализовать метод, который кодирует переданную строку с
помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
нужно пробелом.
ПРИВЕТ -> .--. .-. .. .-- . -
УРА -> ..- .-. .-
 */

import java.util.Scanner;

public class Task6_MorseCode {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String s = input.next().toUpperCase(); // условие заглавных букв
        String[] morseCode = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-",
                ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.",
                "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        String alphabet = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"; // алфавит без буквы Ё

        for (int i = 0; i < s.length(); i++) {

            // Логика такая: циклом for проходим по каждому символу введенного слова (s) с 0 до s.length()
            // i == 0 - первая буква. Находим индекс первого вхождения этой буквы в алфавите (alphabet)
            // Индекс буквы в алфавите соответствует индексу коду Морзе, соответственно, на выходе код морзе
            // далее i инкрементируется и i == 1 - вторая буква. И так далее по всей длине строки.
            System.out.print(morseCode[alphabet.indexOf(s.charAt(i))] + " ");
        }
    }
}
