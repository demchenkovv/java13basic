package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
6
-10 9 -5 -6 1 -3
-4
======> -3
2
10 20
21
======> 20
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

//        int n = 6;
//        int[] arr = {-10, 9, -5, -6, 1, -3};
//        int m = -4;

        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        int m = input.nextInt();

        int num = arr[0];
        int temp = Math.abs(arr[0] - m); // 6
        for (int i = 1; i < n; i++) {
            if (Math.abs(arr[i] - m) <= temp)
                num = arr[i];
            // еще необходимо добавить условие на наибольшее число в равном отдалении (важно!)
            // темп не обновляется (бот пропустил, но решение неверное)
        }
        System.out.println(num);
    }
}
