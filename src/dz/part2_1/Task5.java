package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.
Вх. данные:         Вых. данные:
5                   -11 2 38 44 0
38 44 0 -11 2
2
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt(); // длина массива
        int[] arr = new int[n]; // заполнение массива
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        int m = input.nextInt(); // величина сдвига
        if (m > 0)
            m += 1; // у бота на 1 элемент сдвиг больше введённого

        int[] tempArr = new int[m]; // создали временный массив
        for (int i = 0; i < m; i++) { // скопировали значения с временного массива с 0 по m (длину сдвига)
            tempArr[i] = arr[i];
        }

        // Сдвинуть элементы влево на m
        for (int i = m; i < n; i++) {
            arr[i - m] = arr[i];
        }
        for (int i = 0, j = arr.length - m; i < m; i++, j++) {
            arr[j] = tempArr[i];
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
