package dz.part2_1;
/*
(1 балл)
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
Входные данные:
3
1.5 2.7 3.14
-> 2,44бббб....
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt(); // длина массива

        double[] arr = new double[n]; // заполнение массива
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextDouble();

        }
        System.out.print(findArithmeticMean(arr)); // передача массива arr методу findArithmeticMean
    }

    public static double findArithmeticMean (double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum / arr.length; // вычисление и возвращение значения среднего арифметического
    }
}