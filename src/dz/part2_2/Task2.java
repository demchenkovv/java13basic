package dz.part2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
7
1 2
3 4
---->
0 0 0 0 0 0 0
0 0 0 0 0 0 0
0 1 1 1 0 0 0
0 1 0 1 0 0 0
0 1 1 1 0 0 0
0 0 0 0 0 0 0
0 0 0 0 0 0 0

● X1 < X2
● Y1 < Y2
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int[][] arr = new int[n][n];

        // координаты ниже надо заполнить цифрой 1
        // координаты старт
        int x1 = input.nextInt(); // столбец
        int y1 = input.nextInt(); // строка

        // координаты финиш
        int x2 = input.nextInt(); // столбец
        int y2 = input.nextInt(); // строка

        for (int i = 0; i < arr.length; i++) {
             for (int j = 0; j < arr[i].length; j++) {
                // заполняем от начала и до конца координат единицами
                if (i >= y1 && i <= y2 && j >= x1 && j <= x2)
                    arr[i][j] = 1;
                // заполняем между этими координатами (внутри) нулями
                if (i > y1 && i < y2 && j > x1 && j < x2)
                    arr[i][j] = 0;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(arr[i][j]);
                else
                    System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}

