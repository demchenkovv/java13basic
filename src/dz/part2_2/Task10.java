package dz.part2_2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
12374 --> 4 7 3 2 1
 */
public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = 12374;
        String message = n + "";
        numbersFromRightToLeft(message);
    }

    public static String numbersFromRightToLeft(String message) {
        if (message.isEmpty()) {
            return message;
        }
        else {
            System.out.print(message.charAt(message.length() - 1) + " ");
            return numbersFromRightToLeft(message.substring(0, message.length() - 1));
        }
    }
}
