package dz.part2_2;
/*
Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.

На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.

4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
-------->
Дарья: Добряш, 9.0
Николай: Кнопка, 7.6
Иван: Жучка, 6.6
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = 4; // количество участников (строк)

        String[] nameMan = {
                "Иван",
                "Николай",
                "Анна",
                "Дарья"
        };

        String[] namePet = {
                "Жучка",
                "Кнопка",
                "Цезарь",
                "Добряш"
        };
        double[][] score = {
                {7, 6, 7},
                {8, 8, 7},
                {4, 5, 6},
                {9, 9, 9}
        };

        // массив со средними оценками
        double[] averageValue = new double[n];
        double sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < score[i].length; j++) {
                sum += score[i][j];
            }
            averageValue[i] = (int) (sum / 3 * 10) / 10.0;
            sum = 0;
        }

        double first = averageValue[0];
        int firstIndex = 0;
        double second = 0;
        int secondIndex = 0;
        int thirdIndex = 0;
        for (int i = 1; i < averageValue.length; i++) {
            if (averageValue[i] > first) {
                thirdIndex = secondIndex;
                second = first;
                secondIndex = firstIndex;
                first = averageValue[i];
                firstIndex = i;
            } else if (averageValue[i] < first && averageValue[i] > second) {
                thirdIndex = secondIndex;
                second = averageValue[i];
                secondIndex = i;
            } else {
                thirdIndex = i;
            }
        }
        int[] result = {firstIndex, secondIndex, thirdIndex};
        for (int i = 0; i < 3; i++) {
            System.out.print(nameMan[result[i]] + ": " + namePet[result[i]] + ", " + averageValue[result[i]] + "\n");
        }
    }
}
