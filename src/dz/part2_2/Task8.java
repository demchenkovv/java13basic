package dz.part2_2;
/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
12374 --> 17
 */
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int sum = 0;
        System.out.println(findsum(n, sum));
    }

    private static int findsum(int n, int sum) {
        sum += n % 10;
        if (n == 0)
            return sum;
        return findsum(n / 10, sum);
    }
}
