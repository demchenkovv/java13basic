package dz.part2_2;

import java.util.Scanner;

/*
Петя решил начать следить за своей фигурой. Но все существующие
приложения для подсчета калорий ему не понравились и он решил написать
свое. Петя хочет каждый день записывать сколько белков, жиров, углеводов и
калорий он съел, а в конце недели приложение должно его уведомлять,
вписался ли он в свою норму или нет.
На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
Пример:
882 595 1232 17500
116 85 76 2300
100 98 124 2500
182 70 154 2750
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 2500
---> Отлично
882 595 1232 17500
142 85 76 2300
100 93 124 2500
282 70 144 3350
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 3790
---> Нужно есть поменьше
 */
public class Task6 {
    public static final String OK = "Отлично";
    public static final String BADLY = "Нужно есть поменьше";

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Лимиты
        int A = input.nextInt();
        int B = input.nextInt();
        int C = input.nextInt();
        int K = input.nextInt();

        int[][] dailyNutrients = new int[7][4];
        for (int i = 0; i < dailyNutrients.length; i++) {
            for (int j = 0; j < dailyNutrients[i].length; j++) {
                dailyNutrients[i][j] = input.nextInt();
            }
        }

        // факт
        int currentA = 0;
        int currentB = 0;
        int currentC = 0;
        int currentK = 0;

        for (int i = 0; i < dailyNutrients.length; i++) {
            for (int j = 0; j < dailyNutrients[i].length; j++) {
                if (j == 0)
                    currentA += dailyNutrients[i][j];
                if (j == 1)
                    currentB += dailyNutrients[i][j];
                if (j == 2)
                    currentC += dailyNutrients[i][j];
                if (j == 3)
                    currentK += dailyNutrients[i][j];
            }
        }
        if (A > currentA && B > currentB && C > currentC && K > currentK)
            System.out.println(OK);
        else System.out.println(BADLY);
    }
}
