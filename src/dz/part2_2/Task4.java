package dz.part2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
Пример:
3
1 2 3
1 7 3
1 2 3
7
----->
1 3
1 3
-------------------
4
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 5
5
------>
1 2 3
1 2 3
1 2 3
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

//        int n = 4; // размер массива
//        int[][] arr = {
//                {1, 2, 3, 4},
//                {1, 2, 3, 4},
//                {1, 2, 3, 4},
//                {1, 2, 3, 5}
//        };
//
//        int p = 5; // число P для поиска

        int n = 3; // размер массива
        int[][] arr = {
                {1, 2, 3},
                {1, 7, 3},
                {1, 2, 3},
        };

        int p = 7; // число P для поиска

        // координаты числа P
        int indexI = 0; // строка
        int indexJ = 0; // столбец
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (arr[i][j] == p) {
                    indexI = i;
                    indexJ = j;
                }
            }
        }

        // создаем новый массив на одну строку и один столбец меньше, посколько
        // мы знаем, что новый массив будет на одну строку и один столбец меньше исходного
        // поскольку условие
        int[][] result = new int[n - 1][n - 1];
        int iResult = 0; // счетчик строк
        int jResult = 0; // счетчик столбцов
        for (int i = 0; iResult < result.length; i++) {
            for (int j = 0; jResult < result[0].length; j++) {
                if (j == indexJ)
                    continue;
                result[iResult][jResult] = arr[i][j];
                jResult++;
            }
            if (i == indexI)
                continue;
            iResult++;
            jResult = 0;
        }

        // Выводим результирующий массив
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                if (j == result[i].length - 1)
                    System.out.print(result[i][j]);
                else
                    System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
