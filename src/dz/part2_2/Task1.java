package dz.part2_2;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
3 2
10 20 15
7 5 9
-------> 10 5
1 3
30
42
15
--------> 30 42 15
 */


import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt(); // столбцы
        int m = input.nextInt(); // строки
        int[][] arr = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = input.nextInt();
            }
        }

        int minNumber = arr[0][0];
        int minNumIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(arr[i][j] < minNumber) {
                    minNumber = arr[i][j];
                    minNumIndex = j;
                }
            }
            System.out.print(arr[i][minNumIndex] + " ");
            minNumber = arr[i][0];
            minNumIndex = 0;
        }
    }
}
