package dz.part2_2;
/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
12374 -> 1 2 3 7 4
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = 12374;
        String message = n + "";
        numbersFromLeftToRighterse(message);
    }

    public static String numbersFromLeftToRighterse(String message) {
        if (message.isEmpty()) {
            return message;
        } else {
            System.out.print(message.charAt(0) + " ");
            return numbersFromLeftToRighterse(message.substring(1));
        }
    }
}
