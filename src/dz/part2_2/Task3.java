package dz.part2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
    Ограничения:
    0 <= X, Y < N
7
3 3
->>
0 0 0 0 0 0 0
0 0 X 0 X 0 0
0 X 0 0 0 X 0
0 0 0 K 0 0 0
0 X 0 0 0 X 0
0 0 X 0 X 0 0
0 0 0 0 0 0 0
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // высота и ширина матрицы
        int n = 7;
        Object[][] arr = new Object[n][n];
        // координаты коня на шахматной доске
        int x = 3;
        int y = 3;
        //возможные ходы
//        int[][] possibleMoves = {
//                {-1, -2}, {-2, -1}, {-2, 1}, {1, -2},
//                {-1, 2}, {2, -1}, {1, 2}, {2, 1}};

        char horse = 'K';
        char moves = 'X';

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = 0;
                if (i == x - 2 && j == y - 1)
                    arr[i][j] = moves;

                if (i == x - 2 && j == y + 1)
                    arr[i][j] = moves;

                if (i == x - 1 && j == y - 2)
                    arr[i][j] = moves;

                if (i == x - 1 && j == y + 2)
                    arr[i][j] = moves;

                if (i == x + 1 && j == y - 2)
                    arr[i][j] = moves;

                if (i == x + 1 && j == y + 2)
                    arr[i][j] = moves;

                if (i == x + 2 && j == y - 1)
                    arr[i][j] = moves;

                if (i == x + 2 && j == y + 1)
                    arr[i][j] = moves;

                if (i == x && j == y)
                    arr[i][j] = horse;
            }
        }
        // выводим таблицу
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(arr[i][j]);
                else
                    System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
