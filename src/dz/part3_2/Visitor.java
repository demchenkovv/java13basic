package dz.part3_2;

public class Visitor {
    private String name;
    private int id;
    private Book book;

    public Visitor() {
    }

    public Visitor(String name) {
        this.name = name;
        this.id = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
