package dz.part3_2;

/**
 * Класс main
 */
public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        // добавить книги
        library.addBook("Му-Му", "И.С. Тургенев");
        library.addBook("Мертвые души", "Н.В. Гоголь");
        library.addBook("Отцы и дети", "И.С. Тургенев");
        library.addBook("Капитанская дочь", "А.С. Пушкин");
        library.addBook("Беглец", "А.П. Чехов");
        // добавить книгу, которая есть в библиотеке
        library.addBook("Отцы и дети", "И.С. Тургенев");
        // удалить книгу
        library.removeBook("Капитанская дочь");
        // показать все книги
        library.showAllBooks();
        // поиск книг по названию
        library.searchBookByTitle("Отцы и дети");
        // поиск книг по автору
        library.searchBookByAuthor("И.С. Тургенев");
        // добавить посетителей
        library.addVisitor("Иван");
        library.addVisitor("Петр");
        library.addVisitor("Юрий");
        // отобразить посетителей
        library.showAllVisitors();
        // одолжить книгу
        library.borrowBook("Петр", "Мертвые души");
        library.borrowBook("Иван", "Отцы и дети");
        library.borrowBook("Юрий", "Му-му");
        // одолжить книгу посетителю, у которого есть книга на руках
        library.borrowBook("Петр", "Отцы и дети");
        // вернуть и оценить книгу
        library.returnBook("Петр", 1, "Мертвые души", 5);
        library.returnBook("Иван", 2, "Отцы и дети", 4);
        library.returnBook("Юрий", 3, "Му-му", 5);

        // взяли и вернули несколько раз для теста средней оценки
        library.borrowBook("Иван", "Мертвые души");
        library.returnBook("Иван", 2, "Мертвые души", 2);
        library.borrowBook("Иван", "Мертвые души");
        library.returnBook("Иван", 2, "Мертвые души", 3);
        library.borrowBook("Иван", "Мертвые души");
        library.returnBook("Иван", 2, "Мертвые души", 1);
        library.borrowBook("Иван", "Мертвые души");
        library.returnBook("Иван", 2, "Мертвые души", 1);

        // отобразить выставленные оценки
        library.showBookRatings();
        // отобразить среднюю оценку книги
        library.averageRatingBookByTitle("Мертвые души");
        library.averageRatingBookByTitle("Беглец");
    }
}
