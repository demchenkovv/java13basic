package dz.part3_2;

import java.util.ArrayList;

public class Book {
    private String title;
    private String author;
    private boolean bookAvailable;
    private ArrayList<Integer> rateBook;

    public Book() {
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        bookAvailable = true;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isBookAvailable() {
        return bookAvailable;
    }

    public void setBookAvailable(boolean bookAvailable) {
        this.bookAvailable = bookAvailable;
    }


    public ArrayList<Integer> getRateBook() {
        return rateBook;
    }

    public void setRateBook(int rate) {
        if (rateBook == null) {
            this.rateBook = new ArrayList<>();
        }
        this.rateBook.add(rate);
    }

    public int averageRateBook() {
        if (rateBook == null)
            return 0;
        int res = 0;

        for (int i = 0; i < rateBook.size(); i++) {
            res += getRateBook().get(i);
        }
        return res / getRateBook().size();
    }
}