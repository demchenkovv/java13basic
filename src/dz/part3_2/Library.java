package dz.part3_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Library {
    private ArrayList<Book> books = new ArrayList<>();
    private ArrayList<Visitor> visitors = new ArrayList<>();
    private static int visitorsId;

    /**
     * Добавить книгу в библиотеку
     */
    public ArrayList<Book> addBook(String title, String author) {
        for (Book book : books)
            if (title.equalsIgnoreCase(book.getTitle())) {
                System.out.println("Книга " + title + " уже есть в библиотеке.");
                return books;
            }
        books.add(new Book(title, author));
        return books;
    }

    /**
     * Удалить книгу из библиотеки
     */
    public ArrayList<Book> removeBook(String title) {
        for (Book book : books)
            if (title.equalsIgnoreCase(book.getTitle())) {
                books.remove(book);
                System.out.println("Книга " + title + " удалена.");
                return books;
            }
        System.out.println("Книга " + title + " не найдена.");
        return books;
    }

    /**
     * Поиск книги по названию
     */
    public void searchBookByTitle(String title) {
        System.out.println("\nПоиск книги: " + title);
        for (Book book : books)
            if (title.equalsIgnoreCase(book.getTitle())) {
                System.out.println("Книга найдена, автор " + book.getAuthor());
                return;
            }
    }

    /**
     * Поиск книг по автору
     */
    public void searchBookByAuthor(String author) {
        System.out.println("\nПоиск книг по автору: " + author + ".\nНайдены следующие книги: ");
        for (Book book : books)
            if (author.equalsIgnoreCase(book.getAuthor()))
                System.out.println(book.getTitle());
    }

    /**
     * Все книги в библиотеке
     */
    public void showAllBooks() {
        System.out.println("\nВ библиотеке зарегистрированы следующие книги: ");
        for (Book book : books)
            System.out.println(book.getTitle() + ", " + book.getAuthor() + ", в наличии: " + book.isBookAvailable());
    }

    /**
     * Добавить посетителя c id == 0
     */
    public void addVisitor(String name) {
        visitors.add(new Visitor(name));
    }

    /**
     * Все посетители библиотеки
     */
    public void showAllVisitors() {
        System.out.println("\nПосетители библиотеки (id : Имя): ");
        for (Visitor visitor : visitors)
            System.out.println(visitor.getId() + " : " + visitor.getName());
    }

    /**
     * Одолжить посетителю name книгу:title
     */
    public void borrowBook(String name, String title) {
        for (Visitor visitor : visitors) {
            if (visitor.getName().equalsIgnoreCase(name) && visitor.getBook() == null) {
                for (Book book : books) {
                    if (book.getTitle().equalsIgnoreCase(title) && book.isBookAvailable()) {
                        visitor.setBook(book);
                        book.setBookAvailable(false);
                        System.out.println("\nКнигу " + title + " одолжил " + name + ".");
                        if (visitor.getId() == 0) {
                            visitor.setId(++visitorsId);
                            System.out.println("Посетителю " + name + " присвоен id: " + visitor.getId() + ".");
                        }
                        return;
                    }
                }
            }
        }
    }

    /**
     * Посетитель:name с id возвращает книгу:title в библиотеку и ставит оценку от 1 до 5
     */
    public void returnBook(String name, int id, String title, int rate) {
        for (Visitor visitor : visitors)
            if (visitor.getName().equalsIgnoreCase(name) && visitor.getId() == id)
                for (Book book : books)
                    if (book.getTitle().equalsIgnoreCase(title)) {
                        book.setBookAvailable(true);
                        visitor.setBook(null);
                        System.out.println("\n" + name + " вернул книгу " + title + " в библиотеку и поставил оценку " + rate + ".");
                        if (rate < 1 || rate > 5) {
                            System.out.println("Ваша оценка не засчитана, т.к. она меньше 1 или больше 5");
                        } else {
                            rateBook(title, rate);
                        }
                        return;
                    }
    }

    /**
     * Книге title поставить оценку rate
     */
    public void rateBook(String title, int rate) {
        for (Book book : books)
            if (book.getTitle().equalsIgnoreCase(title))
                book.setRateBook(rate);
    }

    /**
     * Отразить все выставленные оценки
     */
    public void showBookRatings() {
        System.out.println("\nОценки книг: ");
        for (Book book : books)
            System.out.println(book.getTitle() + ": " + book.getRateBook());
    }

    /**
     * Отразить среднюю оценку книги по названию
     */
    public void averageRatingBookByTitle(String title) {
        for (Book book : books) {
            if (book.getTitle().equalsIgnoreCase(title)) {
                System.out.println("\nСредняя оценка книги " + book.getTitle() + " " + book.averageRateBook());
                return;
            }
        }
    }
}