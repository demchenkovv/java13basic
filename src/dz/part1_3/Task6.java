package dz.part1_3;
/*
В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
 */
import java.util.Scanner;

public class Task6 {
    public static final int BANKNOTE_8 = 8;
    public static final int BANKNOTE_4 = 4;
    public static final int BANKNOTE_2 = 2;
    public static final int BANKNOTE_1 = 1;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt(); // исходная сумма

        int num; // количество купюр
        int sum; // остаток суммы

        num = n / BANKNOTE_8; // количество купюр номиналом 8
        System.out.print(num + " ");
        sum = n % BANKNOTE_8; // остаток от выдачи купюр номиналом 8
        num = sum / BANKNOTE_4; // количество купюр номиналом 4
        System.out.print(num + " ");
        sum %= BANKNOTE_4; // остаток от выдачи купюр номиналом 4
        num = sum / BANKNOTE_2; // количество купюр номиналом 2
        System.out.print(num + " ");
        sum %= BANKNOTE_2; // остаток от выдачи купюр номиналом 2
        num = sum / BANKNOTE_1; // количество купюр номиналом 1
        System.out.print(num + " ");

    }
}
