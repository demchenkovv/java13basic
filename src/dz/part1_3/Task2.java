package dz.part1_3;
//На вход подается два положительных числа m и n. Найти сумму чисел между m
//и n включительно.
public class Task2 {
    public static void main(String[] args) {
        java.util.Scanner input = new java.util.Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int sum = 0;
        for (int i = m; i <= n; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}