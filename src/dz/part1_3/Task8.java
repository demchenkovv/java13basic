package dz.part1_3;
/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
Например:
6
29
40 37 97 72 80 18
-> 326
 */
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int p = input.nextInt();

        int sum = 0; 

        for (int i = 0; i < n; i++) {
            int temp = input.nextInt();
            if (temp > p) {
                sum += temp;
            }
        }
        System.out.println(sum);
    }
}