package dz.part1_3;
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.
Например:
-55 -42 -19 -15 17 33 -> 4
10 20 -> 0
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt(); // первая инициализация входных данных
        int sum = 0; // количество неотрицательных чисел

        while (true) {
            if (n < 0) {
                sum++;
                n = input.nextInt();
            } else break;
        }
        System.out.println(sum);
    }
}
