package dz.part1_3;

import java.util.Scanner;

/*
Вывести на экран “ёлочку” из символа звездочки (#) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |

Входные данные: 3
Выходные данные: (ёлочка)
  #
 ###
#####
|
С пониманием логики помогло это видео https://www.youtube.com/watch?v=2nKptpwCIkE
 */
public class Task10 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt(); // высота ёлочки
        // для решения необходимо понимание сколько будет итераций,
        // сколько необходимо пробелов при каждой итерации

        // первый цикл - высота елочки (количество внешних итераций)
        for (int i = 1; i <= n; i++) {
            // 1-й внутренний цикл - пробелы
            for (int j = 0; j < (n - i); j++) {
                System.out.print(" ");
            } // 2-й внутренний цикл - дерево
            for (int j = 0; j < i + (i - 1); j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int i = 0; i < n - 1; i++) {
            System.out.print(" ");
        }
        System.out.println("|");
    }
}