package dz.part1_3;
/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
Пример:
Hello world -> 10
Never give up -> 11
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String message = input.nextLine();

        int sumSymbol = 0; // сумма символов, обновляемая после каждой итерации
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) != ' ') {
                sumSymbol++;
            }
        }
        System.out.println(sumSymbol);
    }
}