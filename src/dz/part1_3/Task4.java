package dz.part1_3;
// Дано натуральное число n. Вывести его цифры в “столбик”.
// Например: 74
// 7
// 4

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        String s = "" + n;
        for (int i = 0; i < s.length(); i++) {
            System.out.println(s.charAt(i));
        }
    }
}
