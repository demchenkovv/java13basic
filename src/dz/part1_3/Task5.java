package dz.part1_3;
/*
Даны положительные натуральные числа m и n.
Найти остаток от деления m на n, не выполняя операцию взятия остатка.
Пример:
9 1 -> 0
8 3 -> 2
7 9 -> 7
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();

        if (m > n) {
            int mn = m / n;
            int mid = mn * n;
            int res = m - mid;
            System.out.println(res);

        } else {
            System.out.println(m);
        }
    }
}