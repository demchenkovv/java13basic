package dz.part1_3;

import java.util.Scanner;
// На вход подается два положительных числа m и n.
// Необходимо вычислить m^1 + m^2 + ... + m^n
// Например: 8 5 = 37448
public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();

        int sum = 0;
        for (int i = 1; i <= n ; i++) {
            sum += (int)Math.pow(m, i);
        }
        System.out.println(sum);
    }
}