package dz.part1_2;
/* (1 балл)
"А логарифмическое?" - не унималась дочь.
Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n.
Ограничения: -500 < n < 500 */

import java.util.Scanner;

public class Task10 {
    public static final double EPSILON = 1.E-4;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        double n = Math.abs(scanner.nextDouble());
        double isExp = Math.exp(Math.log(n));
        double isLog = Math.log(Math.exp(n));
        boolean isTrue = (isExp - isLog) <= EPSILON;

        if (isTrue) {
            System.out.println(isTrue);
        } else {
            System.out.println("false");
        }
    }
}