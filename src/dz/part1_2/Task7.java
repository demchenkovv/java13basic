package dz.part1_2;
/* (1 балл)
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100

Пример:
Входные данные Hi great team!
Выходные данные
Hi
great team!
*/

import java.util.Scanner;

public class Task7 {
    public static final int MAX_LENGHT = 100;
    public static final int MIN_LENGHT = 2;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        s = s.trim(); // Первый и последний символ строки гарантированно не пробел
        String s1 = " "; // Пробел

        // Проверка на наличие пробела (s1) в строке (s)
        if (s.contains(s1)) {
            // Проверка длины строки
            if (s.length() > MIN_LENGHT || s.length() < MAX_LENGHT) {
                int k = s.indexOf(' ');
                String first = s.substring(0, k);
                String second = s.substring(k + 1);
                System.out.println(first + "\n" + second);
            }
        } else
            System.out.println("Нарушены условия ограничения. В строке отсутствует пробел и/или длина строки меньше двух или больше 99 символов.");
    }
}