package dz.part1_2;
/*(2 балла) Старый телефон Андрея сломался, поэтому он решил приобрести
новый. Продавец телефонов предлагает разные варианты, но Андрея
интересуют только модели серии samsung или iphone. Также Андрей решил
рассматривать телефоны только от 50000 до 120000 рублей. Чтобы не тратить
время на разговоры, Андрей хочет написать программу, которая поможет ему
сделать выбор.
На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или
iphone и стоимость от 50000 до 120000 рублей включительно. Иначе вывести
"Не подходит".
Гарантируется, что в модели телефона не указано одновременно несколько
серий.*/

import java.util.Scanner;

public class Task_additional3 {
    public static final int PRICE_MIN = 50000;
    public static final int PRICE_MAX = 120000;
    public static final String phone1 = "iphone";
    public static final String phone2 = "samsung";


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //version 1 (модель телефона и сумма объявляются в разных переменных (String, Int)
        String model = scanner.nextLine();
        int price = scanner.nextInt();

        if (model.contains(phone1) || model.contains(phone2) && price > PRICE_MIN && price <= PRICE_MAX)
            System.out.println("Можно купить");
        else
            System.out.println("Не подходит");


        // version 2 (модель телефона и сумма объявляются одной строкой (String)
//        String s = scanner.nextLine();              // подается одна строка с моделью и суммой телефона
//
//        int startIndex = s.lastIndexOf(' ');     // индекс последнего пробела (между моделью телефона и его суммой
//        String sumStr = s.substring(startIndex+1);     // выделяем и помещаем сумму в строковую переменную sumStr
//        int sumInt = Integer.parseInt(sumStr);         // преобразование числовую строку преобразуем в число
//
//        if (s.contains(phone1) || s.contains(phone2) && sumInt > PRICE_MIN && sumInt <= PRICE_MAX)
//            System.out.println("Можно купить");
//        else
//            System.out.println("Не подходит");

    }
}