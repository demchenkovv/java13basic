package dz.part1_2;
/* (1 балл)
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
Входные данные Hi great team!
Выходные данные Hi great
team!
*/

import java.util.Scanner;

public class Task8 {
    public static final int MAX_LENGHT = 100;
    public static final int MIN_LENGHT = 2;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        s = s.trim();
        String s1 = " ";

        if (s.contains(s1)) {
            if (s.length() > MIN_LENGHT && s.length() < MAX_LENGHT) {
                int k = s.lastIndexOf(' ');
                String first = s.substring(0, k);
                String second = s.substring(k + 1);
                System.out.println(first + '\n' + second);
            }
        } else {
            System.out.println("Нарушены условия ограничения. В строке отсутствует пробел и/или длина строки меньше двух или больше 99 символов.");
        }
    }
}