package dz.part1_2;
/* (1 балл)
Пока Петя практиковался в работе со строками, к нему подбежала его
дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+
cos^2(x) - 1 == 0) всегда-всегда выполняется?"
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество будет выполняться (то есть будет выводить true
при любом x).
Ограничения: -1000 < x < 1000
*/

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double y = x; // для проверки ограничений
        x = Math.toRadians(x);
        boolean tt = Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2) - 1 == 0;


        if (-1000 < y && y < 1000) {
            if (tt) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        } else
            System.out.println("Нарушены условия ограничения. Введите число от -999 до 999 включительно.");
    }
}