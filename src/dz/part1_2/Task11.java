package dz.part1_2;
/* (1 балл)
Разобравшись со своими (и не только) задачками, Петя уже собирался
лечь спать и отдохнуть перед очередным тяжелым рабочим днем, но вдруг в
тишине раздается детский шепот: "Паааапааа, мы забыли решить ещё одну
задачку! Давай проверим, можно ли из трех сторон составить треугольник?".
Что ж, придется написать еще одну программу, связанную со школьной
математикой.
На вход подается три целых положительных числа – длины сторон
треугольника. Нужно вывести true, если можно составить треугольник из этих
сторон и false иначе.
Ограничения: 0 < a, b, c < 100 */

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        boolean isTrue = a < b + c && b < a + c && c < a + b;

        if (isTrue)
            System.out.println("true");
        else
            System.out.println("false");

    }
}