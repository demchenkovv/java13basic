package dz.part1_2;
/* (1 балл)
После вкусного обеда Петя принимается за подсчет дней до выходных.
Календаря под рукой не оказалось, а если спросить у коллеги Феди, то тот
называет только порядковый номер дня недели, что не очень удобно. Поэтому
Петя решил написать программу, которая по порядковому номеру дня недели
выводит сколько осталось дней до субботы. А если же сегодня шестой
(суббота) или седьмой (воскресенье) день, то программа выводит "Ура,
выходные!"
Ограничения: 1 <= n <= 7
*/

import java.util.Scanner;

public class Task4 {
    public static final int SATURDAY = 6;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n == 6 || n == 7) {
            System.out.println("Ура, выходные!");
        } else if (n >= 1 && n <= 5) {
            n = SATURDAY - n;
            System.out.println(n);

            // В одну строку
            // System.out.println(n == 6 || n == 7 ? "Ура, выходные!" : SATURDAY - n);

        }
    }
}