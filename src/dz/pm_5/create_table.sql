create table flowers
(
    id    serial primary key,
    name  varchar(20) not null
        constraint flowers_pk unique,
    price varchar(5)  not null
);

create table customers
(
    id    serial primary key,
    name  varchar(20) not null,
    phone varchar(10) not null
);

create table orders
(
    id       serial primary key,
    customer integer     not null references customers (id),
    flower   varchar(20) not null references flowers (name),
    amount   integer     not null check (amount > 0 and amount < 1000),
    date     date default now()
);