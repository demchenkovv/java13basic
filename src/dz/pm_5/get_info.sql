select * from flowers;
select * from customers;
select * from orders;

-- По идентификатору заказа получить данные заказа и данные клиента,
-- создавшего этот заказ
select orders.id, customers.name, customers.phone, flower, amount
from customers
join orders on customers.id = orders.customer
where orders.id = 2;

-- Получить данные всех заказов одного клиента по идентификатору
-- клиента за последний месяц
select *
from orders
join customers ON orders.customer = customers.id
where (customer = 3) and (date > date - interval '1 month');

-- Найти заказ с максимальным количеством купленных цветов, вывести их
-- название и количество
select id, flower, max(amount)
from orders
group by id
order by max(amount) desc
limit 1;

-- Вывести общую выручку (сумму золотых монет по всем заказам) за все время
-- select flowers.name, sum(flowers.price), sum(orders.amount), sum(flowers.price * orders.amount)
select sum(flowers.price * orders.amount) as gross_revenue
from flowers
join orders ON flowers.name = orders.flower;
--group by flowers.name;