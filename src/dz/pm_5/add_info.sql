insert into flowers (id, name, price) values (1, 'rose', 100);
insert into flowers (id, name, price) values (2, 'lilies', 50);
insert into flowers (id, name, price) values (3, 'daisies', 25);

insert into customers (id, name, phone) values (1, 'Semen_Gorbunkov', 9101110010);
insert into customers (id, name, phone) values (2, 'Gesha_Kozodoev', 9202220020);
insert into customers (id, name, phone) values (3, 'Lyolik', 9303330030);
insert into customers (id, name, phone) values (4, 'Comrade Saakhov', 9404440040);
insert into customers (id, name, phone) values (5, 'Lyolik', 9505550050);

insert into orders (id, customer, flower, amount) values (1, 1, 'rose', 10);
insert into orders (id, customer, flower, amount) values (2, 2, 'rose', 20);
insert into orders (id, customer, flower, amount) values (3, 2, 'rose', 30);
insert into orders (id, customer, flower, amount) values (4, 3, 'lilies', 40);
insert into orders (id, customer, flower, amount) values (5, 3, 'daisies', 50);