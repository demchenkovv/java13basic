package dz.pm_4.task6;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */
public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setSet = new HashSet<>();
        Set<Integer> integerSet;


        for (int i = 0; i < 4; i++) {
            integerSet = new HashSet<>();
            for (int j = 0; j < 2; j++) {
                integerSet.add(i + j);
            }
            setSet.add(integerSet);
        }
        System.out.println("Set<Set<Integer>>: ");
        setSet.forEach(System.out::println);

        Set<Integer> generalSet = toSetFrom2DSet(setSet);
        System.out.println("\nSet<Integer> generalSet: ");
        generalSet.forEach(s -> System.out.print(s + " "));
    }

    public static Set<Integer> toSetFrom2DSet(Set<Set<Integer>> integer2DSet) {
        return integer2DSet.stream()
                .flatMap(Set::stream) // == integers -> integers.stream()
                .collect(Collectors.toSet());
    }
}
