package dz.pm_4.task5;

import java.util.List;

/*
 * На вход подается список непустых строк. Необходимо привести все символы строк к
 * верхнему регистру и вывести их, разделяя запятой.
 * Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Main {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "def", "qqq");

        List<String> uppercaseStringsList = getStringToUpperCase(stringList);

        for (int i = 0; i < uppercaseStringsList.size(); i++) {
            if (i == uppercaseStringsList.size() - 1) {
                System.out.print(uppercaseStringsList.get(i) + ".");
            } else {
                System.out.print(uppercaseStringsList.get(i) + ", ");
            }
        }
    }

    private static List<String> getStringToUpperCase(List<String> stringList) {
        return stringList.stream()
                .map(String::toUpperCase)
                .toList();
    }
}
