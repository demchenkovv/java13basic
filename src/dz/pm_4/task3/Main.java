package dz.pm_4.task3;

import java.util.List;

/*
 * На вход подается список строк. Необходимо вывести количество непустых строк в
 * списке.
 * Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class Main {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "", "", "def", "qqq");
        List<String> result = getListWithoutEmptyLines(stringList);
        result.forEach(System.out::println);
    }

    private static List<String> getListWithoutEmptyLines(List<String> stringList) {
        return stringList.stream()
                .filter(str -> str.length() != 0)
                .toList();
    }
}
