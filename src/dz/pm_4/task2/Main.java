package dz.pm_4.task2;

import java.util.List;

/*
 * На вход подается список целых чисел. Необходимо вывести результат перемножения
 * этих чисел.
 * Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
 * 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> integerList = List.of(1, 2, 3, 4, 5);
        int factorial = getFactorial(integerList);
        System.out.println(factorial);
    }

    public static int getFactorial(List<Integer> integerList) {
        return integerList.stream()
                .reduce((accumulator, digit) -> accumulator * digit)
                .get();
    }
}
