package dz.pm_4.task4;

import java.util.Collections;
import java.util.List;

/*
 * На вход подается список вещественных чисел. Необходимо отсортировать их по
 * убыванию.
 */
public class Main {
    public static void main(String[] args) {
        List<Double> doubleList = List.of(4.23, 1.5, 6.71, 1.0, 4.24);
        System.out.println("doubleList: " + getDecreasingDigitalSort(doubleList));

        List<Float> floatList = List.of(4.23F, 1.5F, 6.71F, 1.0F, 4.24F);
        System.out.println("floatList: " + getDecreasingDigitalSort(floatList));
    }

    public static <T> List<T> getDecreasingDigitalSort(List<T> digitalList) {
        return digitalList.stream()
                .sorted(Collections.reverseOrder())
                .toList();
    }
}
