package dz.pm_4.task7;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter two words: ");
        String str1 = input.next();
        String str2 = input.next();
        System.out.println(str1 + " and " + str2 + ": " + isOneAction(str1, str2));
    }

    public static boolean isOneAction(String str1, String str2) {
        int lengthDifference = str1.length() - str2.length();
        int stringOfMinimumLength = Math.min(str1.length(), str2.length());
        int mismatches = 0;

        if (lengthDifference == -1 || lengthDifference == 0 || lengthDifference == 1) {
            for (int i = 0; i < stringOfMinimumLength; i++) {
                if (str1.charAt(i) != str2.charAt(i)) {
                    mismatches++;
                }
            }
        } else {
            return false;
        }

        if (lengthDifference != 0 && mismatches == 0) {
            return true;
        } else return lengthDifference == 0 && mismatches <= 1;
    }

    /**
     * Версия Андрея Гаврилова
     */
    public static boolean check(String s1, String s2) {
        List<Character> list1 = s1.chars().mapToObj(character -> (char) character).collect(Collectors.toList());
        List<Character> list2 = s2.chars().mapToObj(character -> (char) character).collect(Collectors.toList());
        int differenceSize = list1.size() - list2.size();
        if (differenceSize == 0 || differenceSize == 1) {
            list1.removeAll(list2);
            return list1.size() == 1;
        } else if (differenceSize == -1) {
            list2.removeAll(list1);
            return list2.size() == 1;
        } else {
            return false;
        }
    }
}
