package dz.pm_4.task1;

import java.util.ArrayList;
import java.util.List;

/*
 * Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
 * экран.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        int sum = list.stream()
                .filter(digit -> digit % 2 == 0)
                .reduce(Integer::sum)
                .get();

        System.out.println(sum);
    }
}
