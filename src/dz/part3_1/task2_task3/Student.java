package dz.part3_1.task2_task3;

import java.util.Arrays;

/*
Task2:
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

Task3:
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */
public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10]; // последние 10 оценок студента
    private double averageMark; // средняя оценка
    public static int currentStudents; // число студентов (для задания № 3 в целях определения длины массива)

    Student() {
        currentStudents++;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    // Метод, добавляющий новую оценку в grades
    public void addMark(int[] grades, int mark) {
        if (mark > 0) { // если оценка больше нуля, то присваивается temp
            for (int i = 0; i < grades.length - 1; i++) {
                grades[i] = grades[i + 1]; // смещение оценок влево
            }
            grades[grades.length - 1] = mark; // последний элемент массива - наша оценка
        }
    }

    // Метод, возвращающий средний балл студента
    public void findAverageMark(int[] grades) {
        double sumMarks = 0; // сумма оценок
        double currentMarks = 0; // число оценок
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] > 0) {
                sumMarks += grades[i];
                currentMarks++;
            }
        }
        averageMark = (int) (sumMarks / currentMarks * 100) / 100.0; // средняя оценка
        // Если бы возвращали:
        // return this.averageMark = (int) (sumMarks / currentMarks * 100) / 100.0; // средняя оценка если бы возвращали
    }

    public static void main(String[] args) {
        Student student1 = new Student();
        student1.setName("Полиграф");
        student1.setSurname("Шариков");
        // Метод addMark(int[], int) для внесения оценок в массив
        student1.addMark(student1.grades, 2);
        student1.addMark(student1.grades, 3);
        student1.addMark(student1.grades, 3);
        student1.addMark(student1.grades, 2);
        student1.addMark(student1.grades, 1);
        student1.addMark(student1.grades, 3);
        student1.addMark(student1.grades, 3);
        // Метод findAverageMark(int[], int) для расчета средней оценки
        student1.findAverageMark(student1.grades);
        System.out.println("Имя и фамилия: " + student1.getName() + " " + student1.getSurname());
        System.out.println("Оценки: " + Arrays.toString(student1.getGrades()));
        System.out.println("Средняя оценка: " + student1.averageMark);

        Student student2 = new Student();
        student2.setName("Юрий");
        student2.setSurname("Деточкин");
        // Метод addMark(int[], int) для внесения оценок в массив
        student2.addMark(student2.grades, 5);
        student2.addMark(student2.grades, 3);
        student2.addMark(student2.grades, 4);
        student2.addMark(student2.grades, 4);
        student2.addMark(student2.grades, 5);

        // Метод findAverageMark(int[], int) для расчета средней оценки
        student2.findAverageMark(student2.grades);
        System.out.println("Имя и фамилия: " + student2.getName() + " " + student2.getSurname());
        System.out.println("Оценки: " + Arrays.toString(student2.getGrades()));
        System.out.println("Средняя оценка: " + student2.averageMark);

        System.out.println("------------------------");
        Student student3 = new Student();
        student3.setName("Семён");
        student3.setSurname("Горбунков");
        // Метод addMark(int[], int) для внесения оценок в массив
        student3.addMark(student3.grades, 5);
        student3.addMark(student3.grades, 5);
        student3.addMark(student3.grades, 5);
        student3.addMark(student3.grades, 5);
        student3.addMark(student3.grades, 4);

        // Метод findAverageMark(int[], int) для расчета средней оценки
        student3.findAverageMark(student3.grades);
        System.out.println("Имя и фамилия: " + student3.getName() + " " + student3.getSurname());
        System.out.println("Оценки: " + Arrays.toString(student3.getGrades()));
        System.out.println("Средняя оценка: " + student3.averageMark);


        System.out.println("\nОбщее число студентов: " + Student.currentStudents);

        // Задание № 3 создать массив студентов
        Student[] studentsArray = new Student[Student.currentStudents];
        studentsArray[0] = student1;
        studentsArray[1] = student2;
        studentsArray[2] = student3;

        // передать массив студентов для определения лучшего
        StudentService.bestStudent(studentsArray);

        // передать массив студентов для сортировки по фамилии
        StudentService.sortBySurname(studentsArray);

    }


    static class StudentService {
        public static void bestStudent(Student[] studentsArray) { // принимаем массив студентов
            String bestName = studentsArray[0].getName(); // имя лучшего студента
            String bestSurname = studentsArray[0].getSurname(); // фамилия лучшего студента
            double bestAverageMark = studentsArray[0].averageMark; // средняя оценка лучшего студента
            for (int i = 1; i < studentsArray.length; i++) {
                if (studentsArray[i].averageMark > bestAverageMark) {
                    bestName = studentsArray[i].getName();
                    bestSurname = studentsArray[i].getSurname();
                    bestAverageMark = studentsArray[i].averageMark;
                }
            }
            System.out.println("\nЛучший студент " + bestName + " " + bestSurname +
                    " со средней оценкой " + bestAverageMark);
        }

        // Сортировка студентов по фамилии методом выбора
        public static void sortBySurname(Student[] studentsArray) {
            System.out.println("\nФамилии до сортировки: ");
            for (Student obj : studentsArray) {
                System.out.println(obj.getSurname() + " " + obj.getName());
            }

            // Сортировка методом выбора, используя compareTo
            for (int i = 0; i < studentsArray.length - 1; i++) {
                String nextSurname = studentsArray[i].surname;
                int surnameIndex = i;
                Student student = studentsArray[i];


                for (int j = i + 1; j < studentsArray.length; j++) {
                    // если число положительное, значит должно стоять следующее в списке
                    // например, "В".compareTo("А") == 1, значит "В" дожно стоять после "А"
                    if (nextSurname.compareTo(studentsArray[j].surname) > 0) {
                        nextSurname = studentsArray[j].surname;
                        surnameIndex = j;
                        student = studentsArray[j];
                    }
                }
                // Переставить/заменить объекты местами studentsArray[surnameIndex] и studentsArray[i]
                if (surnameIndex != i) {
                    studentsArray[surnameIndex] = studentsArray[i];
                    studentsArray[i] = student;
                }
            }
            System.out.println("\nФамилии после сортировки: ");
            for (Student obj : studentsArray) {
                System.out.println(obj.getSurname() + " " + obj.getName());
            }
        }
    }
}
