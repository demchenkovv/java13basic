package dz.part3_1.task8;

/*
Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static)
 */
public class Atm {
    private double currencyRubUsd; // стоимость 1 рубля за 1 доллар
    private double currencyUsdRub; // стоимость 1 доллара за 1 рубль
    private static int instanceAtm; // количество созданных инстансов класса Atm

    Atm() {
        currencyRubUsd = 0;
        currencyUsdRub = 0;
        instanceAtm++;
    }

    // курс валют перевода долларов в рубли
    public void setCurrencyUsdRub(double currencyUsdRub) {
        if (currencyUsdRub > 0)
            this.currencyUsdRub = currencyUsdRub;
    }

    // курс валют перевода рублей в доллары
    public void setCurrencyRubUsd(double currencyRubUsd) {
        if (currencyRubUsd > 0)
            this.currencyRubUsd = currencyRubUsd;
    }

    // Конвертирует переданную сумму рублей в доллары
    public void exchangeRubToUsd(double rub) {
        if (rub > 0)
            System.out.println((int) (rub * currencyRubUsd * 100) / 100.0);
    }

    // Конвертирует переданную сумму долларов в рубли
    public void exchangeUsdToRub(double usd) {
        if (usd > 0)
            System.out.println((int) (usd * currencyUsdRub * 100) / 100.0);
    }

    // публичный метод, возвращающий количество созданных инстансов класса Atm
    public static void getInstanceAtm() {
        System.out.println(instanceAtm);
    }

    /**
     * Метод main()
     */
    public static void main(String[] args) {
        Atm atm = new Atm();
        atm.setCurrencyUsdRub(61.50);
        atm.setCurrencyRubUsd(0.016229);
        atm.exchangeRubToUsd(1500);
        atm.exchangeUsdToRub(23);
        getInstanceAtm();
    }
}