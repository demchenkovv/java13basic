package dz.part3_1.task5;

/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class DayOfWeek {
    private int dayOfWeek; // номер дня недели
    private String nameDayOfWeek; // название дня недели


    DayOfWeek() {

    }
    // Метод для изменения номера дня недели
    public void setDayOfWeek(byte dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
    // Метод для изменения названия дня недели
    public void setNameDayOfWeek(String nameDayOfWeek) {
        this.nameDayOfWeek = nameDayOfWeek;
    }
    // Метод для изменения номера дня недели и названия дня недели
    public void setDayAndNameOfWeek(byte dayOfWeek, String nameDayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        this.nameDayOfWeek = nameDayOfWeek;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public String getNameDayOfWeek() {
        return nameDayOfWeek;
    }
}

class Test {
    public static void main(String[] args) {
        // Создать 7 объектов дней недели
        DayOfWeek[] arrayDayOfWeek = new DayOfWeek[7];
        for (int i = 0; i < 7; i++) {
            arrayDayOfWeek[i] = new DayOfWeek();
        }
        // присвоить каждому полю объекта номер и день недели, используя setter для private полей данных
        arrayDayOfWeek[0].setDayAndNameOfWeek((byte) 1,"Monday");
        arrayDayOfWeek[1].setDayAndNameOfWeek((byte) 2,"Tuesday");
        arrayDayOfWeek[2].setDayAndNameOfWeek((byte) 3,"Wednesday");
        arrayDayOfWeek[3].setDayAndNameOfWeek((byte) 4,"Thursday");
        arrayDayOfWeek[4].setDayAndNameOfWeek((byte) 5,"Friday");
        arrayDayOfWeek[5].setDayAndNameOfWeek((byte) 6,"Saturday");
        arrayDayOfWeek[6].setDayAndNameOfWeek((byte) 7,"Sunday");

        // вывести на экран номер дня недели и название дня недели
        for (int i = 0; i < 7; i++) {
            System.out.println(arrayDayOfWeek[i].getDayOfWeek() + " " + arrayDayOfWeek[i].getNameDayOfWeek());
        }
    }
}
