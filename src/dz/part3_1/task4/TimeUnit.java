package dz.part3_1.task4;

/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.

Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
 */
public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;
    private int hoursAmPm;

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public int getHoursAmPm() {
        return hoursAmPm;
    }

    TimeUnit(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    TimeUnit(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = 0;
    }

    TimeUnit(int hours) {
        this.hours = hours;
        this.minutes = 0;
        this.seconds = 0;
    }

    /**
     * Метод для вывода на экран времени в формате hh:mm:ss
     */
    public void showTime() {
        if ((getHours() < 0 || getHours() >= 24) || (minutes < 0 || minutes >= 60) || (seconds < 0 || seconds >= 60))
            System.out.println("Введены недопустимые значения.");
        else {
            System.out.println(String.format("%02d", getHours()) + ":" +
                    String.format("%02d", getMinutes()) + ":" +
                    String.format("%02d", getSeconds()));
        }
    }

    /**
     * Метод для вывода на экран времени в формате hh:mm:ss am/pm
     */
    public void showTimeAmPm() {
        if ((getHours() < 0 || getHours() >= 24) || (minutes < 0 || minutes >= 60) || (seconds < 0 || seconds >= 60))
            System.out.println("Введены недопустимые значения.");
        else {
            if (getHours() >= 12) {
                this.hoursAmPm = this.hours - 12;
                System.out.println(String.format("%02d", getHoursAmPm()) + ":" +
                        String.format("%02d", getMinutes()) + ":" +
                        String.format("%02d", getSeconds()) + " pm");
            } else
                System.out.println(String.format("%02d", getHours()) + ":" +
                        String.format("%02d", getMinutes()) + ":" +
                        String.format("%02d", getSeconds()) + " am");
        }
    }

    /**
     * Метод, который прибавляет переданное время к установленному в
     * TimeUnit (на вход передаются только часы, минуты и секунды).
     */
    public void addTime(int hours, int minutes, int seconds) {
        this.hours += hours;
        if (this.hours >= 24)
            this.hours -= 24;
        this.minutes += minutes;
        if (this.minutes >= 60)
            this.minutes -= 60;
        this.seconds += seconds;
        if (this.seconds >= 60)
            this.seconds -= 60;
    }

    /**
     * Метод main()
     */
    public static void main(String[] args) {
        TimeUnit timeUnit = new TimeUnit(5, 5, 5);
        timeUnit.showTime();
        timeUnit.showTimeAmPm();
        timeUnit.addTime(12, 25, 15);
        timeUnit.showTime();
        timeUnit.showTimeAmPm();
    }
}
