package dz.part3_1.task6;

/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:

Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String

Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class AmazingString {

    private String string;
    private char[] charArray;

    // Принимает на вход массив char
    AmazingString(char[] charArray) {
        this.charArray = charArray;
    }

    // Принимает на вход String
    AmazingString(String string) {
        this.string = string;
    }

    // Вернуть i-ый символ строки
    public void symbolString(int indexChar) {
        System.out.println(string.charAt(indexChar));
    }

    // Вернуть длину строки
    public void stringLength() {
        // вариант 1
        System.out.println("Длина строки string.length(): " + string.length());

        // вариант 2
        int current = 0;
        for (int i = 0; i < string.length(); i++) {
            current++;
        }
        System.out.println("Длина строки current: " + current);
    }

    // Вывести строку на экран
    public void printString() {
        System.out.println(string);
    }

    // Проверить, есть ли переданная подстрока в AmazingString (на вход
    // подается массив char). Вернуть true, если найдена и false иначе
    public void findSubstringCharArray(char[] charArray) {
        String s = new String(charArray);
        System.out.println(string.contains(s));
    }

    // Проверить, есть ли переданная подстрока в AmazingString (на вход
    // подается String). Вернуть true, если найдена и false иначе
    public void findSubstringString(String newString) {
        System.out.println(string.contains(newString));
    }

    // Удалить из строки AmazingString ведущие пробельные символы, если они есть
    public void spaceTrim(String trimS) {
        // вариант 1
        System.out.println(trimS.trim());

        // вариант 2
        while ((trimS.charAt(0) == ' ')) {
            System.out.println(trimS);
            trimS = trimS.substring(1);
        }
        System.out.println(trimS);
    }

    // Развернуть строку (первый символ должен стать последним, а последний первым и т.д.
    public void reverseString() {
        char[] stringToChar = string.toCharArray();
        for (int i = 0, j = stringToChar.length - 1; i < stringToChar.length / 2; i++, j--) {
            char temp = stringToChar[i];
            stringToChar[i] = stringToChar[j];
            stringToChar[j] = temp;
        }
        String reverseStr = new String(stringToChar);
        System.out.println(reverseStr);
    }

    /**
     * Метод main()
     */
    public static void main(String[] args) {

        AmazingString stringAmazing = new AmazingString("Привет");

        // Метод поиска символа в строке
        stringAmazing.symbolString(2);

        // Вернуть длину строки
        stringAmazing.stringLength();

        // Вывести строку на экран
        stringAmazing.printString();

        // Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char)
        stringAmazing.findSubstringCharArray(new char[]{'П', 'р', 'и', 'в', 'е', 'т'});

        // Проверить, есть ли переданная подстрока в AmazingString (на вход подается String)
        stringAmazing.findSubstringString("До встречи");

        // Удалить из строки AmazingString ведущие пробельные символы, если они есть
        stringAmazing.spaceTrim("   ведущие пробельные символы");

        // Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
        stringAmazing.reverseString();
    }
}