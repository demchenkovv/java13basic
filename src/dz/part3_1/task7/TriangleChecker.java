package dz.part3_1.task7;

/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false.
Входные длины сторон треугольника — числа типа double.
Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */
public class TriangleChecker {
    private static double a;
    private static double b;
    private static double c;
    public static final double EPSILON = 0.1E-5;

    public static void setSides(double a, double b, double c) {
        TriangleChecker.a = a;
        TriangleChecker.b = b;
        TriangleChecker.c = c;
    }

    public static void main(String[] args) {
        TriangleChecker.setSides(3.2, 2.2, 5.3);
        TriangleChecker.testToTriangleInequality1();
        TriangleChecker.testToTriangleInequality2();
        TriangleChecker.testToTriangleInequality3();
        TriangleChecker.testToTriangleInequality4();
        System.out.println(EPSILON);

        // Еще метод, если сумма углов треугольника равна 180°, значит треугольник существует.
        // Но такие входные данные отсутствуют по условию задачи

    }

    // Тест 1
    // Проверить, что числа положительные - true, иначе false
    public static void testToTriangleInequality1() {
        boolean isNumberPositive = a > 0 && b > 0 && c > 0;
        System.out.println(isNumberPositive);
    }

    // Тест 2
    // Если каждая сторона треугольника меньше суммы двух других сторон,
    // значит треугольник существует - true, иначе false
    public static void testToTriangleInequality2() {
        boolean isTriangleInequality = b + c > a + EPSILON && a + c > b + EPSILON && a + b > c + EPSILON;
        System.out.println(isTriangleInequality);
    }

    // Тест 3
    // Метод false. Обратный первому. Если хотя бы одна из сторон больше
    // или равны сумме двух других сторон - false, иначе true
    public static void testToTriangleInequality3() {
        boolean isFalseMethod = b + c <= a || a + c  <= b || a + b  <= c;
        System.out.println(isFalseMethod);
    }

    // Тест 4
    // Взять большую сторону и сравнить с суммой двух меньших сторон
    // Если треугольник существует true, иначе false
    public static void testToTriangleInequality4() {
        if (a >= b && a >= c) {
            System.out.println(a < b + c);
        } else if (b >= a && b >= c) {
            System.out.println(b < a + c);
        } else {
            System.out.println(c < a + b);
        }
    }
}
