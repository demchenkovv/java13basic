package dz.pm_3.task2;

import dz.pm_2.task4.Document;
import dz.pm_3.task1.Main;
import dz.pm_3.task1.Test;

import java.lang.annotation.Annotation;

public class CheckAnnotations {
    public CheckAnnotations(Class<?> clazz) {
        checkAnnotation(clazz);
    }

    public static void checkAnnotation(Class<?> clazz) {
        Annotation[] annotations = clazz.getDeclaredAnnotations();
        System.out.println(clazz.getSimpleName());
        if (annotations.length == 0)
            System.out.println("annotations not found");
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }
        System.out.println("");
    }

    /** Метод main */
    public static void main(String[] args) {
        checkAnnotation(Main.class);
        checkAnnotation(Test.class);
        checkAnnotation(Document.class);
    }
}
