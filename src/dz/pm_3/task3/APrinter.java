package dz.pm_3.task3;

import java.lang.reflect.Method;
import java.util.Scanner;

public class APrinter {
    public APrinter() {
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter the int value: ");
        try {
            invokePrintReflection(APrinter.class, input.nextInt());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void invokePrintReflection(Class<?> klass, int intValue) {
        Method[] methods = klass.getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().equals("print")) {
                try {
                    m.invoke(klass.newInstance(), intValue);
                } catch (Exception e) {
                    throw new RuntimeException("APrinter#invokePrintReflection!error: " + e.getMessage());
                }
            }
        }
    }

    public void print(int a) {
        System.out.println(a);
    }
}
