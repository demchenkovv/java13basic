package dz.pm_3.task6;

public class Main {
    public static void main(String[] args) {

        String s1 = "{()[]()}";
        String s2 = "{)(}";
        String s3 = "[}";
        String s4 = "[{(){}}][()]{}";
        String s5 = "";

        System.out.println(s1 + " - " + isSequenceBrackets(s1));
        System.out.println(s2 + " - " + isSequenceBrackets(s2));
        System.out.println(s3 + " - " + isSequenceBrackets(s3));
        System.out.println(s4 + " - " + isSequenceBrackets(s4));
        System.out.println("Пустая строка s5 - " + s5 + " - " + isSequenceBrackets(s5));
    }

    public static boolean isSequenceBrackets(String s) {
        if (s.isEmpty())
            return true;

        String temp = s;
        // (\(\))?(\[\])?(\{\})?
        while (!temp.isEmpty()) {
            if (temp.contains("()")) {
                temp = temp.replace("()", "");
            }
            else if (temp.contains("[]")) {
                temp = temp.replace("[]", "");
            }
            else if (temp.contains("{}")) {
                temp = temp.replace("{}", "");
            } else {
                return false;
            }
        }
        return true;
    }
}
