package dz.pm_3.task4;

import java.util.*;

public class Main
        extends F {
    public static void main(String[] args) {

        HashSet<Class<?>> result1 = showAllInterfaces(Main.class);
        System.out.println("Main.class: ");
        for (Class<?> c : result1) {
            System.out.println(c.getSimpleName());
        }

        HashSet<Class<?>> result2 = showAllInterfaces(LinkedList.class);
        System.out.println("\nLinkedList.class: ");
        for (Class<?> c : result2) {
            System.out.println(c.getSimpleName());
        }
    }

    public static HashSet<Class<?>> showAllInterfaces(Class<?> clazz) {
        List<Class<?>> listInterfaces = new ArrayList<>();
        while (clazz != Object.class) {
            listInterfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            clazz = clazz.getSuperclass();
        }
        for (int j = 0; j < listInterfaces.size(); j++) {
            Class<?> temp = listInterfaces.get(j);
            listInterfaces.addAll(Arrays.asList(temp.getInterfaces()));
        }
        return new HashSet<>(listInterfaces);
    }
}