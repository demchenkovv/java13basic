package dz.part1_1;

/*На вход подается количество секунд, прошедших с начала текущего дня – count.
Выведите в консоль текущее время в формате: часы и минуты.
Ограничения:
0 < count < 86400*/

import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int count, hours, minutes;

        count = input.nextInt();
        hours = count / 60 / 60;
        minutes = count / 60 % 60;

        System.out.println(hours + " " + minutes);

    }
}