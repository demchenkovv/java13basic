package dz.part1_1;

/* Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров).
На вход подается количество дюймов, выведите количество сантиметров.
Ограничения: 0 < count < 1000 */

import java.util.Scanner;

public class Task5 {
    public static final double CENTIMETERS_PER_INCH = 2.54;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int inch = input.nextInt();
        double cm = inch * CENTIMETERS_PER_INCH;

        System.out.println(cm);

    }
}