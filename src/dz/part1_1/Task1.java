package dz.part1_1;

/* Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
Подсказка: считать по формуле V = 4/3 * pi * r^3.
Значение числа pi взять из Math. Ограничения: 0 < r < 100
Например, вх. 9; вых 3053.6280592892786 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double r = input.nextDouble();
        double v = 4 / 3.0 * Math.PI * Math.pow(r, 3);

        System.out.println(v);

    }
}