package dz.part1_1;

/* На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k
тугриков. Вычислите и выведите, сколько гостей можно пригласить на
мероприятие. Ограничения:
0 < n < 100000
0 < k < 1000
k < n */

import java.util.Scanner;

public class Task9 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n, k, welcome;

        n = input.nextInt();
        k = input.nextInt();
        welcome = n / k;

        System.out.println(welcome);

    }
}