package dz.part1_1;

/*На вход подается двузначное число n. Выведите число, полученное
перестановкой цифр в исходном числе n. Если после перестановки получается
ведущий 0, его также надо вывести.
Ограничения: 9 < count < 100 */

import java.util.Scanner;

public class Task7 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n, tens, ones;

        n = input.nextInt();
        tens = n / 10;
        ones = n % 10;

        // v1
        System.out.print(ones);
        System.out.println(tens);

        // v2
        System.out.print(ones + "" + tens);

    }
}