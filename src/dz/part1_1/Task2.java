package dz.part1_1;

/* На вход подается два целых числа a и b.
Вычислите и выведите среднее квадратическое a и b.
Например, вх. 23 70; вых. 52.100863716449076 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        byte a, b;
        double res;

        a = input.nextByte();
        b = input.nextByte();

        res = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2.0);

        System.out.println(res);

    }
}