package dz.part1_1;

/*На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30
дней. Ограничения: 0 < count < 100000*/

import java.util.Scanner;

public class Task8 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n;
        double b;

        n = input.nextInt();
        b = n / 30.0;

        System.out.println(b);

    }
}