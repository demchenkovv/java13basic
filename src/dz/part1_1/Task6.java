package dz.part1_1;

/*На вход подается количество километров count.
Переведите километры в мили (1 миля = 1,60934 км) и выведите количество миль.
Ограничения: 0 < count < 1000 */

import java.util.Scanner;

public class Task6 {
    public static final double KILOMETERS_PER_MILES = 1.60934;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int count = input.nextInt();
        double miles = count / KILOMETERS_PER_MILES;

        System.out.println(miles);

    }
}