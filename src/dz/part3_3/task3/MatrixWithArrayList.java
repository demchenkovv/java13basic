package dz.part3_3.task3;

import java.util.ArrayList;

public class MatrixWithArrayList {

    public static void inputAndOutputMatrix(int n, int m) {
        // Создание двумерного динамического массива ArrayList
        ArrayList<ArrayList<Integer>> arrayLists = new ArrayList<>();
        ArrayList<Integer> list;

        for (int i = 0; i < m; i++) {
            list = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                list.add(i + j);
            }
            arrayLists.add(list);
        }
        for (ArrayList<Integer> i : arrayLists) {
            for (Integer currValue : i) {
                System.out.print(currValue + " ");
            }
            System.out.println("");
        }
    }
}