package dz.part3_3.task3;
import java.util.Scanner;
/**
 * На вход передается N — количество столбцов в двумерном массиве и M —
 * количество строк. Необходимо вывести матрицу на экран, каждый элемент
 * которого состоит из суммы индекса столбца и строки этого же элемента. Решить
 * необходимо используя ArrayList.
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int m = input.nextInt();
        MatrixWithArrayList.inputAndOutputMatrix(n, m);
    }
}