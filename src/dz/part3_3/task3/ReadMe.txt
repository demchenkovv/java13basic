import java.util.ArrayList;
import java.util.Scanner;

public class MatrixWithArrayList {
    public static void main(String args[]) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int m = input.nextInt();

        ArrayList<ArrayList<Integer>> arr = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            arr.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                arr.get(i).add(i + j);
                System.out.print(arr.get(i).get(j) + " ");
            }
            System.out.println();
        }

//        Альтернативный вариант вывода информации в консоль
//        for (int i = 0; i < m; i++) {
//            for (int j = 0; j < n; j++) {
//                System.out.print(arrayLists.get(i).get(j) + " ");
//            }
//            System.out.println();
//        }
    }
}