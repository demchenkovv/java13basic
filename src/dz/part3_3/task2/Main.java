package dz.part3_3.task2;

/**
 * Метод main
 */
public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();

        Furniture furniture1 = new Stools();
        // умеет чинить только Табуретки - true
        System.out.println(bestCarpenterEver.isFixIt(furniture1));

        Furniture furniture2 = new Tables();
        // иное - false
        System.out.println(bestCarpenterEver.isFixIt(furniture2));
    }
}