package dz.part3_3.task4;

import java.util.ArrayList;

public class Participant implements Comparable<Participant> {
    private String nameMan;
    private Dog dog;
    private ArrayList<Integer> marks = new ArrayList<>();

    public Participant() {

    }

    public Participant(String nameMan) {
        this.nameMan = nameMan;
    }

    public String getNameMan() {
        return nameMan;
    }

    public void setNameMan(String nameMan) {
        this.nameMan = nameMan;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public ArrayList<Integer> getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks.add(marks);
    }

    public double getAverageMark() {
        double sum = 0;
        for (int i = 0; i < marks.size(); i++) {
            sum += marks.get(i);
        }
        return (int) (sum / marks.size() * 10) / 10.0;
    }

    @Override
    public int compareTo(Participant o) {
        return getAverageMark() > o.getAverageMark() ? 1 : (getAverageMark() < o.getAverageMark() ? -1 : 0);
    }
}