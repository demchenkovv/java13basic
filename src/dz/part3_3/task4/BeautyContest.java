package dz.part3_3.task4;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class BeautyContest {
    private ArrayList<Participant> participantsList = new ArrayList<>();
    private ArrayList<Dog> dogsList = new ArrayList<>();


    public void addParticipants(int value) {
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < value; i++) {
            System.out.print("Имя владельца и кличка собаки № " + (i + 1) + ": ");
            participantsList.add(new Participant(input.next()));
            dogsList.add(new Dog(input.next()));
        }
        for (Participant participant : participantsList) {
            for (Dog dog : dogsList) {
                if (dog.getParticipant() == null) {
                    dog.setParticipant(participant);
                    participant.setDog(dog);
                    break;
                }
            }
        }
    }

    public void addMarks() {
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < participantsList.size(); i++) {
            System.out.print("Оценки для участника № " + (i + 1) + ": ");
            for (int j = 0; j < 3; j++) {
                participantsList.get(i).setMarks(input.nextInt());
            }
        }
    }

    public void showParticipants() {
        System.out.println("\nУчастники и их оценки:");
        for (Participant e : participantsList)
            System.out.println(e.getNameMan() + " " + e.getDog().getNamePet() + " " + e.getMarks());
    }

    public void showWinners() {
        Collections.sort(participantsList);
        int i = 3;
        while (i > 0) {
            System.out.println(participantsList.get(i).getNameMan() + ": " +
                    participantsList.get(i).getDog().getNamePet() + ", " +
                    participantsList.get(i).getAverageMark());
            i--;
        }

        // Код ниже делает тоже самое, но был написан до прочтения темы ООП 2 - "Интерфейс Comparable"
        // Благодаря реализации интерфейса Comparable<Participant> в классе Participant и переопределению
        // метода compareTo(), который использует метод sort(), мы смогли отсортировать массив участников
        // по нужному нам параметру (ср. оценке). Оставил просто для сравнения.

//        ArrayList<Double> tempMarks = new ArrayList<>();
//
//        for (Participant participant : participantsList)
//            tempMarks.add(participant.getAverageMark());
//
//        Collections.sort(tempMarks);
//
//        System.out.println("\nПобедители: ");
//
//        for (Participant e : participantsList) {
//            if (e.getAverageMark() == tempMarks.get(tempMarks.size() - 1))
//                System.out.println(e.getNameMan() + ": " + e.getDog().getNamePet() + ", " + tempMarks.get(tempMarks.size() - 1));
//        }
//
//        for (Participant e : participantsList) {
//            if (e.getAverageMark() == tempMarks.get(tempMarks.size() - 2))
//                System.out.println(e.getNameMan() + ": " + e.getDog().getNamePet() + ", " + tempMarks.get(tempMarks.size() - 2));
//        }
//
//        for (Participant e : participantsList) {
//            if (e.getAverageMark() == tempMarks.get(tempMarks.size() - 3))
//                System.out.println(e.getNameMan() + ": " + e.getDog().getNamePet() + ", " + tempMarks.get(tempMarks.size() - 3));
//        }
    }
}
