package dz.part3_3.task4;

public class Dog {
    private String namePet;
    private Participant participant;

    public Dog() {

    }

    public Dog(String namePet) {
        this.namePet = namePet;
    }

    public String getNamePet() {
        return namePet;
    }

    public void setNamePet(String namePet) {
        this.namePet = namePet;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }
}
