package dz.part3_3.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BeautyContest beautyContest = new BeautyContest();

        System.out.print("Количество участников: ");
        int numberOfParticipants = input.nextInt();

        beautyContest.addParticipants(numberOfParticipants);
        beautyContest.addMarks();
        beautyContest.showParticipants();
        beautyContest.showWinners();
    }
}
