// Альтернативный вариант вывода информации о победителях. Очень интересное последнее решение с forEach

    public void showWinners() {
        List<Double> overallGrades = new ArrayList<>();

        for (Participant participant : participants) {
            overallGrades.add(participant.overallGrade());
        }
        double largest = Integer.MIN_VALUE, secondLargest = Integer.MIN_VALUE, thirdLargest = Integer.MIN_VALUE;

        for (Double overallGrade : overallGrades) {
            if (overallGrade > largest) {
                thirdLargest = secondLargest;
                secondLargest = largest;
                largest = overallGrade;
            } else if (overallGrade > secondLargest) {
                thirdLargest = secondLargest;
                secondLargest = overallGrade;
            } else if (overallGrade > thirdLargest) {
                thirdLargest = overallGrade;
            }
        }

        double finalLargest = largest;
        participants.forEach(p -> System.out.print(finalLargest == p.overallGrade() ? p.getName() + ": " +
                p.getDog().getName() + ", " + finalLargest : ""));
        double finalSecondLargest = secondLargest;
        System.out.println();
        participants.forEach(p -> System.out.print(finalSecondLargest == p.overallGrade() ? p.getName() + ": " +
                p.getDog().getName() + ", " + finalSecondLargest : ""));
        double finalThirdLargest = thirdLargest;
        System.out.println();
        participants.forEach(p -> System.out.print(finalThirdLargest == p.overallGrade() ? p.getName() + ": " +
                p.getDog().getName() + ", " + finalThirdLargest : ""));
    }
}


А еще интереснее с переопределенным методом compareTo(Participant o).
    @Override
    public int compareTo(Participant o) {
        return getAverageMark() > o.getAverageMark() ? 1 : (getAverageMark() < o.getAverageMark() ? -1 : 0);
    }

Логика сортировки метода sort() и участия в нем метода compareTo(), который возвращает -1, 0 или 1
была описана в курсе СберУниверситета https://partner.sberbank-school.ru/programs/18940/item/887017

Класс Participant реализует интерфейс Comparable с типом <Participant>
Ключевое слово implements указывает на то, что Participant наследует
все константы из интерфейса Comparable и реализует все методы в интерфейсе.
Метод compareTo() сравнивает среднюю оценку Participant.
Экземпляр класса Participant также является объектом типа Object и Comparable.