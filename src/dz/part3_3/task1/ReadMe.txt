// В методе main можно было создать массив объектов следующим образом

    public static void main(String[] args) {
        Animal[] arrayAnimal = {new Bat(), new Dolphin(), new Eagle(), new GoldFish()};
        for (Animal e : arrayAnimal)
            testActions(e);

// Альтернативный вариант через упорядоченную коллекцию (также известная как последовательность).
// Пользователь этого интерфейса имеет точный контроль над тем, куда в списке вставляется каждый элемент.

        List<Animal> list = Arrays.asList(new Bat(), new Dolphin(), new Eagle(), new GoldFish());
        for (Animal e : list)
            testActions(e);


// Третий вариант добавлять объекты через list.add()
        List<Animal> list = new ArrayList<>();
        list.add(new Bat());
        ...