package dz.part3_3.task1;

public abstract class Animal {

    protected final void eat() {
        System.out.println("Есть");
    }

    protected final void sleep() {
        System.out.println("Спать");
    }

    protected abstract void wayOfBirth();
}
