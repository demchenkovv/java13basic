package dz.part3_3.task1;

public class Dolphin extends Fish
        implements ISwimming {

    @Override
    public void swim() {
        System.out.println("Дельфин плывет быстро");
    }
}
