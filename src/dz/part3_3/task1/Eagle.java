package dz.part3_3.task1;

public class Eagle extends Bird
        implements IFlying {

    @Override
    public void fly() {
        System.out.println("Орёл летит быстро");
    }
}
