package dz.part3_3.task1;

public class Bat extends Bird
        implements IFlying {

    @Override
    public void fly() {
        System.out.println("Летучая мышь летит медленно");
    }
}
