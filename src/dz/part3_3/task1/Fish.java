package dz.part3_3.task1;

abstract class Fish extends Animal {
    @Override
    protected void wayOfBirth() {
        System.out.println("Рыбы (Fish) мечут икру.");
    }
}
