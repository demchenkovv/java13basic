package dz.part3_3.task1;

abstract class Mammal extends Animal {
    @Override
    protected void wayOfBirth() {
        System.out.println("Млекопитающие (Mammal) живородящие.");
    }
}
