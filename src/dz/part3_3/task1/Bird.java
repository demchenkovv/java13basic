package dz.part3_3.task1;

abstract class Bird extends Animal {
    @Override
    protected void wayOfBirth() {
        System.out.println("Птицы (Bird) откладывают яйца");
    }
}
