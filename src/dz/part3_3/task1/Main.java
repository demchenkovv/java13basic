package dz.part3_3.task1;

import java.util.Arrays;
import java.util.List;

/** Метод main */
public class Main {
    public static void main(String[] args) {
        List<Animal> list = Arrays.asList(new Bat(), new Dolphin(), new Eagle(), new GoldFish());
        for (Animal e : list)
            testActions(e);
    }
/** Тестирование методов, реализованные в суперклассе Animal и его
 * подклассах, с реализацией интерфейсов IFlying и ISwimming */
    public static void testActions(Animal a) {
        System.out.println("");
        a.sleep();
        a.eat();
        a.wayOfBirth();
        if (a instanceof Bat)
            ((Bat) a).fly();
        if (a instanceof Dolphin)
            ((Dolphin) a).swim();
        if (a instanceof Eagle)
            ((Eagle) a).fly();
        if (a instanceof GoldFish)
            ((GoldFish) a).swim();
    }
}
