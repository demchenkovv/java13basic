package dz.part3_3.task1;

public class GoldFish extends Fish
        implements ISwimming {

    @Override
    public void swim() {
        System.out.println("Золотая рыбка плывет медленно");
    }
}
