package firstweekpractice;

/*
        Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
        Входные данные
        a = 8; b = 10
     */


import java.util.Scanner;

public class Task5_swap {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println("Входные данные");
        System.out.println("a=" + a + " ;b=" + b);

        //логика
        a = a + b; // 2 + 5 = 7
        b = a - b; // 7 - 5 = 2
        a = a - b; // 7 - 2 = 5

        System.out.println("Результат");
        System.out.println("a=" + a + " ;b=" + b);
    }
}
