package lessons.oop.part6.task10;
// Напишите клиент этого класса — программу, которая создает два объекта типа MyDate (с помощью new MyDate()
// и new MyDate(34355555133101L)) и отображает их год, месяц и день.
public class TestMyDate {
    public static void main(String[] args) {
        MyDate date = new MyDate();
        System.out.println("год: " + date.getYear());
        System.out.println("месяц: " + date.getMonth());
        System.out.println("день: " + date.getDay());

        date.setDate(34355555133101L);
        System.out.println("год: " + date.getYear());
        System.out.println("месяц: " + date.getMonth());
        System.out.println("день: " + date.getDay());

        date = new MyDate(34355555133101L);
        System.out.println("год: " + date.getYear());
        System.out.println("месяц: " + date.getMonth());
        System.out.println("день: " + date.getDay());
    }
}
