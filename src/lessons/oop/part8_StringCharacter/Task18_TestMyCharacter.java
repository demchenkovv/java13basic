package lessons.oop.part8_StringCharacter;

/*
public MyCharacter(char value);
public char charValue();
public int compareTo(MyCharacter anotherCharacter);
public boolean equals(MyCharacter anotherCharacter);
public boolean isDigit();
public static boolean isDigit(char ch);
public static boolean isLetter(char ch);
public static boolean isLetterOrDigit(char ch);
public static boolean isLowerCase(char ch);
public static boolean isUpperCase(char ch);
public static char toUpperCase(char ch);
public static char toLowerCase(char ch);
 */
class MyCharacter {
    private char value;

    public MyCharacter(char value) {
        this.value = value;
    }

    public char charValue() {
        return value;
    }

    public int compareTo(MyCharacter anotherCharacter) {
        return anotherCharacter.value;
    }

    public boolean equals(MyCharacter anotherCharacter) {
        return value == anotherCharacter.value;
    }

    public boolean isDigit() {
        return isDigit(value);
    }

    public static boolean isDigit(char ch) {
        return ch <= '9' && ch >= '0';
    }

    public static boolean isLetter(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    public static boolean isLetterOrDigit(char ch) {
        return isDigit(ch) || isLetter(ch);
    }

    public static boolean isLowerCase(char ch) {
        return ch >= 'a' && ch <= 'z';
    }

    public static boolean isUpperCase(char ch) {
        return ch >= 'A' && ch <= 'Z';
    }

    public static char toUpperCase(char ch) {
        if (isLowerCase(ch)) {
            return (char) (ch - 'a' + 'A');
        }
        return ch;
    }

    public static char toLowerCase(char ch) {
        if (isUpperCase(ch)) {
            return (char) (ch - 'A' + 'a');
        }
        return ch;
    }

}

public class Task18_TestMyCharacter {
    public static void main(String[] args) {
        MyCharacter myCharacter1 = new MyCharacter('1');
        System.out.println(MyCharacter.isDigit('t'));
        System.out.println(MyCharacter.isLetter('t'));
        System.out.println(MyCharacter.isLetterOrDigit('t'));
        System.out.println(MyCharacter.isLowerCase('T'));
        System.out.println(MyCharacter.isUpperCase('T'));
        System.out.println(MyCharacter.toUpperCase('q'));
        System.out.println(MyCharacter.toLowerCase('Y'));
    }
}