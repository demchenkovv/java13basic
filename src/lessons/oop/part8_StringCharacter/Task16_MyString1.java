package lessons.oop.part8_StringCharacter;

/*
1. public MyString1(char[] chars);
2. public char charAt(int index);
3. public int length();
4. public MyString1 substring(int begin, int end);
5. public MyString1 toLowerCase();
6. public static MyString1 valueOf(int i);
7. public char[] toChars();
8. public boolean equals(MyString1 obj);
 */
public class Task16_MyString1 {
    public static char[] chars = {'H', 'e', 'l', 'l', 'o'};
    public static String message = "Привет";
    private String tempMessage;

    Task16_MyString1(String newMessage) {
        this.tempMessage = newMessage;
    }

    public static void main(String[] args) {
        System.out.println(myString1(chars));
        System.out.println(charAt(2));
        System.out.println(length());
        System.out.println(substring(1, 3));
        System.out.println(toLowerCase());
        System.out.println(valueOf(4));
        System.out.println(toChars());
        Task16_MyString1 task16MyString1 = new Task16_MyString1("Прощай");
        System.out.println(isEquals(task16MyString1));
    }

    // 1. Принимает массив символов chars, создает новый строковый объект
    // со значениями chars[] и возвращает строку
    public static String myString1(char[] chars) {
        return new String(chars);
    }

    // 2. Принимает число индекса, и возвращает символ массива chars[]
    public static char charAt(int index) {
        return chars[index];
    }

    // 3. Возвращает длину массива chars[]
    public static int length() {
        return chars.length;
    }

    // 4. Возвращает подстроку с индекса begin по индекс end
    public static String substring(int begin, int end) {
        String s = new String(chars);
        return s.substring(begin, end);
    }

    // 5. Возвращает строку со всеми строчными буквами
    public static String toLowerCase() {
        String s = new String(chars);
        return s.toLowerCase();
    }

    // 6. Принимает число i и возвращает строку со значением i
    public static String valueOf(int i) {
        return String.valueOf(i);
    }

    // 7. Возвращает строку в массив символов
    public static char[] toChars() {
        return message.toCharArray();
    }

    // 8. public boolean equals(MyString1 obj);
    public static boolean isEquals(Task16_MyString1 obj) {
        return obj.tempMessage.equals(Task16_MyString1.message);
    }

}
