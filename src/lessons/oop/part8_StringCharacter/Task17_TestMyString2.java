package lessons.oop.part8_StringCharacter;

import java.util.Arrays;

/*
public MyString2(char[] chars);
public MyString2 substring(int begin);
public MyString2 toUpperCase();
public char[] toChars();
public static MyString2 valueOf(boolean b);
 */
public class Task17_TestMyString2 {
    public static void main(String[] args) {
        MyString2 myString2 = new MyString2(new char[] {'a', 'b', 'c', 'd', 'e'});
        myString2.substring(2);
        myString2.toUpperCase();
        myString2.toChars();
        char[] chars = MyString2.valueOf(false).toChars();
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i]);
        }

    }
}

class MyString2 {
    private char[] chars;

    public MyString2(char[] chars) {
        this.chars = new char[chars.length];
        System.arraycopy(chars,0,this.chars,0, chars.length);
    }

    public MyString2 substring(int begin) {
        char[] temp = new char[chars.length - begin];
        for (int i = begin; i < chars.length; i++) {
            temp[i - begin] = chars[i];
        }
        System.out.println(Arrays.toString(temp));
        return new MyString2(temp);
    }

    public MyString2 toUpperCase(){
        char[] temp = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            temp[i] = Character.toUpperCase(chars[i]);
            System.out.print(temp[i] + " ");
        }
        return new MyString2(temp);
    }

    public char[] toChars() {
        return chars;
    }

    public static MyString2 valueOf(boolean b){
        if (b)
            return new MyString2(new char[] {'t','r','u','e'});
        else
            return new MyString2(new char[] {'f','a','l','s','e'});
    }
}
