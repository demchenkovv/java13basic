package lessons.oop.part1.task1_rectangle;

class Rectangle {
    // Поле данных
    double width;
    double height;

    // Безаргументный конструктор, создающий прямоугольник с указанными по умолчанию значениями
    Rectangle() {
        width = -1;
        height = -1;
    }

    // Конструктор, создающий прямоугольник с указанными шириной и высотой
    Rectangle(double newWidth, double newHeight) {
        width = newWidth;
        height = newHeight;
    }

    // Метод с именем getArea(), возвращающий площадь этого прямоугольника
    double getArea() {
        return width * height;
    }

    // Метод с именем getPerimeter(), возвращающий периметр
    double getPerimeter() {
        return (width + height) * 2;
    }
}
