package lessons.oop.part1.task1_rectangle;

/*
По примеру класса Circle, создайте класс Rectangle для представления прямоугольника.
Класс Rectangle должен содержать:
Два поля данных типа double с именами width и height, задающими ширину и высоту прямоугольника.
+ Значение по умолчанию: -1 как для ширины, так и для высоты.
+ Безаргументный конструктор, создающий прямоугольник с указанными по умолчанию значениями.
+ Конструктор, создающий прямоугольник с указанными шириной и высотой.
+ Метод с именем getArea(), возвращающий площадь этого прямоугольника.
+ Метод с именем getPerimeter(), возвращающий периметр.

Напишите клиент этого класса — программу, которая создает два объекта типа Rectangle:
первый — с шириной 4 и высотой 40, а второй - с шириной 3.5 и высотой 35.9.
Программа также должна отображать ширину, высоту, площадь и периметр каждого прямоугольника в указанном порядке.
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4, 40);
        System.out.println("При ширине " + rectangle1.width + " и высоте " + rectangle1.height + " площадь равна "
                + rectangle1.getArea() + " и периметр равен " + rectangle1.getPerimeter());
        Rectangle rectangle2 = new Rectangle(3.5, 35.9);
        System.out.println("При ширине " + rectangle2.width + " и высоте " + rectangle2.height + " площадь равна "
                + rectangle2.getArea() + " и периметр равен " + rectangle2.getPerimeter());
    }
}

