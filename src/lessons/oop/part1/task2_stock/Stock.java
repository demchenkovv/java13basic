package lessons.oop.part1.task2_stock;

class Stock {
    String symbol;
    String name;
    double previousClosingPrice = 281.50;
    double currentPrice = 282.87;
    double changePercent;
    Stock(String newSymbol, String newName){
        symbol = newSymbol;
        name = newName;
    }
    double getChangePercent() {
        return changePercent = currentPrice / previousClosingPrice;
    }
}
