package lessons.oop.part7_BigInteger;

import java.math.BigInteger;

// Найдите первые 10 чисел с 50 десятичными цифрами, которые кратны 2 или 3.
// BigInteger bigNum = new BigInteger("10000000000000000000000000000000000000000000000000");
public class Task11 {
    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger("10000000000000000000000000000000000000000000000000");
        int i = 0;
        while (i < 10) {
            if (bigInteger.remainder(new BigInteger("2")).equals(BigInteger.ZERO) ||
                bigInteger.remainder(new BigInteger("3")).equals(BigInteger.ZERO)) {
                System.out.println(bigInteger);
                i++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }
}
