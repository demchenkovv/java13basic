package lessons.oop.part7_BigInteger;

import java.math.BigDecimal;

// Найдите первые 10 квадратных чисел, которые больше Long.MAX_VALUE.
// Квадратное число — число в виде n^2. Например, 4, 9 и 16 являются квадратными числами.
public class Task12 {
    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal(Long.MAX_VALUE);
        int current = 0;
        while (current < 10) {
            System.out.println(bigDecimal = bigDecimal.multiply(bigDecimal));
            current++;
        }
    }
}
