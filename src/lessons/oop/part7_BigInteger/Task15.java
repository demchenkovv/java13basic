package lessons.oop.part7_BigInteger;

import java.math.BigInteger;

/*
Задание №15
Найдите первые 10 чисел, больших Long.MAX_VALUE, которые кратны 5 или 6.
 */
public class Task15 {
    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger(Long.MAX_VALUE + "");
        int count = 0;
        while (count < 5) {
            if (findMax(bigInteger)) {
                System.out.println(bigInteger);
                count++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }

    public static boolean findMax(BigInteger newBigInteger) {
        BigInteger divided5 = new BigInteger("5");
        BigInteger divided6 = new BigInteger("6");
        return (newBigInteger.remainder(divided5).equals(BigInteger.ZERO) || newBigInteger.remainder(divided6).equals(BigInteger.ZERO));
    }
}
