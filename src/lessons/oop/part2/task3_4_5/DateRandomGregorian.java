package lessons.oop.part2.task3_4_5;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class DateRandomGregorian {
    public static void main(String[] args) {
        // Класс Date
        Date date = new Date();
        System.out.println("Время, прошедшее с 1 января 1970 г., равно " +
                date.getTime() + " миллисекунд.");
        System.out.println(date.toString());

        //Класс Random - 5 случайных чисел
        Random generator1 = new Random(1);
        System.out.print("Из generator1: ");
        for (int i = 0; i < 5; i++)
            System.out.print(generator1.nextInt(1000) + " "); // случайные числа от 0 до 999;

        Random generator2 = new Random(1);
        System.out.print("\nИз generator2: ");
        for (int i = 0; i < 5; i++)
            System.out.print(generator2.nextInt(10) + " "); // случайные числа от 0 до 9;
        System.out.println();


        // ЗАДАНИЯ. Часть 2
        System.out.println("\nЗАДАНИЯ. Часть 2");

        // Задание №3: Класс Date
        //Напишите программу, которая создает объект Date, устанавливает у него прошедшее время, равным 10000,
        // 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000 и 100000000000, и
        // отображает дату и время с помощью метода toString(), соответственно.
        System.out.println("\nЗадание №3: Класс Date");
        Date date2 = new Date(10000);
        Date date3 = new Date(100000);
        Date date4 = new Date(1000000);
        Date date5 = new Date(10000000);
        Date date6 = new Date(100000000);
        Date date7 = new Date(1000000000);
        Date date8 = new Date(10000000000L);
        Date date9 = new Date(100000000000L);
        System.out.println(date2);
        System.out.println(date3);
        System.out.println(date4);
        System.out.println(date5);
        System.out.println(date6);
        System.out.println(date7);
        System.out.println(date8);
        System.out.println(date9);

        // Задание №4: Класс Random
        // Напишите программу, которая создает объект типа Random с начальным (случайным) значением 1000
        // и отображает первые 50 случайных целых чисел между 0 и 100 с помощью метода nextInt(100).
        System.out.println("\nЗадание №4: Класс Random");
        Random random = new Random(1000);
        for (int i = 0; i < 50; i++) {
            System.out.print(random.nextInt(100) + " ");
        }

        // Задание №5: Класс GregorianCalendar
        // У Java API в пакете java.util есть класс GregorianCalendar, который можно использовать для получения
        // года, месяца и дня даты. Безаргументный конструктор создает объект типа GregorianCalendar
        // для текущей даты, а методы get(GregorianCalendar.YEAR), get(GregorianCalendar.MONTH) и
        // get(GregorianCalendar.DAY_OF_MONTH) возвращают год, месяц (от 0 до 11) и день.
        // Напишите программу для выполнения следующих двух задач:
        //1. Отобразите текущий год, месяц и день.
        System.out.println("\n\nЗадание №5: Класс GregorianCalendar");
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        System.out.println(gregorianCalendar.get(GregorianCalendar.YEAR) + " " +
                gregorianCalendar.get(GregorianCalendar.MONTH) + " " +
                gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH));
        //2. У класса GregorianCalendar есть метод setTimeInMillis(long),
        // который можно использовать для задания указанного времени, прошедшего с 1 января 1970 г.
        // Установите значение, равное 1234567898765L, и отобразите для него год, месяц и день.
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
        gregorianCalendar2.setTimeInMillis(1234567898765L);
        System.out.println(gregorianCalendar2.get(GregorianCalendar.YEAR) + " " +
                gregorianCalendar2.get(GregorianCalendar.MONTH) + " " +
                gregorianCalendar2.get(GregorianCalendar.DAY_OF_MONTH));

    }
}
