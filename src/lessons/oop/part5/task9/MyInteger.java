package lessons.oop.part5.task9;

/*
Создайте класс с именем MyInteger, который должен содержать:

Поле данных value типа int, в котором хранится целочисленное значение, представленное этим объектом.
Конструктор, который создает объект типа MyInteger для указанного значения типа int.
Getter-метод, который возвращает значение типа int.
Методы isEven(), isOdd() и isPrime(), которые возвращают значение true,
если значение типа int в этом объекте является четным, нечетным или простым соответственно.
Статические методы isEven(int), isOdd(int) и isPrime(int), которые возвращают значение true,
если указанное значение является четным, нечетным или простым соответственно.
Статические методы isEven(MyInteger), isOdd(MyInteger) и isPrime(MyInteger),
которые возвращают значение true, если указанное значение является четным, нечетным или простым соответственно.
Методы equals(int) и equals(MyInteger), которые возвращают значение true,
если значение типа int в этом объекте равняется указанному значению.
Статический метод parseInt(char[]), который преобразует массив числовых символов в значение типа int.
Статический метод parseInt(String), который преобразует строку в значение типа int.
Нарисуйте UML-диаграмму класса MyInteger, а затем реализуйте этот класс.
Напишите клиент этого класса — программу, которая проверяет все методы этого класса.
 */
public class MyInteger {
    private int value;

    MyInteger(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    //Методы isEven(), isOdd() и isPrime(), которые возвращают значение true,
    // если значение типа int в этом объекте является четным, нечетным или простым соответственно.
    public boolean isEven() {
        return isEven(value);
    }

    public boolean isOdd() {
        return isOdd(value);
    }

    public boolean isPrime() {
        return isPrime(value);
    }

    // Статические методы isEven(int), isOdd(int) и isPrime(int), которые возвращают значение true,
    // если указанное значение является четным, нечетным или простым соответственно.
    public static boolean isEven(int num) {
        return num % 2 == 0;
    }

    public static boolean isOdd(int num) {
        return num % 2 != 0;
    }

    public static boolean isPrime(int num) {
        if (num < 2) return false;
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    // Статические методы isEven(MyInteger), isOdd(MyInteger) и isPrime(MyInteger),
    // которые возвращают значение true, если указанное значение является четным,
    // нечетным или простым соответственно.
    public static boolean isEven(MyInteger o) {
        return isEven(o.getValue());
    }

    public static boolean isOdd(MyInteger o) {
        return isOdd(o.getValue());
    }

    public static boolean isPrime(MyInteger o) {
        return isPrime(o.getValue());
    }

    public static int parseInt(char[] numbers) {
        // numbers состоит из символов цифр.
        // Например, если numbers равно {'1', '2', '5'}, то возвращаемое значение
        // должно быть равным 125. Обратите внимание, что
        // numbers[0] равно '1'
        // numbers[1] равно '2'
        // numbers[2] равно '5'
        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result = result * 10 + (numbers[i] - '0');
        }
        return result;
    }

    public static int parseInt(String s) {
        // s состоит из символов цифр.
        // Например, если s равно "125", то возвращаемое значение
        // должно быть равным 125.
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            result = result * 10 + (s.charAt(i) - '0');
        }
        return result;
    }
}