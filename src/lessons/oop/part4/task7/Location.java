package lessons.oop.part4.task7;

import java.util.Scanner;
/*
ЗАДАНИЯ. Часть 4
Задание №7: Класс Location
Создайте класс с именем Location для нахождения наибольшего элемента и его позиции в двумерном массиве.
Класс должен содержать public-поля row, column и maxValue, в которых будут храниться наибольший элемент
типа double и его индексы в двумерном массиве со строчками и столбцами типа int.

Напишите следующий метод, который возвращает позицию наибольшего элемента в двумерном массиве:
public static Location locateLargest(double[][] a)

Возвращаемое значение должно быть типа Location. Напишите клиент этого класса — программу, которая
предлагает пользователю ввести двумерный массив и отображает позицию наибольшего элемента в этом массиве.
Пример выполнения программы:
Введите количество строчек и столбцов массива: 3 4
Введите массив:
23.5 35 2 10
4.5 3 45 3.5
35 44 5.5 9.6
Наибольший элемент массива, равный 45.0, находится в позиции (1, 2)
 */
public class Location {
    public int row;
    public int column;
    public double maxValue;

    public Location() {
        this.row = 0;
        this.column = 0;
        this.maxValue = 0;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Location location = new Location();
        System.out.print("Введите количество строчек и столбцов массива: ");
        int n = input.nextInt();
        int m = input.nextInt();
        System.out.print("Введите массив: ");
        double[][] array = new double[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = input.nextDouble();
            }
        }
        locateLargest(array, location);

        System.out.print("Наибольший элемент массива, равный " + location.maxValue +
                " находится в позиции (" + location.row + ", " + location.column + ")");
    }

    public static Location locateLargest(double[][] array, Location location) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > location.maxValue) {
                    location.maxValue = array[i][j];
                    location.row = i;
                    location.column = j;
                }
            }
        }
        return location;
    }
}

// Можно было и так:

// public class TotalArea {
//  /** Метод main */
//  public static void main(String[] args) {
//    // Объявить массив circleArray
//    Circle[] circleArray;
//
//    // Создать объект circleArray
//    circleArray = createCircleArray();
//
//    // Отобразить circleArray и общую площадь всех кругов
//    printCircleArray(circleArray);
//  }
//
//  /** Создает массив объектов типа Circle */
//  public static Circle[] createCircleArray() {
//    Circle[] circleArray = new Circle[5];
//
//    for (int i = 0; i < circleArray.length; i++) {
//      circleArray[i] = new Circle(Math.random() * 100);
//    }
//
//    // Вернуть массив типа Circle
//    return circleArray;
//  }
//
//  /** Отображает массив кругов и их общую площадь */
//  public static void printCircleArray(Circle[] circleArray) {
//    System.out.println("Радиус\t\t\tПлощадь");
//    for (int i = 0; i < circleArray.length; i++) {
//      System.out.println(circleArray[i].getRadius() + "\t"
//        + circleArray[i].getArea());
//    }
//
//    System.out.println("-----------------------------------------");
//
//    // Вычислить и отобразить результат
//    System.out.print("Общая площадь равна \t" + sum(circleArray));
//  }
//
//  /** Складывает площади кругов */
//  public static double sum(Circle[] circleArray) {
//    // Инициализировать сумму
//    double sum = 0;
//
//    // Прибавить площади к сумме
//    for (int i = 0; i < circleArray.length; i++)
//      sum += circleArray[i].getArea();
//
//    return sum;
//  }
//}