package lessons.oop.part3.task6;

/*
Создайте класс с именем StopWatch для представления секундомера.
Класс StopWatch должен содержать:

+ Скрытые поля данных startTime и endTime с getter-методами.
+ Безаргументный конструктор, который инициализирует startTime с текущим временем.
+ Метод с именем start(), который сбрасывает startTime до текущего времени.
+ Метод с именем stop(), который присваивает endTime текущее время.
+ Метод с именем getElapsedTime(), который возвращает прошедшее время на секундомере в миллисекундах.
+ Нарисуйте UML-диаграмму класса StopWatch, а затем реализуйте этот класс.
+ Напишите клиент этого класса – программу, которая вычисляет время выполнения сортировки 100 000 чисел методом выбора.
 */
public class StopWatch {
    private long startTime;
    private long endTime = 0;

    public StopWatch() {
        startTime = System.currentTimeMillis();
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void stop() {
        endTime = System.currentTimeMillis();
    }

    public long getElapsedTime() {
        return endTime - startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }


    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println(stopWatch.startTime);
        int[] array = new int[100000];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }
        SelectionSort.selectionSort(array);
        stopWatch.stop();
        System.out.println(stopWatch.endTime);
        System.out.println(stopWatch.getElapsedTime());
    }
}

