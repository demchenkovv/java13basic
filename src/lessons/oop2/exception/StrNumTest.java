package lessons.oop2.exception;

import java.util.Scanner;

public class StrNumTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();
        scanner.nextLine(); // Предложение отбрасывает строчку с текущими входными данными, чтобы пользователь мог ввести строчку с новыми входными данными.
        String str = scanner.nextLine();
        System.out.println(num + " " + str);
    }
}
