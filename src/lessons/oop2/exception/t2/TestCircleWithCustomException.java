package lessons.oop2.exception.t2;

public class TestCircleWithCustomException {
    public static void main(String[] args) {
        // Тест исключений 1
//        try {
//            new CircleWithCustomException(5);
//            new CircleWithCustomException(-5);
//            new CircleWithCustomException(0);
//        } catch (InvalidRadiusException ex) {
//            System.out.println(ex);
//        }
//        System.out.println("Количество созданных объектов: " +
//                CircleWithCustomException.getNumberOfObjects());

        // Тест исключений 2
        /** Можно ли определить пользовательский класс исключений с помощью порождения
         * от RuntimeException? Да, но это не очень хороший способ, потому что он делает
         * пользовательское исключение непроверяемым. Лучше делать пользовательское
         * исключение проверяемым, чтобы компилятор мог принудительно перехватить эти
         * исключения в программе.
         */
        try {
            method();
            System.out.println("После вызова метода");
        } catch (RuntimeException ex) {
            System.out.println("RuntimeException в методе main()");
        } catch (Exception ex) {
            System.out.println("Exception в методе main()");
        }
    }

    static void method() throws Exception {
        try {
            CircleWithCustomException c1 = new CircleWithCustomException(1);
            c1.setRadius(-1);
            System.out.println(c1.getRadius());
        } catch (RuntimeException ex) {
            System.out.println("RuntimeException в методе method()");
        } catch (Exception ex) {
            System.out.println("Exception в методе method()");
            throw ex;
        }
    }
}