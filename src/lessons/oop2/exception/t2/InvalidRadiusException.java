package lessons.oop2.exception.t2;

public class InvalidRadiusException extends Exception {
    private double radius;

    /** Создает исключение */
    public InvalidRadiusException(double radius) {
        super("Недопустимый радиус: " + radius);
        this.radius = radius;
    }

    /** Возвращает радиус */
    public double getRadius() {
        return radius;
    }
}