package lessons.oop2.exception;

public class TestException {
    public static void main(String[] args) {
        try {
            System.out.println(sum(new int[]{1, 2, 3, 4, 5}));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("\n" + ex.getMessage());
            System.out.println("\n" + ex.toString());

            System.out.println("\nИнформация о трассировке, полученная из getStackTrace:");

            // Выводит элементы трассировки стека в массив. Каждый элемент представляет вызов метода.
            StackTraceElement[] traceElements = ex.getStackTrace();
            for (int i = 0; i < traceElements.length; i++) {
                // Для каждого элемента можно получить:
                // Метод
                System.out.print("метод " + traceElements[i].getMethodName());
                // Имя класса
                System.out.print("(" + traceElements[i].getClassName() + ":");
                // Номер строчки исключения
                System.out.println(traceElements[i].getLineNumber() + ")");
            }
        }
    }

    private static int sum(int[] list) {
        int result = 0;
        for (int i = 0; i <= list.length; i++) // ошибка
            result += list[i];
        return result;
    }
}