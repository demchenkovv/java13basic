package lessons.oop2.exception;

import java.util.function.Function;

public class Test {
    public void foo() {
        Function <String, Integer> anon = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        };
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.foo();
    }
}
