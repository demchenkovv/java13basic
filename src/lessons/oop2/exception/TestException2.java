package lessons.oop2.exception;

public class TestException2 {
    public static void main(String[] args) {
        try {
            method();
            //System.out.println(1/0);
            System.out.println("После вызова метода");
        } catch (RuntimeException ex) {
            System.out.println("RuntimeException в методе main()");
        } catch (Exception ex) {
            System.out.println("Exception в методе main()");
        } finally {
            System.out.println("Блок finally");
        }
        System.out.println("После обработки finally");
    }

    static void method() throws Exception {
        try {
            String s = "abc";
            System.out.println(s.charAt(3));
        } catch (ArithmeticException ex) {
            System.out.println("RuntimeException в методе method()");
        } catch (Exception ex) {
            System.out.println("Exception в методе method()");
            //throw ex;
        }
    }
}