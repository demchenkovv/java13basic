package lessons.oop2.exception.t1;

public class TestCircleWithException {
    public static void main(String[] args) {
        try {
            CircleWithException c1 = new CircleWithException(1);
            CircleWithException c2 = new CircleWithException(0);
            CircleWithException c3 = new CircleWithException(-5);
            CircleWithException c4 = new CircleWithException(4);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("Количество созданных объектов: " + CircleWithException.getNumberOfObjects());
    }
}