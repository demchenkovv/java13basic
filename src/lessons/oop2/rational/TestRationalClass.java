package lessons.oop2.rational;

public class TestRationalClass {
    public static void main(String[] args) {
        // Создать и инициализировать два рациональных числа r1 и r2
        Rational r1 = new Rational(-2, 6);
        Rational r2 = new Rational(2, 3);

        // Отобразить результаты
        System.out.println(r1 + " + " + r2 + " = " + r1.add(r2));
        System.out.println(r1 + " - " + r2 + " = " + r1.subtract(r2));
        System.out.println(r1 + " * " + r2 + " = " + r1.multiply(r2));
        System.out.println(r1 + " / " + r2 + " = " + r1.divide(r2));
        System.out.println(r2 + " равно " + r2.doubleValue());
        System.out.println(r1.intValue());
        System.out.println(r1.floatValue());

        // Можно создать таким образом, но сравнивать только в этом порядке,
        // т.к. у r22 типа Object нет метода compareTo(), а у r11 он есть,
        // а именно переопределен от интерфейса Comparable с типом данных <Rational>
        Rational r11 = new Rational(1, 2);
        Object r22 = new Rational(1, 3);
        System.out.println(r11.compareTo((Rational) r22));


        // Еще вариант
        Rational r8 = new Rational(1, 2);
        Rational r9 = new Rational(1, -2);
        System.out.println(r8.add(r9));

        //
        // Из теста
        System.out.println(new Rational(4,3).intValue());
        System.out.println(new Rational(4,3).doubleValue());


    }
}