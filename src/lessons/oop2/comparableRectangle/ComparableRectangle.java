package lessons.oop2.comparableRectangle;

public class ComparableRectangle extends Rectangle
        implements Comparable<ComparableRectangle> {
    /**
     * Создает ComparableRectangle с указанными свойствами
     */
    public ComparableRectangle(double width, double height) {
        super(width, height);
    }

    @Override // Реализует метод compareTo, определенный в Comparable
    public int compareTo(ComparableRectangle o) {
        if (getArea() > o.getArea())
            return 1;
        else if (getArea() < o.getArea())
            return -1;
        else
            return 0;

        //        тоже самое, только в одну строчку
        //        return getArea() > o.getArea() ? 1 : (getArea() < o.getArea() ? -1 : 0);
    }

    @Override // Реализует метод toString в GeometricObject
    public String toString() {
        return "Ширина: " + getWidth() + " Высота: " + getHeight() +
                " Площадь: " + getArea() + " Периметр: " + getPerimeter();
    }

    @Override
    public boolean equals(Object o) {
        return getArea() == ((ComparableRectangle)o).getArea();
    }

}