package lessons.zaur_tregulov.generics.wildcards;

import java.util.ArrayList;
import java.util.List;

// ? - символ Wildcard. Вместо ? может стоять и Integer, и String, и др.
// В нашем случае List<?> является супертипом любого ArrayList, например, ArrayList<String>
public class Test3 {
    public static void main(String[] args) {
        // Так нельзя, поскольку List<?> не знает какой тип мы добавляем.
        // Основная цель дженериков - Type Safe, то компилятор не разрешает
        // добавлять элементы, типы которых он не знает. В нашем случае List<?>
        // может хранить как Integer, так и String и любой другой тип
//        List<?> list = new ArrayList<String>();
//        list.add("privet");

        // bounded wildcards - значит ограничивать сверху или снизу
        List<? extends Number> list = new ArrayList<Integer>();


        // Примеры
        List<Double> list1 = new ArrayList<>();
        list1.add(3.14);
        list1.add(3.15);
        list1.add(3.16);
        showListIngo(list1);

        List<String> list2 = new ArrayList<>();
        list2.add("privet");
        list2.add("poka");
        showListIngo(list2);

        ArrayList<Double> arrayList1 = new ArrayList<>();
        arrayList1.add(3.14);
        arrayList1.add(3.15);
        System.out.println(sum(arrayList1));

        ArrayList<Integer> arrayList2 = new ArrayList<>();
        arrayList2.add(14);
        arrayList2.add(15);
        System.out.println(sum(arrayList2));
    }

    static void showListIngo(List<?> list) {
        System.out.println("Мой лист содержит следующие элементы: " + list);
    }

    public static double sum(ArrayList<? extends Number> arrayList) {
        double sum = 0;
        for (Number n : arrayList) {
            sum += n.doubleValue();
        }
        return sum;
    }
}
