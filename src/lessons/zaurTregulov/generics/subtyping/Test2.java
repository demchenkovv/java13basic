package lessons.zaur_tregulov.generics.subtyping;

import java.util.ArrayList;

public class Test2
        // Теперь T может быть только наследником от AllImplClass
        // и имплементить интерфейс 1 и интерфейс 2
        <T extends
                AllImplClass
                & Interface1
                & Interface2> {
    public static void main(String[] args) {
        X x = new Y(); // X - родитель, Y - наследник

        Test2<AllImplClass> test2 = new Test2<>();


    }
}

class AllImplClass
        extends X
        implements Interface1, Interface2 {
}

class X<T extends Number> {
    T value;
}

class Y extends X {
}

class GenMethod {
    // <T extends Number> - T может быть только наследником Number (Integer, ...)
    public static <T extends Number> T getSecondElement(ArrayList<T> arrayList) {
        return arrayList.get(1);
    }
}

interface Interface1 {
}

interface Interface2 {
}
