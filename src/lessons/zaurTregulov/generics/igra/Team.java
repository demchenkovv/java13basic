package lessons.zaur_tregulov.generics.igra;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// <T extends Participant> - этим мы показываем, что дженерик T может быть
// только наследником (подклассом) класса Participant
public class Team <T extends Participant> {
    private String name;
    private List<T> participants = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public void addNewParticipant(T participant) {
        participants.add(participant);
        System.out.println("В команду " + name + " был добавлен" +
                " новый участник по имени " + ((Participant)participant).getName());
    }

    public void playWith(Team<T> team) {
        String winnerName;
        Random random = new Random();
        int i = random.nextInt(2);
        if (i == 0) {
            winnerName = this.name;
        } else {
            winnerName = team.name;

        }
        System.out.println("Выиграла команда " + winnerName);
    }
}
