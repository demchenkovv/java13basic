package lessons.zaur_tregulov.generics.typeErasure;

// Type Erasure - стирание типов для совместимости
public class erasureClass {

//    // Мы не можем создать такие перегруженные методы,
//    // поскольку для JVM они выглядят одинаково abc(Info info)
//    public void abc(Info<String> info) {
//        String s = info.getValue();
//    }
//
//    public void abc(Info<Integer> info) {
//        Integer i = info.getValue();
//    }
}

class Info<T> {
    private T value;

    public Info(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}

// Так с двумя классами сделать тоже нельзя, поскольку информацию о дженериках
// JVM не видит. Потому что компилятор понимает, что при кастинге в сабклассе будут
// возникать проблемы, поэтому не позволяет так делать
//class Parent {
//    public void abc(Info<String> info) {
//        String s = info.getValue();
//    }
//}
//
//class Child extends Parent {
//    public void abc(Info<Integer> info) {
//        Integer i = info.getValue();
//    }
//}