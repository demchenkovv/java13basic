package lessons.zaur_tregulov.nested_classes.static_nested_class;

// Нам нужны статические классы тогда, когда мы один класс хотим привязать к другому.
// static класс можно использовать как обычный внешний класс. Т.е. мы в другом классе Test
// смогли создать объект класса Engine без создания outer класса Car.
// Важно указать, где находится вложенный класс (Car.)
// Важно! Создание объекта вложенного статического класса возможно так:
// Car.Engine engine = new Car.Engine(256);

// Если вложенный класс сделать private, то мы не сможем в стороннем классе
// создать объект внутреннего класса.

// Если добавить final, то мы не сможем иметь наследников от этого класса
// Если добавить abstract, то мы не сможем создать объект этого класса, т.е. только extends (наследоваться).
// Если решим наследоваться от вложенного класса, то выглядеть это будет примерно так:
// class Oil extends Car.Engine {}
//
// Вложенный класс может содержать статические поля, например, количество созданных моторов static int.
// Если вложенного класса мы можем обращаться к внешним static полям внешнего (outer) класса, но не к другим.
public class Car {
    private String color;
    int doorCount;
    Engine engine;
    static int a;

    public Car(String color, int doorCount, Engine engine) {
        this.color = color;
        this.doorCount = doorCount;
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "My car: {" +
                "color='" + color + '\'' +
                ", doorCount=" + doorCount +
                ", engine=" + engine +
                '}';
    }

    public static class Engine {
        int horsePower;

        public Engine(int horsePower) {
            this.horsePower = horsePower;
        }

        @Override
        public String toString() {
            return "My Engine: {" +
                    "horsePower=" + horsePower +
                    '}';
        }
    }
}

class Test {
    public static void main(String[] args) {
        Car.Engine engine = new Car.Engine(256);
        System.out.println(engine);

        Car car = new Car("red", 2, engine);
        System.out.println(car);
    }
}

class Oil extends Car.Engine {
    Oil() {
        super(200);
    }
}
