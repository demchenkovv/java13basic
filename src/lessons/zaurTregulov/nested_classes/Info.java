package lessons.zaur_tregulov.nested_classes;

// Класс Info является внешним, так же его называют top-lvl.
// Класс Info является аутер классом (outer class).
// Вложенные (Nested) классы имеют доступ к переменным outer class.
public class Info {
    static class A {} // первый тип - статичный класс
    class B{} // второй тип - вложенный (inner) class
    void method() {
        class C{} // третий тип - локальный класс
    }
}

// четвертый тип - анонимный класс, его рассмотрение будет в следующих уроках