package lessons.zaur_tregulov.collection.queue_interface;

import java.util.PriorityQueue;

// PriorityQueue – это специальный вид очереди, в котором используется натуральная сортировка ИЛИ та,
// которую мы описываем с помощью Comparable или Comparator.
// Порядок ИСПОЛЬЗОВАНИЯ элементов в коллекции согласно сортировке, однако если вывести
// коллекцию сразу после добавления элементов, то мы увидим, что элементы расположены в порядке
// их добавления в коллекцию. Иметь это ввиду.
public class PriorityQueueEx1 {
    public static void main(String[] args) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(4);
        priorityQueue.add(1);
        priorityQueue.add(7);
        priorityQueue.add(10);
        priorityQueue.add(8);
        // 1 4 7 10 8 - порядок добавления, 1 4 7 8 10 - порядок использования
        // наглядно это можно увидеть, если использовать удаление элементов
        System.out.println(priorityQueue);
        System.out.println("peek(): " + priorityQueue.peek()); // первый в очереди
        priorityQueue.remove();
        System.out.println("peek(): " + priorityQueue.peek());
        priorityQueue.remove();
        System.out.println("peek(): " + priorityQueue.peek());
        priorityQueue.remove();
        System.out.println("peek(): " + priorityQueue.peek());
        priorityQueue.remove();
        System.out.println("peek(): " + priorityQueue.peek());

    }
}
