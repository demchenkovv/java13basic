package lessons.zaur_tregulov.collection.queue_interface;

import java.util.LinkedList;
import java.util.Queue;

public class LinkedListEx {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.add("Zaur");
        queue.add("Oleg");
        queue.offer("Ivan");
        queue.offer("Mariya");
        queue.offer("Aleksandr");
        System.out.println(queue);

        System.out.println("queue.remove(): " + queue.remove()); // удаление (возвращает удаляемый элемент), с exception
        System.out.println("queue.element(): " + queue.element()); // показывает первый элемент в очереди (head), с exception
        System.out.println("queue.poll(): " + queue.poll()); // удаление (возвращает удаляемый элемент, без exception)
        System.out.println("queue.peek(): " + queue.peek()); // // показывает первый элемент в очереди (head), без exception
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue);

    }
}
