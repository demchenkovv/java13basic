package lessons.zaur_tregulov.collection.queue_interface;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeEx {
    public static void main(String[] args) {
        Deque<Integer> deque = new ArrayDeque<>();
        // Методы добавления элементов, из которых offer не выбрасывают exception
        deque.addFirst(3); // 3
        deque.addFirst(5); // 5 3
        deque.addLast(7); // 5 3 7
        deque.offerFirst(1); // 1 5 3 7
        deque.offerLast(8); // 1 5 3 7 8
        System.out.println(deque);

        // Методы получения элементов, из которых peek не выбрасывают exception
        System.out.println("Методы получения элементов: ");
        System.out.println(deque.getFirst());
        System.out.println(deque.getLast());
        System.out.println(deque.peekFirst());
        System.out.println(deque.peekLast());

        // Методы удаления элементов, из которых poll не выбрасывают exception
        System.out.println("Удаление элементов из очереди: ");
        System.out.println(deque);
        System.out.println(deque.removeFirst());
        System.out.println(deque.removeLast());
        System.out.println(deque.pollFirst());
        System.out.println(deque.pollLast());
        System.out.println("Очередь после удаления" + deque);



    }
}
