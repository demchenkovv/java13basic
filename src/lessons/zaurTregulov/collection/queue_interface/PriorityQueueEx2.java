package lessons.zaur_tregulov.collection.queue_interface;

import java.util.Objects;
import java.util.PriorityQueue;

public class PriorityQueueEx2 {
    public static void main(String[] args) {
        Student st1 = new Student("Zaur", 5);
        Student st2 = new Student("Misha", 1);
        Student st3 = new Student("Igor", 2);
        Student st4 = new Student("Marina", 3);
        Student st5 = new Student("Olya", 4);

        PriorityQueue<Student> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(st1);
        priorityQueue.add(st2);
        priorityQueue.add(st3);
        priorityQueue.add(st4);
        priorityQueue.add(st5);
        System.out.println(priorityQueue);

        System.out.println("Порядок удаления студентов: ");
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
    }
}

class Student implements Comparable<Student> {
    String name;
    int course;

    public Student(String name, int course) {
        this.name = name;
        this.course = course;
    }

    @Override
    public String toString() {
        return name + "_" + course;
    }

    @Override
    public int compareTo(Student o) {
        return this.course - o.course;
    }

    // Если переопределяем equals, то hashCode желательно тоже переопределить
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        // Чтобы соблюсти правило "ЕСЛИ a.equals(b) == true, ТО a.compareTo(b) == 0"
        // необходимо убрать часть из return, а именно "&& Objects.equals(name, student.name)"
//        return course == student.course && Objects.equals(name, student.name);
        return course == student.course;
    }

    // Соответственно в hashCode мы тоже должны убрать name, поскольку сравнение
    // идет только по полю course, соответственно и hashCode должен использоваться только course
    @Override
    public int hashCode() {
        return Objects.hash(course);
    }
}
