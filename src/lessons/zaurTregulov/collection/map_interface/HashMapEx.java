package lessons.zaur_tregulov.collection.map_interface;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Элементами HashMap являются пары ключ/значение.
 * HashMap НЕ запоминает порядок добавления элементов.
 * Его методы работают очень быстро.
 * <p>
 * Ключи элементов должны быть уникальными. Ключ может быть null.
 * При попытке добавить элемент с ключом, который уже есть в словаре (Map),
 * то значение с таким ключом будет перезаписано на новый.
 * Значения элементов могут повторяться. Значения могут быть null.
 * <p>
 * Методы:
 * put(K,V) - добавляет элемент;
 * putIfAbsent(K,V) - добавляет элемент, если элемента с таким ключ нет;
 * get(K) - возвращает элемент с заданным ключом, если такого ключа нет, то null;
 * remove(K) - удаляет элемент с заданным ключом,
 * containsValue(V) - возвращает boolean, есть ли в коллекции элемент со значением V;
 * containsKey(K) - возвращает boolean, есть ли в коллекции элемент с ключом K;
 * keySet() - возвращает множество всех ключей K (Set<K>), которые есть в Map;
 * values() - возвращает коллекцию всех значений V (Collection<V>), которые есть в Map;
 * entrySet - используется для создания набора из тех же элементов, которые содержатся в карте.
 */
public class HashMapEx {
    public static void main(String[] args) {
        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1000, "Заур Трегулов");
        map1.put(3568, "Иван Иванов");
        map1.put(6578, "Мария Сидорова");
        map1.put(15879, "Николай Петров");
        map1.putIfAbsent(1000, "Олег Иванов");
        map1.put(null, "Сергей Петров");
        map1.put(10100, null);
        System.out.println(map1);

        System.out.println("get(1000): " + map1.get(1000));

        System.out.println("containsValue(\"Иван Иванов\"): " + map1.containsValue("Иван Иванов"));
        System.out.println("containsKey(1200): " + map1.containsKey(1200));

        Set<Integer> integerSet = map1.keySet();
        System.out.println("keySet(): " + integerSet);

        Collection<String> stringSet = map1.values();
        System.out.println("values(): " + stringSet);

        Set<Map.Entry<Integer,String>> entrySet = map1.entrySet();
        System.out.println("entrySet(): " + entrySet);

//        map1.remove(15879);
//        System.out.println("after remove(15879): " + map1);
    }
}
