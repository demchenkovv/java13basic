package lessons.zaur_tregulov.collection.map_interface;

/** Порядок добавления сохраняется (сортировки по умолчанию нет). */
import java.util.LinkedHashMap;

public class LinkedHashMapEx {
    public static void main(String[] args) {


        // При accessOrder == true - список будет сортироваться по времени использования
        // элементов в коллекции. Т.о. элемент, к которому мы обращались крайний раз, станет
        // последним в коллекции (уйдет в tail).
        // Т.о. в коллекции будут двигаться те элементы, к которым мы обращались.

        LinkedHashMap<Double, Student> lhm = new LinkedHashMap<>(16, 0.75F, true);

        Student st1 = new Student("Zaur", "Tregulov", 3);
        Student st2 = new Student("Mariya", "Ivanova", 1);
        Student st3 = new Student("Sergey", "Petrov", 4);
        Student st4 = new Student("Igor", "Sidorov", 2);

        lhm.put(7.2, st3);
        lhm.put(7.5, st4);
        lhm.put(5.8, st1);
        lhm.put(6.4, st2);

        System.out.println(lhm);

        System.out.println(lhm.get(6.4));
        System.out.println(lhm.get(7.5));
        System.out.println(lhm.get(7.2));
        System.out.println(lhm.get(5.8));


        System.out.println(lhm);
    }
}
