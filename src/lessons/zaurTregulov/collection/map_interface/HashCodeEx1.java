package lessons.zaur_tregulov.collection.map_interface;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Если Вы переопределили equals, то переопределите и hashcode.
 * Ситуация, когда результат метода hashcode для разных объектов одинаков, называется коллизией. Чем её меньше, тем лучше.
 * Результат нескольких выполнений метода hashcode для одного и того же объекта должен быть одинаковым.
 * Если, согласно методу equals, два объекта равны, то и hashcode данных объектов обязательно должен быть одинаковым.
 * Если, согласно методу equals, два объекта НЕ равны, то hashcode данных объектов НЕ обязательно должен быть разным.
 */
public class HashCodeEx1 {
    public static void main(String[] args) {
        Student st1 = new Student("Zaur", "Tregulov", 3);
        Student st2 = new Student("Mariya", "Ivanova", 1);
        Student st3 = new Student("Sergey", "Petrov", 4);

        Map<Student, Double> map = new HashMap<>();
        map.put(st1, 7.5);
        map.put(st2, 8.7);
        map.put(st3, 9.2);
        System.out.println(map);

        Student st4 = new Student("Zaur", "Tregulov", 3);
        Student st5 = new Student("Igor", "Sidorov", 4);

        // При сравнении объектов HashMap и HashSet ОБЯЗАТЕЛЬНО необходимо
        // переопределять методы equals() и hashCode()
        // Однако у Integer и String методы уже переопределены, поэтому
        // переопределять необходимо в первую очередь у классов, отличных от
        // Integer и String

//        boolean result = map.containsKey(st4);
//        System.out.println("result: " + result);

        System.out.println(st1.hashCode());
        System.out.println(st5.hashCode());

        for (Map.Entry<Student, Double> entry : map.entrySet()) {
            // entrySet() возвращает множество Entry<K, V>. Entry - это внутренний интерфейс
            // для Map, который имплементируется нашим классом внутренним для Map -
            // class Node<K,V> implements Map.Entry<K,V>.
            // Класс Node содержит ключ, значение, хэш и ссылку
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

        /*
        Initial capacity – начальный размер массива;
        Load factor – коэффициент того, насколько массив должен быть заполнен,
        после чего его размер будет увеличен вдвое.
         */
        Map<Integer, String> map2 = new HashMap<>(16, 0.75F);

    }
}

final class Student implements Comparable<Student> {
    final String name;
    final String surname;
    int course;

    public Student(String name, String surname, int course) {
        this.name = name;
        this.surname = surname;
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", course=" + course +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return course == student.course && Objects.equals(name, student.name) && Objects.equals(surname, student.surname);
    }

    @Override
    public int hashCode() {
        // то что предлагает IDEA
        return Objects.hash(name, surname, course);

        // Одна из своих реализаций метода, однако его логика некорректна,
        // поскольку при разных значениях длины могут быть одинаковыми.
//        return name.length() + surname.length() + course;

        // Когда у разных объектов с разными значениями возвращается
        // одинаковый hashCode это называется КОЛЛИЗИЕЙ. И чем меньше
        // коллизий в нашем коде, тем лучше.
        // Для того, что бы коллизий было меньше можно использовать умножение
        // значений на какие-то простые числа (число, которое делится только
        // на само себя и на единицу). Но такая реализация также имеет свои минусы.
        // Поэтому в нашем случае лучше использовать метод, который предлагает IDEA.
//        return name.length() * 7 + surname.length() * 11 + course * 53;
    }

    @Override
    public int compareTo(Student otherStudent) {
        return this.name.compareTo(otherStudent.name); // сравнение по имени в лексикографическом порядке
//        return this.course - otherStudent.course; // сравнение по номеру курса в порядке возрастания
    }
}
