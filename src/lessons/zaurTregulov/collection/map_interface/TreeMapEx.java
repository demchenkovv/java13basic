package lessons.zaur_tregulov.collection.map_interface;

import java.util.Comparator;
import java.util.TreeMap;

/**
 * Элементами TreeMap являются пары ключ/значение.
 * В TreeMap элементы хранятся в отсортированном по возрастанию порядке (сортировка по ключу).
 * В основе TreeMap лежит красно-чёрное дерево.
 * Это позволяет методам работать быстро, но не быстрее, чем методы HashMap.
 * Уникальность ключей должна поддерживаться, в противном случае значение будет перезаписано.
 * Основная цель использования TreeMap - это, как правило, нахождение каких-то отрезков (ренджей).
 */
public class TreeMapEx {
    public static void main(String[] args) {
        // Пример 1
        // Если key - double, a value - Student
        TreeMap<Double, Student> treeMap = new TreeMap<>();
        Student st1 = new Student("Zaur", "Tregulov", 3);
        Student st2 = new Student("Mariya", "Ivanova", 1);
        Student st3 = new Student("Sergey", "Petrov", 4);
        Student st4 = new Student("Igor", "Sidorov", 2);
        Student st5 = new Student("Vasiliy", "Smirnov", 1);
        Student st6 = new Student("Sasha", "Kapustin", 3);
        Student st7 = new Student("Elena", "Sidorova", 4);

        treeMap.put(5.8, st1);
        treeMap.put(9.1, st7);
        treeMap.put(6.4, st2);
        treeMap.put(7.5, st4);
        treeMap.put(7.2, st3);
        treeMap.put(8.2, st6);
        treeMap.put(7.9, st5);

        System.out.println("treeMap sort by default: \n" + treeMap);

        // get(K) - поиск по ключу
//        System.out.println(treeMap.get(6.4));

        // remove(K) - удаление по ключу
//        treeMap.remove(5.8);

        // descendingMap() - возвращает коллекцию в обратном лексикографическом порядке
//        System.out.println(treeMap.descendingMap());

        // tailMap(K) - поиск value (значений), где ключ больше, чем K:
//        System.out.println(treeMap.tailMap(7.3));

        // headMap(K) - поиск value (значений), где ключ меньше, чем K:
//        System.out.println(treeMap.headMap(7.3));

        // lastEntry() - возвращает последний элемент коллекции (tail)
//        System.out.println(treeMap.lastEntry());
//
//        // firstEntry() - возвращает первый элемент коллекции (head)
//        System.out.println(treeMap.firstEntry());


        // Пример 2
        // Если key - Student, a value - double
        // Поскольку TreeMap отсортированная по умолчанию коллекция, за основу беря ключи.
        // У нас ключ Student, поэтому JVM не может понять какой из этих студентов больше,
        // а какой меньше. Классу Student необходимо имплементировать интерфейс Comparable,
        // и переопределить метод compareTo().

        TreeMap<Student, Double> treeMap2 = new TreeMap<>();
        treeMap2.put(st1, 5.8);
        treeMap2.put(st7, 9.1);
        treeMap2.put(st2, 6.4);
        treeMap2.put(st4, 7.5);
        treeMap2.put(st3, 7.2);
        treeMap2.put(st6, 8.2);
        treeMap2.put(st5, 7.9);
        System.out.println("treeMap2 sort by name: \n" + treeMap2);


        // Пример 3
        // Реализация Comparator(T o1, T o2) в параметре при создании TreeMap()
        TreeMap<Student, Double> treeMap3 = new TreeMap<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.surname.compareTo(o2.surname);
            }
        });
        treeMap3.put(st1, 5.8);
        treeMap3.put(st7, 9.1);
        treeMap3.put(st2, 6.4);
        treeMap3.put(st4, 7.5);
        treeMap3.put(st3, 7.2);
        treeMap3.put(st6, 8.2);
        treeMap3.put(st5, 7.9);
        System.out.println("treeMap3 sort by surname: \n" + treeMap3);

    }
}
