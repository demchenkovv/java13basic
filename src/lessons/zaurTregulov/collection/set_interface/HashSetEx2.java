package lessons.zaur_tregulov.collection.set_interface;

import java.util.HashSet;

public class HashSetEx2 {
    public static void main(String[] args) {
        HashSet<Integer> hashSet1 = new HashSet<>();
        hashSet1.add(5);
        hashSet1.add(2);
        hashSet1.add(3);
        hashSet1.add(1);
        hashSet1.add(8);
        System.out.println("hashSet1: " + hashSet1);

        HashSet<Integer> hashSet2 = new HashSet<>();
        hashSet2.add(7);
        hashSet2.add(4);
        hashSet2.add(3);
        hashSet2.add(5);
        hashSet2.add(8);
        System.out.println("hashSet2: " + hashSet2);

        // Операция Union - объединение
        HashSet<Integer> union = new HashSet<>(hashSet1);
        union.addAll(hashSet2);
        System.out.println("union (объединение): " + union); // [1, 2, 3, 4, 5, 7, 8]

        // Операция Intersect - создание множества с общими пересекающимися элементами из двух множеств
        HashSet<Integer> intersect = new HashSet<>(hashSet1);
        intersect.retainAll(hashSet2);
        System.out.println("intersect (пересекающиеся): " + intersect); // [3, 5, 8]

        // Операция Subtract - обратная intersect, т.е. создание множества с различными (непересекающимися)
        // элементами из двух множеств
        HashSet<Integer> subtract = new HashSet<>(hashSet1);
        subtract.removeAll(hashSet2);
        System.out.println("subtract (непересекающиеся): " + subtract); // [3, 5, 8]

    }
}
