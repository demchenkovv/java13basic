package lessons.zaur_tregulov.collection.set_interface;

import java.util.HashSet;
import java.util.Set;

    // В HashSet отсутствует метод get(), поскольку в нем нет смысла.
    // В Map мы получали значения по ключу, а здесь содержатся только значения.
    // Смысл писать "Заур", что бы найти Заур? Нет никакого смысла.
    // Мнение со SOF: Потому что в наборах нет упорядочивания. Некоторые реализации (особенно те,
    // которые реализуют java.util.SortedSet интерфейс), но это не общее свойство множеств.
    // Если вы пытаетесь использовать наборы таким образом, вам следует подумать об
    // использовании вместо этого списка.

public class HashSetEx1 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Zaur");
        set.add("Oleg");
        set.add("Marina");
        set.add("Igor");
        set.add("Igor"); // дубликаты не добавляются в множество
        set.add(null); // тоже может содержать, поскольку корни идут от HashMap
        set.remove("Zaur");

        for (String s : set) {
            System.out.println(s);
        }

        System.out.println("size: " + set.size());
        System.out.println("isEmpty: " + set.isEmpty());
    }
}
