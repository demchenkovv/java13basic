package lessons.zaur_tregulov.collection.set_interface;

import java.util.Objects;
import java.util.TreeSet;

public class TreeSetEx2 {
    public static void main(String[] args) {

        TreeSet<Student> treeSet = new TreeSet<>();
        Student st1 = new Student("Zaur", 5);
        Student st2 = new Student("Misha", 1);
        Student st3 = new Student("Igor", 2);
        Student st4 = new Student("Marina", 3);
        Student st5 = new Student("Olya", 4);
        // ! Если добавлять значения в "дерево" TreeSet (как и в TreeMap),
        // то выбросит исключение ClassCastException, т.к. JVM не понимает
        // по какому критерию сортировать множество.
        // Необходимо имплементировать Comparable и переопределить метод compareTo
        // или при создании TreeSet<>(в конструкторе) указать Comparator и его реализацию.
        // Переопределять методы hashCode и equals необходимости нет, т.к. это Tree. Так же
        // мы поступили и в TreeMap.
        treeSet.add(st1);
        treeSet.add(st2);
        treeSet.add(st3);
        treeSet.add(st4);
        treeSet.add(st5);

        for (Student o : treeSet) {
            System.out.println(o);
        }

        System.out.println("first: " + treeSet.first());
        System.out.println("last: " + treeSet.last());

        Student st6 = new Student("Oleg", 2);

        // Возвращаются значение исходя из номера курса, поскольку в переопределенном
        // методе compareTo() задана логика сравнения по номеру курса
        System.out.println("headSet: " + treeSet.headSet(st6));
        System.out.println("tailSet: " + treeSet.tailSet(st6));

        Student st7 = new Student("Katya", 2);
        Student st8 = new Student("Ivan", 4);

        // Возвращает множество элементы которого находятся между двумя показателями
        // в нашем случае между курсами 2 и 4 (иначе course >= 2 && course < 4)
        System.out.println("subSet(st7, st8): " + treeSet.subSet(st7, st8));

        // Проверяем сравнение студентов по курсу с учетом
        // переопределенных методов equals, hashCode и compareTo (interface Comparable)
        System.out.println("st3.equals(st6): " + st3.equals(st6));
        System.out.println("st3.hashCode(): " + st3.hashCode());
        System.out.println("st6.hashCode(): " + st6.hashCode());
        System.out.println("hashCode() st3 == st6: " + (st6.hashCode() == st3.hashCode()));
    }
}

class Student implements Comparable<Student> {
    String name;
    int course;

    public Student(String name, int course) {
        this.name = name;
        this.course = course;
    }

    @Override
    public String toString() {
        return name + "_" + course;
    }

    @Override
    public int compareTo(Student o) {
        return this.course - o.course;
    }

    // Если переопределяем equals, то hashCode желательно тоже переопределить
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        // Чтобы соблюсти правило "ЕСЛИ a.equals(b) == true, ТО a.compareTo(b) == 0"
        // необходимо убрать часть из return, а именно "&& Objects.equals(name, student.name)"
//        return course == student.course && Objects.equals(name, student.name);
        return course == student.course;
    }

    // Соответственно в hashCode мы тоже должны убрать name, поскольку сравнение
    // идет только по полю course, соответственно и hashCode должен использоваться только course
    @Override
    public int hashCode() {
        return Objects.hash(course);
    }
}
