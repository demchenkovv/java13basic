package lessons.zaur_tregulov.collection.set_interface;

import java.util.LinkedHashSet;
/** Использовать LinkedHashSet необходимо тогда, когда
 * важна последовательность добавления элементов в коллекцию */
public class LinkedHashSetEx {
    public static void main(String[] args) {
        LinkedHashSet<Integer> lhs = new LinkedHashSet<>();
        // Последовательность в LinkedHashSet сохраняется
        lhs.add(5);
        lhs.add(3);
        lhs.add(1);
        lhs.add(8);
        lhs.add(10);
        System.out.println(lhs);
        lhs.remove(8);
        System.out.println(lhs);
        System.out.println("lhs.contains(8): " + lhs.contains(8));
        System.out.println("lhs.contains(10): " + lhs.contains(10));
    }
}
