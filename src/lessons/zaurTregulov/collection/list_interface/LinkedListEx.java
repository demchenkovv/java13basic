package lessons.zaur_tregulov.collection.list_interface;

import java.util.LinkedList;

public class LinkedListEx {
    public static void main(String[] args) {
        Student2 st1 = new Student2("Иван", 3);
        Student2 st2 = new Student2("Николай", 2);
        Student2 st3 = new Student2("Елена", 1);
        Student2 st4 = new Student2("Пётр", 4);
        Student2 st5 = new Student2("Мария", 3);

        // LinkedList - это звенья одной цепочки. Элементы хранят определенные
        // данные, а так же ссылки на предыдущий (lhs) и следующий (rhs) элементы.
        // Т.е. текущий элемент имеет ссылку на следующий и предыдущий элементы.
        // В нашем случае "Мария" - это хвост(tail), "Иван" - это голова (head).
        // LinkedList имеет голову и хвост. Метод get() перебирает все элементы,
        // пока не найдет заданный (что является доп. нагрузкой на память), т.о. работает связанный список.
        // Порядок добавления элементов сохраняется.

        // Если мы планируем работать с методом get(), то
        // лучше использовать ArrayList, т.к. поиск элемента происходит моментально за счет того, что
        // в основе (своей базы) используется массив.
        // LinkedList - поиск работает медленно, т.к. мы проходимся по всем элементам, пока не доходим до нужного.
        // НО! Если поиск элементов не частый, а в основном используется добавление/удаление элементов, то
        // лучше использовать LinkedList, так как при этих операциях меняется только ссылка, когда как в
        // ArrayList происходит копирование и сдвиг элементов. А когда таких элементов много, то это значительно
        // увеличивает время операции.

        // Как правило, LinkedList следует использовать когда:
        // 1) Невелико количество операций получения элементов;
        // 2) Велико количество операций добавления и удаления элементов. Особенно, если речь
        // идет об элементах в начале коллекции.

        LinkedList<Student2> student2LinkedList = new LinkedList<>();
        student2LinkedList.add(st1);
        student2LinkedList.add(st2);
        student2LinkedList.add(st3);
        student2LinkedList.add(st4);
        student2LinkedList.add(st5);

        System.out.println("LinkedList: ");

        System.out.println(student2LinkedList);
//        System.out.println(student2LinkedList.get(2));

        Student2 st6 = new Student2("Заур", 3);
        Student2 st7 = new Student2("Игорь", 4);

        student2LinkedList.add(st6);
        System.out.println(student2LinkedList);

        student2LinkedList.add(1, st7);
        System.out.println(student2LinkedList);

        student2LinkedList.remove(3);
        System.out.println(student2LinkedList);
    }
}

class Student2 {
    private String name;
    private int course;

    public Student2(String name, int course) {
        this.name = name;
        this.course = course;
    }

    @Override
    public String toString() {
        return "name = '" + name + '\'' +
                ", course = " + course;
    }
}
