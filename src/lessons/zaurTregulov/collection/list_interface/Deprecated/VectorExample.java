package lessons.zaur_tregulov.collection.list_interface.Deprecated;

import java.util.Vector;

/**
 * !!! Не рекомендован для использования.
 * Vector – устаревший synchronized класс.
 * В своей основе содержит массив элементов Object.
 * Более ресурсоемкий.
 *
 * Вместо данного класса используется ArrayList
 */
public class VectorExample {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>();
        vector.add("Zaur");
        vector.add("Misha");
        vector.add("Oleg");
        vector.add("Katya");
        System.out.println(vector);
        System.out.println(vector.firstElement());
        System.out.println(vector.lastElement());

        vector.remove(2);
        System.out.println(vector);

        System.out.println(vector.get(1));

    }
}
