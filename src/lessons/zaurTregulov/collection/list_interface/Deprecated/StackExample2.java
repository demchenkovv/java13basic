package lessons.zaur_tregulov.collection.list_interface.Deprecated;

import java.util.Stack;

/**
 * !!! Не рекомендован для использования.
 * Stack – устаревший synchronized класс.
 * Использует принцип LIFO (последний пришел - первый ушел, как "стопка тарелок").
 */
public class StackExample2 {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("Заур");
        stack.push("Миша");
        stack.push("Олег");
        stack.push("Катя");
        System.out.println(stack);

        // Метод pop() возвращает последний (верхний, хвостовой) элемент и удаляет его
//        while (!stack.isEmpty()) {
//            System.out.println(stack.pop());
//            System.out.println("after pop: " + stack);
//        }

        // Метод pick() возвращает последний (верхний, хвостовой) элемент, НО не удаляет его
        System.out.println(stack.peek());
        System.out.println(stack);
    }
}
