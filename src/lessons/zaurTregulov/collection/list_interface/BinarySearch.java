package lessons.zaur_tregulov.collection.list_interface;

import java.util.*;

public class BinarySearch {
    public static void main(String[] args) {
        // 1
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(-3);
        arrayList.add(8);
        arrayList.add(12);
        arrayList.add(-8);
        arrayList.add(0);
        arrayList.add(5);
        arrayList.add(10);
        arrayList.add(1);
        arrayList.add(150);
        arrayList.add(-30);
        arrayList.add(19);

        // Прежде, чем использовать бинарный поиск, список необходимо отсортировать
        Collections.sort(arrayList);
        int index1 = Collections.binarySearch(arrayList, 99);
        System.out.println("index1: " + index1);

        // 2
        Employee emp1 = new Employee(100, "Заур", 12345);
        Employee emp2 = new Employee(15, "Иван", 6542);
        Employee emp3 = new Employee(123, "Петр", 8542);
        Employee emp4 = new Employee(15, "Мария", 5678);
        Employee emp5 = new Employee(182, "Коля", 125);
        Employee emp6 = new Employee(15, "Саша", 9874);
        Employee emp7 = new Employee(250, "Елена", 1579);
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(emp1);
        employeeList.add(emp2);
        employeeList.add(emp3);
        employeeList.add(emp4);
        employeeList.add(emp5);
        employeeList.add(emp6);
        employeeList.add(emp7);
        System.out.println("employeeList: " + employeeList);

        // Чтобы использовать сортировку sort() для объектов классов,
        // необходимо имплементировать интерфейс Comparable<T>
        // и переопределить метод compareTo(T o)
        Collections.sort(employeeList);
        System.out.println("employeeList: " + employeeList);

        int index2 = Collections.binarySearch(
                employeeList,
                new Employee(182, "Коля", 125));
        System.out.println("binarySearch \"Коля\" index2: " + index2);

        // 3
        // Использование binarySearch() в массивах
        int[] array = {-3, 8, 12, -8, 0, 5, 10, 1, 150, -30, 19};
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        int index3 = Arrays.binarySearch(array, 150);
        System.out.println("binarySearch 150 index3: " + index3);

        // 4
        // Отзеркалить список можно с помощью
        // метода reverse() класса Collections
        Collections.reverse(arrayList);
        System.out.println("reverse: " + arrayList);

        // 5
        // Перемешать список можно с помощью
        // метода shuffle() класса Collections
        Collections.shuffle(arrayList);
        System.out.println("shuffle: " + arrayList);
    }
}

class Employee
        implements Comparable<Employee> {
    int id;
    String name;
    int salary;

    public Employee(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }

    @Override
    public int compareTo(Employee anotherEmp) {
        int res = this.id - anotherEmp.id;
        if (res == 0) {
            res = this.name.compareTo(anotherEmp.name);
        }
        return res;
    }
}
