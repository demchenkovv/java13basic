package lessons.zaur_tregulov.collection.list_interface;

import java.util.ArrayList;

public class ArrayListMethods3 {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Заур");
        arrayList1.add("Иван");
        arrayList1.add("Мария");
        System.out.println(arrayList1);

        ArrayList<String> arrayList2 = new ArrayList<>();
        arrayList2.add("!!!");
        arrayList2.add("???");

        arrayList1.addAll(1, arrayList2);
        System.out.println(arrayList1);

//        arrayList1.clear();
//        System.out.println(arrayList1);

        // Возвращает номер индекса объекта
        // Если их два одинаковых, то возвращает номер первого вхождения
        int firstIndex = arrayList1.indexOf("Мария");
        System.out.println("firstIndex: " + firstIndex);

        int lastIndex = arrayList1.lastIndexOf("Иван");
        System.out.println("lastIndex: " + lastIndex);

        System.out.println("size: " + arrayList1.size());

        System.out.println("isEmpty: " + arrayList1.isEmpty());

        System.out.println("contains Петя: " + arrayList1.contains("Петя"));
        System.out.println("contains Мария: " + arrayList1.contains("Мария"));
    }
}
