package lessons.zaur_tregulov.collection.list_interface;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorExample {
    public static void main(String[] args) {
        // Iterator - это повторитель. С помощью повторителя можно пробежаться по списку.
        // Одно из отличий Iterator от forEach, что с помощью итератора
        // можно удалить значение из коллекции
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Заур");
        arrayList1.add("Иван");
        arrayList1.add("Мария");
        arrayList1.add("Николай");
        arrayList1.add("Елена");

        Iterator<String> iterator = arrayList1.iterator();
        // iterator.hasNext() - проверяет наличие следующего объекта
        // iterator.next() - берет этот объект для последующих операций
        while (iterator.hasNext()){
            iterator.next();
            iterator.remove();
        }
        System.out.println(arrayList1);
    }
}
