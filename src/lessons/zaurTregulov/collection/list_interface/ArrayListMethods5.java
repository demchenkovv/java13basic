package lessons.zaur_tregulov.collection.list_interface;

import java.util.ArrayList;
import java.util.List;

public class ArrayListMethods5 {
    public static void main(String[] args) {

        // 1
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Заур");
        arrayList1.add("Иван");
        arrayList1.add("Мария");
        arrayList1.add("Николай");
        arrayList1.add("Елена");
        System.out.println("arrayList1: " + arrayList1);

//        ArrayList<String> arrayList2 = new ArrayList<>();
//        arrayList2.add("Заур");
//        arrayList2.add("Иван");
//        arrayList2.add("Игорь");
//        System.out.println(arrayList2);

        // removeAll() удаляет все совпадающие элементы из листа в параметрах
//        arrayList1.removeAll(arrayList2);
//        System.out.println(arrayList1);

        // retainAll() оставит только те элементы, которые есть в листе из параметров
//        arrayList1.retainAll(arrayList2);
//        System.out.println(arrayList1);

        // containsAll() проверяет, содержит ли arrayList1 все элементы из листа в параметрах (arrayList2)
//        boolean isContains = arrayList1.containsAll(arrayList2);
//        System.out.println("isContains: " + isContains);


        // subList() возвращает отрывок списка ОТ и ДО индекса
        // subList - не является отдельным листом, а является представлением (view).
        // Т.о. mySubList не является конкретной сущностью.
//        List<String> mySubList = arrayList1.subList(1, 3);
//        System.out.println("mySubList: " + mySubList);
//        mySubList.add("Федор");
//        System.out.println("arrayList1 after add: " + arrayList1);
//        System.out.println("mySubList after add: " + mySubList);
//        arrayList1.add("Света");
//        System.out.println("arrayList1: " + arrayList1);
//        // Если мы попытаемся вывести на экран mySubList, то получим исключение
//        // ConcurrentModificationException, потому что все структурные модификации
//        // в данном случае должны быть сделаны с помощью нашего представления (view).
//        // Т.о. все структурные изменения необходимо делать через представление.
//        System.out.println("arrayList1: " + mySubList);


        // Теперь массив array содержит все элементы
//        Object[] array = arrayList1.toArray();

        // Если хотим получить массив String, что в нашем случае логично,
        // необходимо действовать следующим образом
//        String[] array2 = arrayList1.toArray(new String[arrayList1.size()]);
//        String[] array2 = arrayList1.toArray(new String[0]);
//        for (String s : array2) {
//            System.out.println(s);
//        }


        // 2
        // Методы List.of() - быстрое создание списка без add()
        // и List.copyOf()
        // Оба метода являются неизменяемыми (un-modification), т.е. при попытке добавить элемент add(),
        // выбросит исключение UnsupportedOperationException.
        // Важно! Не могут содержать значения null и являются un-modification
        List<Integer> list1 = List.of(3, 8, 13);
        System.out.println("List.of: " + list1);

        List<String> list2 = List.copyOf(arrayList1);
        System.out.println("List.copyOf: " + list2);
    }
}
