package lessons.zaur_tregulov.collection.list_interface;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

// Проверка на палиндром (читаются одинаково слева направо и справа налево), например, madam
// Данный метод проверки на палиндром не является эффективным, поскольку iterator.next() и
// reverseIterator.previous() проходятся по всем чарам, когда смысл пройтись и сравнить только
// до середины, т.е. "madam".length() / 2.
public class ListIteratorExample {
    public static void main(String[] args) {
        String s = "madam"; // Почему-то не работает с кириллицей. РАЗОБРАТЬСЯ!
        System.out.println(s);
        List<Character> list = new LinkedList<>();
        for (char ch : s.toCharArray()) {
            list.add(ch);
        }
        System.out.println(list);

        ListIterator<Character> iterator = list.listIterator(); // с начала
        ListIterator<Character> reverseIterator = list.listIterator(list.size()); // с конца

        boolean isPalindrome = true;
        while (iterator.hasNext() && reverseIterator.hasPrevious()) {
            if (iterator.next() != reverseIterator.previous()) {
                isPalindrome = false;
                break;
            }
        }
        System.out.println("isPalindrome: " + isPalindrome);
    }
}
