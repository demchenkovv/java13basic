package lessons.zaur_tregulov.collection.list_interface;

import java.util.ArrayList;

public class ArrayListMethods1 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Zaur");
        arrayList.add("Ivan");
        arrayList.add("Mariya");
        arrayList.add(1, "Misha");
        for (String s : arrayList) {
            System.out.print(s + " ");
        }
        System.out.println();

        arrayList.set(1, "Masha");
        arrayList.remove(0);

        System.out.println(arrayList);
    }
}
