package lessons.zaur_tregulov.collection.list_interface;

import java.util.ArrayList;

public class ArrayListEx {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>(5);
        arrayList.add(2);
        arrayList.add(2);
        arrayList.add(2);
        System.out.println(arrayList.size());

        Integer[] array = new Integer[arrayList.size()];
        arrayList.toArray(array);
        for(Integer e : array)
            System.out.print(e + " ");
    }
}
