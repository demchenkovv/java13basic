package lessons.reflection;

public class Main {
    public static void main(String[] args)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {

        Checker checker = new Checker();
        Rabbit rabbit = new Rabbit();

        checker.showClass(rabbit);
        checker.showFields(rabbit);
        checker.showMethods(rabbit);
        checker.showFieldAnnotations(rabbit);

        checker.fillPrivateFields(rabbit);
        System.out.println("getName: " + rabbit.getName());
        System.out.println("getRole: " + rabbit.getRole());

        // Метод createNewObject(Object o) возвращает "клон" объекта rabbit
        Object clone = checker.createNewObject(rabbit);
        checker.showClass(clone);
        checker.showFields(clone);

    }
}
