package lessons.reflection;

public class Rabbit {
    @Deprecated
    String name;
    int size;
    int number;
    @RabbitAnnotation
    String role;

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public int getNumber() {
        return number;
    }

    public String getRole() {
        return role;
    }

    public void nonStaticFoo() {
        System.out.println("Non-static method");
    }

    public static void staticFoo() {
        System.out.println("Static method");
    }


}