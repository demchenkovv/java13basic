package lessons.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Checker {

    /**
     * Метод выводит информацию о классе
     */
    public void showClass(Object obj) {
        Class clazz = obj.getClass();
        System.out.println("\nshowClass: " + clazz.getName());
    }

    /**
     * Метод выводит поля класса
     */
    public void showFields(Object obj) {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("\nshowFields: ");
        for (Field f : fields)
            System.out.println(f.getName());
    }

    /**
     * Метод выводит методы класса
     */
    public void showMethods(Object obj) {
        Class clazz = obj.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        System.out.println("\nshowMethods: ");
        for (Method m : methods)
            System.out.println(m.getName());
    }

    /**
     * Метод выводит информацию о поля с аннотациями
     */
    public void showFieldAnnotations(Object obj) {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("\nshowFieldAnnotations: ");
        for (Field f : fields) {
            Annotation[] annotations = f.getAnnotations();
            for (Annotation a : annotations) {
                System.out.println(f.getName() + ":" + a.toString());
            }
        }
    }

    /**
     * Метод присваивает новые значения полям с аннотацией RabbitAnnotation
     */
    public void fillPrivateFields(Object obj) throws IllegalAccessException {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("\nshowFieldAnnotations: ");
        for (Field f : fields) {
            Annotation annotation = f.getAnnotation(RabbitAnnotation.class);
            if (annotation == null) {
                continue;
            }
            // из приватного поля делаем доступным
            f.setAccessible(true);
            // где есть аннотация RabbitAnnotation, данному
            // полю присваиваем значение на Field
            f.set(obj, "Field");
            // из доступного поля делаем обратно приватным
            f.setAccessible(false);
        }
    }

    /**
     * Метод createNewObject(Object o) возвращает "клон" объекта rabbit
     * Метод создает объект неизвестного нам класса (делать этого не рекомендуется, т.к.
     * требует больше ресурсов)
     */
    public Object createNewObject(Object obj)
    // Такие исключения могут возникать в случаях, когда мы передаем объект,
    // конструктор которого имеет параметры.
    // Наша конструкция подразумевает создание объекта без параметров
    // InstantiationException - если конструктор будет только с параметрами, а мы попытаемся создать без параметров
    // IllegalAccessException - если конструктор является приватным и к нему нет доступа
            throws InstantiationException, IllegalAccessException
    {
        Class clazz = obj.getClass();
        return clazz.newInstance();
    }
}