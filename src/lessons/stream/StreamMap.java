package lessons.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamMap {
    public static void main(String[] args) {

        //4
        // Пример с преобразованием коллекции
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            list.add(i);
        }

        // 6
//        // Стримы можем группировать и в Map (словарь)
//        Map<Integer, Integer> map = list.stream() // 1
//                .filter(digit -> digit >= 2 && digit <= 4)
//                .map(digit -> digit * 2)
//                .collect(Collectors.toMap(k -> k, v -> v * 2)); // 2
//        System.out.println("Map: " + map);
//
//
//        // 7
//        // Что делать с дублирующими элементами в Map?
//        // Добавим для примера в list дублирующимся значением 3.
//
//        list.add(3);
//        System.out.println("list after add 3: " + list);
//
//        // Получим исключение (Exception), так как Collectors.toMap ()
//        // не знает что делать с дублирующимися значениями.
//        // Чтобы этого не было, необходимо добавить mergeFunction,
//        // которое будет принимать oldValue, newValue, которое будет возвращать
//        // какой-то результат -> { }; return oldValue + newValue.
//
//        Map<Integer, Integer> map2 = list.stream() // 1
//                .filter(digit -> digit >= 2 && digit <= 4)
//                .map(digit -> digit * 2)
//                .collect(Collectors.toMap(k -> k, v -> v * 2, (oldValue, newValue) -> {
//                    System.out.println("oldValue=" + oldValue);
//                    System.out.println("newValue=" + newValue);
//                    return oldValue + newValue;
//                })); // 2
//        System.out.println("Map2: " + map2);


        // 8
        // Пример, в качестве ключа k берем остаток от деления на 3 (k % 3)
        // значения v -> v
        Map<Integer, Integer> map3 = list.stream()
                .collect(Collectors.toMap(k -> k % 3, v -> v, (oldValue, newValue) -> {
                    System.out.println("oldValue=" + oldValue);
                    System.out.println("newValue=" + newValue);
                    return oldValue + newValue;
                }));
        System.out.println("list: " + list);
        System.out.println("Map3: " + map3);
    }
}
