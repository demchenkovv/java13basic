package lessons.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamListSet {

    public static void main(String[] args) {

//        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5);

        // 1
        // терминальные операции дважды использовать нельзя, а промежуточные можно
        // применили Predicate (filter), который принимает одно, возвращает другое
//        stream.filter((Integer digit) -> {
//                    if (digit > 2) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                })
//                .filter((Integer digit) -> {
//                    if (digit >= 4) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                })
//                .forEach(System.out::println);


        // 2
        // пример со строками
//        Stream<String> newStream = stream.map((Integer digit) -> {
//            return "str" + digit;
//        });
//
//        newStream.forEach((String str ) -> {
//            System.out.println(str);
//        });


        // 3
        // Компактная запись варианта 1), но мы уже не можем записать Stream<Integer> stream =
        // так как в результате мы возвращаем строки return "newStr" + digit;
        // если хотим собрать результаты в коллекцию,используем collect(Collectors.toList())
        // Запись Integer digit упросили до digit
//        List<String> list = Stream.of(1, 2, 3, 4, 5)
//                .filter(digit -> digit > 2)
//                .map(digit -> "newStr" + digit)
////                .collect(Collectors.toList()); // преобразовать коллекцию можно так
////                // или способом ниже
//                .toList();
//
//        list.forEach(System.out::println);


        //4
        // Пример с преобразованием коллекции
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }

        list.stream()
                .filter(digit -> digit >= 2 && digit <= 4)
                .map(digit -> digit * 2)
                .forEach(System.out::println); // терминальный символ

        // ВАЖНО! СТРИМ НЕ МЕНЯЕТ КОЛЛЕКЦИЮ, НА КОТОРУЮ ОН ВЫЗВАЛСЯ.
        System.out.println("list: " + list);

        // Чтобы была еще одна измененная коллекцию, необходимо изменить код
        List<Integer> anotherList = list.stream() // 1
                .filter(digit -> digit >= 2 && digit <= 4)
                .map(digit -> digit * 2)
                .collect(Collectors.toList()); // 2
        System.out.println("anotherList: " + anotherList);


        // 5
        // Стримы можем группировать и в Set (множество)
        Set<Integer> set = list.stream() // 1
                .filter(digit -> digit >= 2 && digit <= 4)
                .map(digit -> digit * 2)
                .collect(Collectors.toSet()); // 2
        System.out.println("Set: " + set);
    }
}
