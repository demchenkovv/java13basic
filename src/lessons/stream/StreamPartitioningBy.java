package lessons.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamPartitioningBy {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            list.add(i);
        }

        // 1
        // Методы для разбивки коллекции на две категории
        // Так как разбивка идет на две категории, то
        // типизировать необходимо <Boolean, List<Integer>>,
        // так как первая категория содержит булевые значения,
        // вторая числа (сразу создали список)
        // Метод toMap() возвращает один словарь (Map)
        // Метод partitioningBy() - разбивает на две категории
//        Map<Boolean, List<Integer>> map = list.stream()
//                .collect(Collectors.partitioningBy((Integer digit) -> {
//                    if (digit <= 2) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                }));
//        System.out.println(list);
//        System.out.println(map);


        // 2
        // Метод groupingBy() - разбивает на несколько категорий
        // Данному методу необходимо передать функцию, по которому будем разбивать значения.
        // Т.е. мы хотим разбить нашу коллекцию плоскую на группы и ключом в группе будет
        // остаток от деления на 3 (digit % 3), т.е. ключ будет равным 0, 1, 3, а значения будут
        // список чисел, которые удовлетворяют данной группе.

        Map<Integer, List<Integer>> map = list.stream()
                .collect(Collectors.groupingBy((Integer digit) -> {
                    return digit % 3;
        }));

        System.out.println(list);
        System.out.println(map);


    }
}
