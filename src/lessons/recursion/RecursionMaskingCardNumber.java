package lessons.recursion;

import java.util.Scanner;

/*
Напишите программу, которая маскирует номер банковской карты таким образом, чтобы в консоли отображались
только четыре последних символа, а все остальные были заменены на символ *.
Если входная строка содержит не больше четырех символов, то вернуть входную строку.
 */
public class RecursionMaskingCardNumber {
    public static void main(String[] args) {
        String cardNumber; // номер карты

        Scanner input = new Scanner(System.in);

        // Получить номер карты
        System.out.print("Введите номер карты: ");
        cardNumber = input.next();

        // Получить маскированный номер карты с 4-мя последними элементами
        String updatedCardNumber = maskCharacters(cardNumber, 4);

        // Вывести маскированный номер карты
        System.out.println("Маскированный номер банковской карты: " + updatedCardNumber);
    }

    /**
     * Замена символов строки {@code input} символом '*'
     * с начала строки, не менять в конце {@code numberFromEnd} символа (ов)
     */
    static String maskCharacters(String input, int numberFromEnd) {
        // Если количство отражаемых в конце символов меньшу нуля
        // или больше или равно длине строки
        if (numberFromEnd < 0 || numberFromEnd >= input.length()) {
            // вернуть исходную строку
            return input;
        }
        // заменяем первый элемент строки
        // и делаем рекурсивный вызов для подстроки со 2-ого символа
        return "*" + maskCharacters(input.substring(1), numberFromEnd);
    }
}

// Далее для проверки пути выполнения программы введем номер банковской карты,
// содержащий 16 символов (например, 1234567890123456).
// Маскированный номер карты будет содержать 12 символов «*» и 4 последние цифры номера карты (************3456).