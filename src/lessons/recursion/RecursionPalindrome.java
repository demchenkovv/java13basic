package lessons.recursion;

public class RecursionPalindrome {
    public static void main(String[] args) {
        System.out.println("ропот - это палиндром? " + isPalindrome("ропот"));
        System.out.println("топот - это палиндром? " + isPalindrome("топот"));
        System.out.println("я - это палиндром? " + isPalindrome("я"));
        System.out.println("ара - это палиндром? " + isPalindrome("ара"));
        System.out.println("ар - это палиндром? " + isPalindrome("ар"));
    }

    public static boolean isPalindrome(String s) {
        return isPalindrome(s, 0, s.length() - 1);
    }

    private static boolean isPalindrome(String s, int low, int high) {
        if (high <= low) // простой случай
            return true;
        else if (s.charAt(low) != s.charAt(high)) // простой случай
            return false;
        else
            return isPalindrome(s, low + 1, high - 1);
    }
}

/*
Рекурсивный метод isPalindrome() в программе RecursivePalindromeUsingSubstring неэффективен,
поскольку создает новую строку для каждого рекурсивного вызова. Чтобы избежать создания новых строк,
можно использовать индексы low и high для указания диапазона подстроки.
Эти два индекса должны быть переданы рекурсивному методу. Поскольку исходным методом
является isPalindrome(String s), необходимо создать новый метод isPalindrome(String s, int low, int high),
чтобы принять дополнительную информацию о строке, как показано в программе RecursivePalindrome.

Здесь определены два перегруженных метода isPalindrome().
Первый метод isPalindrome(String s) проверяет, является ли строка палиндромом,
а второй метод isPalindrome(String s, int low, int high) проверяет, является ли подстрока s(low..high) палиндромом.
Первый метод передает строку s с low = 0 и high = s.length() - 1 второму методу.
Второй метод может быть рекурсивно вызван для проверки на палиндром постоянно сокращающуюся подстроку.
Этот распространенный прием проектирования в рекурсивном программировании для определения второго метода,
который получает дополнительные параметры, известен как вспомогательный рекурсивный метод.

Вспомогательные методы очень полезны при проектировании рекурсивных решений задач,
связанных со строками и массивами. Далее приведены еще два таких примера.
 */