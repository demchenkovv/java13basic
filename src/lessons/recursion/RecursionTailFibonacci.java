package lessons.recursion;

import java.util.Scanner;

public class RecursionTailFibonacci {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Получить индекс числа Фибоначчи
        System.out.print("Введите индекс числа Фибоначчи: ");
        int index = input.nextInt();

        // Найти и отобразить число Фибоначчи
        System.out.println("Число Фибоначчи с индексом "
                + index + " равно " + fib(index));
    }

    /**
     * Находит число Фибоначчи
     */
    public static long fib(long index) {
        return fib(index, 1, 0);
    }

    /**
     * Вспомогательный метод с хвостовой рекурсией для fib
     */
    private static int fib(long index, int next, int result) {
        if (index == 0)
            return result;
        else
            return fib(index - 1, next + result, next);
    }
}