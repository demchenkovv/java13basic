package lessons.recursion;
/*
Здесь первый метод ищет ключ во всем массиве.
Второй метод ищет ключ в массиве с индексами от low до high.

Первый метод recursiveBinarySearch() передает исходный массив с low = 0 и high = list.length - 1
второму методу recursiveBinarySearch().
Второй метод рекурсивно вызывается, чтобы найти ключ в постоянно сокращающемся подмассиве.
 */
public class RecursionBinarySearch {
    public static void main(String[] args) {
        int[] list = {3, 5, 7, 8, 12, 17, 24, 29};
        System.out.println(recursiveBinarySearch(list, 7));
        System.out.println(recursiveBinarySearch(list, 0));
    }

    public static int recursiveBinarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;
        return recursiveBinarySearch(list, key, low, high);
    }

    private static int recursiveBinarySearch(int[] list, int key,
                                             int low, int high) {
        if (low > high) // поиск завершается без единого совпадения
            return -low - 1;

        int mid = (low + high) / 2;
        if (key < list[mid])
            return recursiveBinarySearch(list, key, low, mid - 1);
        else if (key == list[mid])
            return mid;
        else
            return recursiveBinarySearch(list, key, mid + 1, high);
    }
}