package lessons.recursion;

import java.util.Scanner;

public class RecursionTasks {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // --- Задача № 1 ---
        //Программа для вычисления факториала с помощью рекурсивного вызова метода.
//        System.out.println("Программа для вычисления факториала с помощью рекурсивного вызова метода.");
//        System.out.print("Введите неотрицательное число: ");
//        int n = input.nextInt();
//
//        System.out.print(n + "! равен " + factorial(n));
//    }
//    public static int factorial (int n) {
//        if(n == 0)
//            return 1;
//        else
//            return n * factorial(n-1); // рекурсивный вызов
//    }


        // --- Задача № 2 ---
        // Закончите следующий рекурсивный метод, который вычисляет
        // значение числа base, возведенного в степень power.
        // Пусть power неотрицательное целое число.
//        System.out.println("Введите число и степень через пробел: ");
//        int
//                base = input.nextInt(), // число
//                power = input.nextInt(); // степень
//        System.out.println(power_raiser(base, power));
//    }
//    public static int power_raiser(int base, int power) {
//        if (power == 0)
//            return (1);
//        else
//            return (base * power_raiser(base, power - 1));
//    }


        // --- Задача № 3 ---
        // Напишите рекурсивный метод,
        // который вычисляет 1 + 2 + 3 + … + n для положительного целого числа n.
//        System.out.print("Введите число n: ");
//        int n = input.nextInt();
//        System.out.print("Сумма 1 + ... + " + n + " равна " + findSumInt(n));
//    }
//    public static int findSumInt(int n) {
//        if (n == 1)
//            return 1;
//        else
//            return n + findSumInt(n-1);

        // --- Задача № 4 ---
        // Какой результат выводит данная программа?
//        xMethod(5);
//    }
//
//    public static void xMethod(int n) {
//        if (n > 0) {
//            xMethod(n - 1); // не доходим до sout, т.к. возвращаемся к методу с новым значением
//            System.out.print(n + " "); // а далее в обратном порядке, с последнего добавленного числа
//        }
    }
}