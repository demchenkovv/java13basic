package lessons.arrays;
/*
Отсортировать числа в порядке убывания.
 */
public class ArraysSelectionSortDecreasing {
    public static void main(String[] args) {
        double[] list = {-2, 4.5, 5, 1, 2, -3.3};
        selectionSort(list);

        // вывести на экран
        for (int i = 0; i < list.length; i++)
            System.out.print(list[i] + " ");
    }

    /**
     * Сортирует массив методом выбора
     */
    public static void selectionSort(double[] list) { // c 0 до 4 (всего 5 элементов)
        for (int i = 0; i < list.length - 1; i++) {
            // Найти наименьшее значение в list[i..list.length-1]
            double currentMax = list[i]; // текущее значение i = максимальному значению
            int currentMaxIndex = i; // индекс текущей итерации

            for (int j = i + 1; j < list.length; j++) { // с 1 до 5 (всего 5 элементов)
                if (currentMax < list[j]) {
                    currentMax = list[j];
                    currentMaxIndex = j;
                }
            }

            // Переставить list[i] и list[currentMaxIndex], если необходимо
            if (currentMaxIndex != i) {
                list[currentMaxIndex] = list[i];
                list[i] = currentMax;
            }
        }
    }
}