package lessons.arrays;

// Для поиска ключа в массиве можно использовать метод binarySearch().
// Массив должен быть предварительно отсортирован в возрастающем порядке.
// Если ключ в массиве не найден, то этот метод возвращает -(insertionIndex + 1).
// Например, следующий код ищет ключи в массиве целых чисел и массиве символов:
public class ArraysBinarySearchMethod {
    public static void main(String[] args) {

        int[] list = {2, 4, 7, 10, 11, 45, 50, 59, 60, 66, 69, 70, 79};
        System.out.println("1. Индекс равен " +
                java.util.Arrays.binarySearch(list, 11));
        System.out.println("2. Индекс равен " +
                java.util.Arrays.binarySearch(list, 13));
        System.out.println("2. Индекс равен " +
                java.util.Arrays.binarySearch(list, 16));

        System.out.println();

        char[] chars = {'a', 'c', 'g', 'x', 'y', 'z'};
        System.out.println("3. Индекс равен " +
                java.util.Arrays.binarySearch(chars, 'a'));
        System.out.println("4. Индекс равен " +
                java.util.Arrays.binarySearch(chars, 't'));

        int[] list1 = {2, 4, 7, 10};
        System.out.println(java.util.Arrays.toString(list1));
    }
}
