package lessons.arrays;

public class ArraysHomeTask2 {
    public static void main(String[] args) {
        int[] list = {1, 2, 3, 4, 5};
        int[] reverse = reverse(list);

//        for (int i = 0; i < reverse.length; i++)
//            System.out.print(reverse[i] + " ");

        for (int num : reverse) {
            System.out.print(num + " ");
        }
    }


    public static int[] reverse(int[] list) {
        int[] newList = new int[list.length];
        for (int i = 0; i < list.length; i++) {
            newList[i] = list[list.length - 1 - i];
//            list = newList;
        }
        return newList;
    }
}