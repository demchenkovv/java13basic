package lessons.arrays;

public class ArraysLinearSearch {
    // Метод линейного поиска последовательно сравнивает
    // ключевой элемент (key) с каждым элементом массива.
    public static void main(String[] args) {

        int[] list = {4, 5, 1, 2, 9, -3};
        int i = linearSearch(list, 4); // Возвращает 1
        int j = linearSearch(list, -4); // Возвращает -1
        int k = linearSearch(list, -3); // Возвращает 5
        System.out.println(linearSearch(list, 2)); // Возвращает 3
    }

    /**
     * Применяет метод линейного поиска ключа в массиве
     */
    public static int linearSearch(int[] list, int key) {
        for (int i = 0; i < list.length; i++) {
            if (key == list[i])
                return i;
        }
        return -1;
    }
}