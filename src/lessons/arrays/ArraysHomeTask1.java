package lessons.arrays;

public class ArraysHomeTask1 {
    public static void main(String[] args) {

        int[] x = {1, 2, 3, 4, 5};
        increase(x);

        for (int num : x) {
            System.out.print(num + " ");
        }
    }

    // void ничего не должен возвращать, а весь массив был проинкрементирован
    // и передан в main
    public static void increase(int[] x) {
        for (int i = 0; i < x.length; i++)
            x[i]++;
    }
}