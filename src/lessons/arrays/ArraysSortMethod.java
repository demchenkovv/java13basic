package lessons.arrays;

import java.util.Arrays; // сразу импортировать, чтобы не вписывать каждый раз java.util внутри метода

// При вызове функции sort(numbers) сортируются все числа массива.
// При вызове функции sort(chars, 1, 3) сортируется часть массива, начиная с chars[1] и
// заканчивая chars[3-1]. Если компьютер имеет несколько процессоров,
// то метод parallelSort() является более эффективным.

public class ArraysSortMethod {
    public static void main(String[] args) {

        double[] numbers = {6.0, 4.4, 1.9, 4.9, 3.8, 3.5};
        Arrays.sort(numbers, 1, 5); // Сортирует часть массива (с 1 по 5-1)
        java.util.Arrays.sort(numbers); // Сортирует весь массив
        for (double elements : numbers) {
            System.out.print(elements + " ");
        }
        // java.util.Arrays.parallelSort(numbers); // Сортирует весь массив

        char[] chars = {'a', 'A', '4', 'F', 'D', 'P'};
        java.util.Arrays.sort(chars, 1, 3); // Сортирует часть массива
        java.util.Arrays.parallelSort(chars, 1, 3); // Сортирует часть массива
    }
}