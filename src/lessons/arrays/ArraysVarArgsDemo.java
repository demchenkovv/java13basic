package lessons.arrays;

public class ArraysVarArgsDemo {
    public static void main(String[] args) {

        printMax(34, 3, 3, 2, 56.5);
        printMax(new double[]{1, 2, 3});
        printMax(1, 2, 2, 1, 4);
        printMax(1.0, 2.0, 2.0, 1.0, 4.0);
        //printMax(new int[]{1, 2, 3}); - недопустимо, так как метод использует тип double

    }

    public static void printMax(double... numbers) {
        if (numbers.length == 0) {
            System.out.println("Ни один аргумент не передан");
            return;
        }

        double result = numbers[0];

        for (int i = 1; i < numbers.length; i++)
            if (numbers[i] > result)
                result = numbers[i];

        System.out.println("Наибольшее значение равно " + result);
    }
}