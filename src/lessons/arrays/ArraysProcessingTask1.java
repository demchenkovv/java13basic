package lessons.arrays;

public class ArraysProcessingTask1 {
    public static void main(String[] args) {

        // 1. Создайте массив, содержащий 10 значений типа double.
        double[] array = {6, 12, 2, 11, 11, 43, 55, 18, 9, 10};

        // 2. Присвойте значение 5.5 последнему элементу массива.
        array[array.length - 1] = 5.5;

        // 3. Отобразите сумму первых двух элементов массива.
        System.out.println(array[0] + array[1]);

        // 4. Напишите цикл, который вычисляет сумму всех элементов массива.
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        System.out.println(sum);

        // 5. Напишите цикл, который находит наименьший элемент массива.
        double min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min)
                min = array[i];
        }
        System.out.println(min);

        // 6. Сгенерируйте случайным образом индекс и отобразите значение элемента этого индекса.
        // из array.length не вычитаем 1, так как последний индекс (число) не входит в диапазон случайных чисел
        System.out.println(array[(int)(Math.random() * array.length)]);

        // 7. С помощью инициализатора массива создайте еще один массив с начальными значениями 3.5, 5.5, 4.52 и 5.6.
        double[] secondArray = {3.5, 5.5, 4.52, 5.6};


    }
}
