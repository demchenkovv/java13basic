package lessons.nestedClassesAndGenerics.nestedClasses;

/**
 * В non-static классе User есть вложенный static класс Builder,
 * который позволяет обеспечить конструирование объекта
 */
public class User {

    private String id;
    private String firstName;
    private String secondName;
    private String lastName;
    private String phone;
    private int age;
    private String role;

    public static Builder builder() {
        return new Builder();
    }

    /**
     * вложенный static класс Builder, который
     * позволяет обеспечить конструирование объекта (new User)
     */
    public static class Builder {

        private User obj = new User();

        public Builder setId(String id) {
            obj.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            obj.secondName = secondName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setPhone(String phone) {
            obj.phone = phone;
            return this;
        }

        public Builder setAge(int age) {
            obj.age = age;
            return this;
        }

        public Builder setRole(String role) {
            obj.role = role;
            return this;
        }

        public User build() {
            if (obj.firstName == null || obj.lastName == null) {
                throw new IllegalArgumentException("First name or last name are required");
            }
            return obj;
        }
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public int getAge() {
        return age;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", age=" + age +
                ", role=" + role +
                '}';
    }
}
