package lessons.nestedClassesAndGenerics.nestedClasses;

/**
 * Hello world!
 */
public class Application {

    public static void main(String[] args) {
//        // Создание объекта пользователя
//        User user = User.build()
//                .setId("1")
//                .setFirstName(null)
//                .setLastName("Ivanov")
//                .setAge(20)
//                .setPhone("8-800-1")
//                .setRole("STANDARD_USER")
//                .build();
//
//        System.out.println("user=" + user);


//        // Работа с журналом студентов
//        Journal journal = new Journal();
//
//        User firstUser = User.builder()
//                .setId("1")
//                .setFirstName("Anton")
//                .setLastName("Ivanov")
//                .build();
//
//        User secondUser = User.builder()
//                .setId("2")
//                .setFirstName("Denis")
//                .setLastName("Petrov")
//                .build();
//
//        journal.add(firstUser);
//        journal.add(secondUser);
//
//        Iterator<User> it = journal.getUserIterator();
//        while (it.hasNext()) {
//            User user = it.next();
//            System.out.println("user=" + user);
//        }


        // Создали анонимный класс, который реализует интерфейс Runnable
        // Использование анонимного класса
        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("I'm in run method!");
            }
        };

        // Использование лямбды
        Runnable newTask = () -> {
            System.out.println("I'm in run method too...");
        };

        task.run();
        newTask.run();

    }
}
