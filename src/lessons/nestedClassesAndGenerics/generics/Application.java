package lessons.nestedClassesAndGenerics.generics;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
//        // 1. Демонстрация работы с дженериками <T>, без необходимости создания методов под каждый тип данных
//        SimplePrinter printer = new SimplePrinter();
//
//        printer.print(10);
//        printer.print(10.0);
//        printer.print(10F);
//        printer.print("Hello");
//        printer.print(new Long(10));
//        printer.print(new Integer(1000));

//        // 2. Демонстрация работы с дженериками
//        ValueHolder<Integer> holder = new ValueHolder<>(10);
//        holder.setValue(200);
//        System.out.println("holder=" + holder);
//
//        ValueHolder<String> stringHolder = new ValueHolder<>("Hello Generics!");
//        stringHolder.setValue(stringHolder.getValue() + " Modified NOW");
//        System.out.println("holder=" + stringHolder);


//        // 3. public interface List<E> extends Collection<E> {
//        // В List можно подставить любой непримитивный тип
//        // (String, Integer, Object и т.д., т.е. любым типом)
//        // нам всегда необходимо использовать типизацию, т.е. в <>
//        // указывать конкретный тип данных, который мы планируем
//        // хранить и обрабатывать в дальнейшем
        List<String> list = new ArrayList<>();

        list.add("Anton");
        list.add("Denis");
        list.add(null);

        System.out.println("list=" + list);

        String value = list.get(1);
        System.out.println("list.get(1): " + value);

//        // Применение метода compare, реализованный в SimplePrinter
//        ValueHolder<String> stringHolder = new ValueHolder<>("A");
//        System.out.println("result: " + stringHolder.compare("B"));

        // 4. Демонстрация того, что в коллекцию с типом А мы можем
        // добавить наследников от А
//        List<A> list = new ArrayList<>();
//        list.add(new A());
//        list.add(new B());


//        // 5. Wildcard c верхней границей <? extends type>
//        List<A> oneCollection = new ArrayList<>();
//        oneCollection.add(new A());
//        oneCollection.add(new A());
//
//        List<B> secondCollection = new ArrayList<>();
//        secondCollection.add(new B());
//        secondCollection.add(new B());
//        // таким образом мы говорим, что можем добавить
//        // ТОЛЬКО наследников от класса А
//        // т.е. List, который типизирован неким классом,
//        // который расширяет класс А, т.е. справа мы можем присвоить любую коллекцию,
//        // которая типизирована типом, которая расширяет класс А
//        List<? extends A> listOne = oneCollection;
//        foo(listOne);
//    }
//    // добавить в коллекцию мы ничего не можем, поскольку не знаем,
//    // что в ней находится. У листа мы можем вызывать метод add,
//    // но добавить ничего не можем. А вот вызывать get мы можем.
//    private static void foo(List<? extends A> list) {
//        A value = list.get(0); // здесь мы можем использовать только класс А
//        // т.е. здесь создали продюсера - получать можем, но изменять коллекцию не можем
//    }


        // 6. Wildcard c нижней границей <? super type>
        List<A> oneCollection = new ArrayList<>();
        oneCollection.add(new A());
        oneCollection.add(new A());

        List<B> secondCollection = new ArrayList<>();
        secondCollection.add(new B());
        secondCollection.add(new B());

        // Мы можем присвоить коллекцию, которая типизирована типом А или классом,
        // который находится уровнем выше по уровню иерархии (наследования), т.е. классом Object
        List<? super A> listOne = oneCollection;
        foo(listOne);
    }

    // Здесь мы говорим в параметрах, что может поступить коллекция,
    // которая тимизирована типом В или по иерархии наследования выше (super),
    // т.е. типом B, типом A или Object. Метод add работает, но get не работает,
    // т.к. мы не знаем каким типом она тимизирована (может как В, как А, так и Object)
    // Можем указать только Object, посколько является родителем любого класса,
    // а указать явный тип А или В мы не можем. Т.е. менять (добавлять) можем,
    // но получить данные из нее нельзя - Consumer
    private static void foo(List<? super A> list) {
        list.add(new B());
        list.add(new A());
        Object value = list.get(1);
    }
}
