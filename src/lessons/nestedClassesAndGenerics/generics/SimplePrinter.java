package lessons.nestedClassesAndGenerics.generics;

public class SimplePrinter {

    /**
     * Соглашение по наименованию
     * Типовой параметр может быть назван как угодно. Используют
     * одинарные заглавные буквы, чтобы сделать очевидным, что
     * это не настоящие имена классов:
     * E - для элемента
     * K - для ключа Map
     * V - для значения Map
     * N - для числа
     * T - для общего типа данных
     * S, U, V и т.д. - для множественных общих типов
     */

    /** Из интернета:
     * Для обозначения дженерик-типа в классе Box мы использовали
     * латинскую букву T. Это необязательно, то есть можно было бы
     * использовать любую другую букву или даже слово — Box<MyType>.
     * Тем не менее есть набор рекомендаций от Oracle о том, когда
     * какие обозначения лучше использовать в дженериках. Вот они:
     * E — element, для элементов параметризованных коллекций;
     * K — key, для ключей map-структур;
     * V — value, для значений map-структур;
     * N — number, для чисел;
     * T — type, для обозначения типа параметра в произвольных классах;
     * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
     *  */

    public <T> void print(T digit) {
        System.out.println(">" + digit);
    }
}
