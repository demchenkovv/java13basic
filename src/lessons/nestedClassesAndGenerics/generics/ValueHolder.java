package lessons.nestedClassesAndGenerics.generics;

// В <> мы говорим, что тип Т должен расширять (наследуется от) Comparable от Т
// т.е. данный класс должен реализовать Comparable
public class ValueHolder <T extends Comparable<T>> {
    private T value;

    // Конструктор, который принимает произвольный тип
    public ValueHolder(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public int compare(T other) {
        return value.compareTo(other);
    }

    @Override
    public String toString() {
        return "ValueHolder{" +
                "value=" + value +
                '}';
    }
}
