package lessons.cycles;

import java.util.Scanner;

/*
Написать программу, которая вычисляет ежемесячные проценты по вкладу (без капитализации),
исходя из введенных пользователем месяца и года открытия вклада, срока вклада в месяцах,
годовой процентной ставки и суммы вклада в рублях.
Пусть количество дней в году будет всегда равно 365, т.е. високосные года в вычислениях не учитываются.
 */
public class CyclesDepositMy {
    public static final int MONTHS_PER_YEAR = 12;

    public static void main(String[] args) {
        int month, year, period, interestRate;
        double amountDeposit, interestPerMonth, total;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите месяц и год открытия вклада: ");
        month = scanner.nextInt();
        year = scanner.nextInt();

        System.out.print("Введите срок вклада в месяцах и процентную ставку: ");
        period = scanner.nextInt();
        interestRate = scanner.nextInt();

        System.out.print("Введите сумму депозита: ");
        amountDeposit = scanner.nextInt();

        interestPerMonth = amountDeposit * interestRate / 100.0 / MONTHS_PER_YEAR; // ежемесячные проценты
        interestPerMonth = Math.round(interestPerMonth * 100) / 100.0;


        for (int i = 1; i <= period; i++) {
            amountDeposit += interestPerMonth;
            total = amountDeposit + interestPerMonth;
            System.out.println("В " + i + " месяце Вы получите " + interestPerMonth + ". Накопленная сумма "
                    + (Math.round(total * 100) / 100.0));

        }
    }
}
