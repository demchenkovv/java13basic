package lessons.cycles;
/*Напишите программу, которая запрашивает у пользователю строку и отображает ее в обратном порядке.
Пример запуска:
Введите строку: ABCD
Обратная строка равна DCBA
*/
public class CyclesCharDCBA {
    public static void main(String[] args) {

        String s = "ABCD";
        int last = s.length() - 1;

        while (last >= 0) {
            System.out.print(s.charAt(last));
            last--;
        }
    }
}
