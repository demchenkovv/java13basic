package lessons.cycles;

import java.time.LocalDate;
import java.util.Scanner;

public class CyclesDepositSberLocalData {
    public static final int MONTHS_OF_YEAR = 12;
    public static final int DAYS_YEAR = 365;

    public static void main(String[] args) {

        int startMonth; // номер открытия месяца
        int startYear; // год открытия месяца
        int depositTerm; // срок вклада в месяцах
        double rate; // процентная ставка (% годовых)
        double depositAmount; // сумма вклада
        double accruedInterestPerMonth;
        double accruedInterestTotal = 0;

        Scanner input = new Scanner(System.in);

        // Получить номер месяца открытия вклада
        System.out.println("Введите номер месяца открытия вклада: ");
        startMonth = input.nextInt();

        // Получить год открытия вклада
        System.out.println("Введите год открытия вклада: ");
        startYear = input.nextInt();

        // Получить срок вклада в месяцах
        System.out.println("Введите срок вклада в месяцах: ");
        depositTerm = input.nextInt();

        // Получить сумму вклада
        System.out.println("Введите сумму вклада: ");
        depositAmount = input.nextDouble();

        // Получить годовую процентную ставку
        System.out.println("Введите годовую процентную ставку: ");
        rate = input.nextDouble();

        int monthsCount = 0; // счетчик месяцев
        int i = startYear;
        while (monthsCount < depositTerm) {
            for (int j = 1; j <= MONTHS_OF_YEAR; j++) { // итерация по месяцам с 1 до 12
                // Если номер месяца меньше или равно номеру месяца открытия вклада
                // то переходим к следующему месяцу, он не попадает в интервал
                if (i == startYear && j <= startMonth) {
                    continue; // // пропускаем текущую итерацию, переходим к следующему месяцу
                }
                // Если номер текущего месяца равен сроку вклада,
                // то расчет заканчиваем и выходим из цикла
                if (monthsCount == depositTerm) {
                    break;
                }

                // Получаем количество дней в текущем месяце
                int lengthOfMonth = LocalDate.of(i, j, 1).lengthOfMonth();

                // Рассчитаем доход по вкладу в текущем месяцев
                accruedInterestPerMonth = depositAmount * rate / 100 / DAYS_YEAR * lengthOfMonth;
                accruedInterestPerMonth = Math.round(accruedInterestPerMonth * 100) / 100.0; // округление до копейки

                // Вывести результат начисленных процентов в месяце
                System.out.println("Начисление процентов в " + i + " году " + j + " месяца = " + accruedInterestPerMonth);

                // Добавим процент по вкладам текущего месяца к суммарной выплате
                accruedInterestTotal = accruedInterestTotal + accruedInterestPerMonth;

                monthsCount++; // увеличим счетчик месяцев на 1

            }
            i++; // увеличивем год на 1
        }

        // Вывести итоговый результат начисленных процентов
        System.out.println("Всего начислено процентов = " + accruedInterestTotal);

    }
}

/*

ОПИСАНИЕ

Сначала внутри класса объявим константы:

месяцев в году — 12;
дней в году — 365.
static final int MONTHS_OF_YEAR = 12; // месяцев в году
static final int DAYS_YEAR = 365; // дней в году
Внутри метода main объявим переменные, необходимые для вычислений, пять из которых необходимы для данных, вводимых пользователем, и две — для результатов расчета.

int startMonth; // номер месяца открытия вклада
int startYear; // год открытия вклада
int depositTerm; // срок вклада в месяцах
double rate; // процентная ставка (% годовых)
double depositAmount; // сумма вклада
double accruedInterestPerMonth; // доход по вкладу в текущем месяце
double accruedInterestTotal = 0; // суммарный доход по вкладу
Сначала получим из консоли номер месяца открытия вклада.

Scanner input = new Scanner(System.in);
// Получить номер месяц открытия вклада
System.out.print("Введите номер месяца открытия вклада: ");
startMonth = input.nextInt();
Далее получим из консоли год открытия вклада.

// Получить год открытия вклада
System.out.print("Введите год открытия вклада: ");
startYear = input.nextInt();
Далее получаем срок вклада в месяцах.

// Получить срок вклада в месяцах
System.out.print("Введите срок вклада в месяцах: ");
depositTerm = input.nextInt();
Далее получаем сумму вклада в рублях.

// Получить сумму вклада
System.out.print("Введите сумму вклада: ");
depositAmount = input.nextDouble();
Далее получаем годовую процентную ставку.

// Получить годовую процентную ставку
System.out.print("Введите годовую процентную ставку: ");
rate = input.nextDouble();
Добавим дополнительную переменную, необходимую для расчета, — счетчик месяцев.

int monthsCount = 0; // счетчик месяцев
С помощью цикла while будем итерироваться по годам, начиная с года открытия вклада.

int i = startYear;
while (monthsCount < depositTerm) { // выполняем пока счетчик месяцев меньше срока вклада
  // в цикле будем увеличивать счетчик месяцев на 1
  i++; // увеличим год на 1
}
С помощью вложенного цикла for будем итерироваться по месяцам в году.

for (int j = 1; j <= MONTHS_OF_YEAR; j++) { // итерация по месяцам с 1 до 12
}
С помощью оператора перехода continue пропускаем месяц первого года, если он не попадает в интервал расчета.

// Если номер месяца меньше или равно номеру месяца открытия вклада
// то переходим к следующему месяцу, он не попадает в интервал
if (i == startYear && j <= startMonth) {
  continue; // пропускаем текущую итерацию, переходим к следующему месяцу
}
С помощью оператора перехода break выходим из расчета, если достигли конца срока вклада.

// Если номер текущего месяца равен сроку вклада,
// то расчет заканчиваем и выходим из цикла
if (monthsCount == depositTerm) {
  // выходим из текущего цикла for, из внешнего цикла while выходим по условию monthsCount < depositTerm
  break;
}
Получаем количество дней в текущем месяце с помощью класса LocalDate из пакета java.time.

// Получаем количество дней в текущем месяце
int lengthOfMonth = LocalDate.of(i, j, 1).lengthOfMonth();
Рассчитаем доход по вкладу в текущем месяце.

// Рассчитаем доход по вкладу в текущем месяце
accruedInterestPerMonth = depositAmount * rate / 100 / DAYS_YEAR * lengthOfMonth;
Результат расчета округлим до копеек.

accruedInterestPerMonth = Math.round(accruedInterestPerMonth * 100) / 100.0; // округление до копейки
Выведем результат начисленных процентов в текущем месяце.

// Вывести результат начисленных процентов в месяце
System.out.println("Начислено процентов в " + i + " году " + j + " месяца = " + accruedInterestPerMonth);
Добавим процент по вкладам текущего месяца к суммарной выплате по вкладу.

// Добавим процент по вкладам текущего месяца к суммарной выплате
accruedInterestTotal = accruedInterestTotal + accruedInterestPerMonth;
Увеличим счетчик месяцев на 1.

monthsCount++; // увеличим счетчик месяцев на 1
Выведем итоговый результат начисленных процентов за весь период.

// Вывести итоговый результат начисленных процентов
System.out.println("Всего начислено процентов = " + accruedInterestTotal);
Запускаем готовую программу.

Введем для проверки номер месяца открытия вклада равным 3, год открытия равным 2021, срок вклада равным 2 года или 24 месяца, сумму вклада равной 1000 рублей, годовую процентную ставку равной 5%.

Проценты начинают начисляться с четвертого месяца 2021 года. Всего 24 начисления. Начисления в каждом месяце отличаются из-за разности количества дней в месяце.
 */
