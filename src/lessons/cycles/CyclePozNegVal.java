package lessons.cycles;
/*
Напишите программу, которая запрашивает у пользователя числовые значения,
а выводит количество положительных и количество отрицательных введенных значений.
Подсказка: в качестве значения сигнальной метки используйте 0.
*/
import java.util.Scanner;

public class CyclePozNegVal {
    public static final int SENTINEL = 0;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int positiveValue = 0;
        int negativeValue = 0;
        int curentValue;

        System.out.print("Введите число или " +SENTINEL + " для выхода: ");
        curentValue = scanner.nextInt();

        while (curentValue != SENTINEL) {
            if (curentValue > 0) {
                positiveValue += 1;
            } else {
                negativeValue += 1;
            }
            System.out.print("Введите следующее число или " + SENTINEL + " для выхода: ");
            curentValue = scanner.nextInt();
        }
        System.out.println("Вы ввелите " + positiveValue + " положительных чисел и " +
                negativeValue + " отрицательных чисел.");
    }
}