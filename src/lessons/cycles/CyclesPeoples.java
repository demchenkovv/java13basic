package lessons.cycles;

public class CyclesPeoples {
    public static void main(String[] args) {


        int count_people = 9870;
        int count_years = 0;
        int count_people_inc;

        while (count_people < 30000) {
            count_people_inc = count_people / 10;
            count_people += count_people_inc;
            count_years++;
        }
        System.out.println("Через " + count_years + " население будет " + count_people);
    }
}