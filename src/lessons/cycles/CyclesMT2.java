package lessons.cycles;

public class CyclesMT2 {
    public static void main(String[] args) {

        // Заголовок
        System.out.println("\n\t\t\t\t ТАБЛИЦА УМНОЖЕНИЯ \n");

        // Столбцы
        for (int i = 0; i <= 9; i++) {
            System.out.print("\t" + i);
        }
        System.out.println();

        for (int i = 0; i <= 9; i++) {
            System.out.print("\t" + "_");
        }
        System.out.println();

        // Строки и умножение
        for (int i = 0; i <= 9; i++) { // вножитель 0
            System.out.print(i + " |");
            for (int j = 0; j <= 9; j++) { // множитель с 0 до 9 сразу
                System.out.print("\t" + i * j); // результат
            }
            System.out.println(); // новая строка, повторяем итерацию со строки 21
        }
    }
}