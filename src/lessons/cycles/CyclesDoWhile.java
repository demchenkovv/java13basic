package lessons.cycles;

import java.util.Scanner;

public class CyclesDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n1, n2, temp1, temp2;

        do {
            System.out.print("Введите число n1 и n2 через пробел: ");
            n1 = scanner.nextInt();
            n2 = scanner.nextInt();
            temp1 = n1 % n2;
            temp1 = n2 % n1;
        } while (n1 % n2 != 0 && n2 % n1 != 0);
        System.out.println("Одно кратно другому");
    }
}