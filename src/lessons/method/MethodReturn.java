package lessons.method;

public class MethodReturn {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        int i, j, k, t;
        i = 5;
        j = 2;
        t = 0;
        k = max(i, j, t); // вызов метода max
        System.out.println("Наибольшее из " + i +
                " и " + j + " равно " + k);
    }

    /**
     * Возвращает наибольшее из двух чисел
     */
    public static int max(int num1, int num2, int num3) {
        int result;

        if (num1 > num2 && num1 > num3)
            result = num1;
        else
            result = num2;

        return result; // возврат result
    }
}