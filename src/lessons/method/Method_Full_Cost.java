package lessons.method;

import java.util.Scanner;

// Напишите программу, которая вычисляет полную стоимость товара.
// Данные, используемые для расчета:
// стоимость единицы товара в рублях, количество единиц товара,
// включение/не включение НДС (20%) в стоимость товара.
// Если количество единиц товара больше или равно 10, то действует скидка 5%.
// Подсказка: для получения полной стоимости товара используйте перегруженные методы.
public class Method_Full_Cost {
    public static final double VAT = 20; // НДС 20% (1 + 20 / 100) = 1.2
    public static final int DISCOUNT = 5; // скидка 5% (1 - 5 / 100) = 0.95

    /**
     * Метод main
     */
    public static void main(String[] args) {
        double price;
        int amount;
        String answerVAT;
        double fullCost;

        Scanner input = new Scanner(System.in);

        System.out.print("Введите стоимость единицы товара в рублях: ");
        price = input.nextInt();

        System.out.print("Введите стоимость количество единиц товара: ");
        amount = input.nextInt();

        // Получить ответ на включение НДС в стоимость товара до тех пор,
        // пока не введено корректное значение
        do {
            System.out.print("Включать НДС в стоимость товара (y/n) ");
            answerVAT = input.next();
        } while (!answerVAT.equalsIgnoreCase("y") && !answerVAT.equalsIgnoreCase("n"));

        // Получить полную стоимость товара по полученным данным
        if (answerVAT.equalsIgnoreCase("y")) {
            // в стоимость включать НДС
            if (amount >= 10) {
                // в стоимости учитывать скидку
                fullCost = fullCost(price,amount, DISCOUNT, VAT);
            } else {
                // в стоимости не учитывать скидку
                fullCost = fullCost(price, amount, VAT);
            }

        } else {
            // в стоимость не включать НДС
            if (amount >= 10) {
                // в стоимости учитывать скидку
                fullCost = fullCost(price, amount, DISCOUNT);
            } else {
                // в стоимости не учитывать скидку
                fullCost = fullCost(price, amount);
            }
        }
        System.out.println("Полная стоимость товара составит: " + fullCost);
    }

    /**
     * расчет без НДС и без скидки
     */
    public static double fullCost(double price, int amount) {
        double notVatNotDiscount = price * amount;
        return notVatNotDiscount;
    }

    /**
     * расчет без НДС и со скидкой
     */
    public static double fullCost(double price, int amount, int discount) {
        double notVatWithDiscount = price * amount * (1 - DISCOUNT / 100.0);
        return notVatWithDiscount;
    }

    /**
     * расчет c НДС и без скидки
     */
    public static double fullCost(double price, int amount, double vat) {
        double withVatNotDiscount = price * amount * (1 + VAT / 100.0);
        return Math.round(withVatNotDiscount * 100) / 100.0;
    }

    /**
     * расчет c НДС и со скидкой
     */
    public static double fullCost(double price, int amount, int discount, double vat) {
        double withVatWithDiscount = price * amount * (1 - DISCOUNT / 100.0) * (1 + VAT / 100.0);
        return Math.round(withVatWithDiscount * 100) / 100.0;
    }
}
