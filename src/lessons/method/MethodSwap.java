package lessons.method;
/*
Программа TestPassByValue демонстрирует результаты передачи аргументов по значению.
В этой программе для перестановки значений двух переменных определяется метод swap(),
который вызывается с передачей двух аргументов.
Интересно, что значения аргументов НЕ меняются после вызова метода.
 */
public class MethodSwap {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        int num1 = 1;
        int num2 = 2;

        System.out.println("До вызова метода swap() num1 равно " +
                num1 + " и num2 равно " + num2);

        // Вызвать метод swap для замены значений двух переменных
        swap(num1, num2);

        System.out.println("После вызова метода swap() num1 равно " +
                num1 + " и num2 равно " + num2);
    }

    /**
     * Переставляет значения двух переменных
     */
    public static void swap(int n1, int n2) {
        System.out.println("\tВнутри метода swap()");
        System.out.println("\t\tДо замены, n1 равно " + n1
                + " и n2 равно " + n2);

        // Переставить n1 и n2
        int temp = n1;
        n1 = n2;
        n2 = temp;

        System.out.println("\t\t\tПосле замены, n1 равно " + n1
                + " и n2 равно " + n2);
    }
}