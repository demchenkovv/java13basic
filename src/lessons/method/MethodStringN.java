package lessons.method;

class MethodStringN {
    public static void main(String[] args) {
        // Инициализировать times
        int times = 3;
        System.out.println("До вызова," + " значение times равно " + times);
        // Вызвать nPrintln и отобразить times
        nPrintln("Welcome to Java!", times);
        System.out.println("После вызова," + " значение times равно " + times);
    }

    // Отобразить message n раз
    public static void nPrintln(String message, int n) {
        while (n > 0) {
            System.out.println("n = " + n);
            System.out.println(message);
            n--;
        }
    }
}