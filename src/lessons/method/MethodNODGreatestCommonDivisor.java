package lessons.method;

import java.util.Scanner;

// Напишите программу, которая вычисляет и отображает наибольший общий делитель (далее — НОД) двух чисел.
public class MethodNODGreatestCommonDivisor {
    /**
     * Метод main
     */
    public static void main(String[] args) {
        int n1, n2, nod;

        Scanner input = new Scanner(System.in);

        // Получить два числа
        System.out.print("Введите число n1: ");
        n1 = input.nextInt();
        System.out.print("Введите число n2: ");
        n2 = input.nextInt();

        // Вычислить НОД двух чисел
        nod = find_nod(n1, n2);

        // Отобразить НОД двух чисел
        System.out.print("Наибольший общий делитель для чисел " + n1 + " и " + n2 + " равен " + nod);
    }
    /**
     * Возвращает НОД двух чисел
     */
    public static int find_nod(int n1, int n2) {
        int nod, k;
        nod = 1; // НОД инициализируется с 1
        k = 1; // предполагаемое значение НОД

        while (k <= n1 && k <= n2) {
            if (n1 % k == 0 && n2 % k == 0)
                nod = k; // НОД обновляется
            k++;
        }
        return nod;
    }
}