package lessons.method;


public class Zadachky {
    public static void main(String[] args) {
// задачка 1
//        int[][] val = {{3, 4, 5, 1}, {33, 6, 1, 2}};
//        int v = val[0][0];
//        for (int[] list : val)
//            for (int element : list)
//                if (v > element)
//                    v = element;
//        System.out.println(v);

// задачка 2
        int[][] values = {
                {3, 4, 5, 1},
                {33, 6, 1, 2}
        };

        for (int i = 0; i < values.length; i++) { // [0] [1]
            System.out.print(m(values[i]) + " "); // 5 33

        }
    }

    public static int m(int[] list) { // 33 6 1 2
        int v = list[0]; // 5
        for (int i = 0; i < list.length; i++) // 3 4 5 1
            if (v < list[i]) // 3 <
                v = list[i];
        return v; //5



    }
}
