package lessons.otherTasks;
public class CharTest {
    public static void main(String[] args) {

        System.out.println((int)'1');
        System.out.println((int)'A');
        System.out.println((int)'B');
        System.out.println((int)'a');
        System.out.println((int)'b');
        System.out.println();
        System.out.println((char)40);
        System.out.println((char)59);
        System.out.println((char)79);
        System.out.println((char)85);
        System.out.println((char)90);
        System.out.println();
        System.out.println((char)0x40);
        System.out.println((char)0x5A);
        System.out.println((char)0x71);
        System.out.println((char)0x72);
        System.out.println((char)0x7A);
        System.out.println();

        int i = '1'; //49
        int j = '1' + '2' * ('4' - '3') + 'b' / 'a'; // 49 + 50 * (1) + (1)
        int k = 'a'; //97
        char c = 90; // Z
        System.out.println(i);
        System.out.println(j);
        System.out.println(k);
        System.out.println(c);

        char x = 'a'; //97
        char y = 'c'; //99
        System.out.println(++x); //98 b
        System.out.println(y++); //99 c
        System.out.println(x - y); // 98 - 100 +0

        // Напишите выражение, которое генерирует случайную строчную латинскую букву.
        System.out.println((char)(97 + (int)(Math.random() * (123-97)))); // лучше написать как ниже
        System.out.println((char)(Math.random() * 26 + 'a')); //  26 - это разница + 97 ('а')

        System.out.println('a' < 'b');
        System.out.println('a' <= 'A');
        System.out.println('a' > 'b');
        System.out.println('a' >= 'A');
        System.out.println('a' == 'a');
        System.out.println('a' != 'b');

        String s1 = "Welcome to Java";
        String s2 = "Welcome to java";
        String s3 = "Welcome to C++";
        System.out.println(s1.equals(s2)); // true
        System.out.println(s1.equals(s3)); // false
        System.out.println();

        System.out.println("Welcome to Java".indexOf("Java", 5)); //возвращает 11.
        System.out.println("Welcome to Java".indexOf("java", 5)); //возвращает -1.

    }
}