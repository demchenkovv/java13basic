package lessons.otherTasks;

public class Increment {
    public static void main(String[] args) {

        int i = 3, res;

        res = i++ + 1 + i++ * 2 + ++i;

        // 3 + 1 + 4 * 2 + 6

        System.out.println(res);
        System.out.println(i);

    }
}
