package lessons.otherTasks;

import java.util.Scanner;

public class Triangle {

    public static void main(String[] args) {

        double x1, y1, x2, y2, x3, y3; // декартовы координаты трех вершин треугольника
        double a, b, c; // длины трех сторон треугольника
        double d, e, f; // размеры трех углов треугольника в градусах

        Scanner input = new Scanner(System.in);

        // Получить декартовы координаты трех вершин треугольника
        System.out.print("Введите декартовы координаты трех вершин треугольника "
                + "через пробел \nx1 y1 x2 y2 x3 y3: ");
        x1 = input.nextDouble(); // α
        y1 = input.nextDouble(); // α
        x2 = input.nextDouble(); // β
        y2 = input.nextDouble(); // β
        x3 = input.nextDouble(); // γ
        y3 = input.nextDouble(); // γ

        a = Math.sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3)); // α
        b = Math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3)); // β
        c = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); // γ

        d = Math.toDegrees(Math.acos((a * a - b * b - c * c) / (- 2 * b * c))); // α
        e = Math.toDegrees(Math.acos((b * b - a * a - c * c) / (- 2 * a * c))); // β
        f = Math.toDegrees(Math.acos((c * c - a * a - b * b) / (- 2 * a * b))); // γ

        System.out.println("Три угла треугольника равны ");
        System.out.println(Math.round(d * 10.0) / 10.0);
        System.out.println(Math.round(e * 10.0) / 10.0);
        System.out.println(Math.round(f * 10.0) / 10.0);

    }
}
