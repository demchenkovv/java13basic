package lessons.otherTasks;

import java.util.Scanner;

public class Year366 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int year; // год

        boolean isLeapYear; // является ли год високосным (true/false)

        year = input.nextInt();

        isLeapYear = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
        if (isLeapYear)
            System.out.println(year + " год високосный.");
        else
            System.out.println(year + " год не високосный.");

    }
}

