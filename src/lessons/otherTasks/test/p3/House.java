package p3;

import java.util.ArrayList;

public class House implements Cloneable, Comparable<House> {
    private int id;
    private double area;
    private java.util.Date whenBuilt;

    public House(int id, double area) {
        this.id = id;
        this.area = area;
        whenBuilt = new java.util.Date();
    }

    public int getId() {
        return id;
    }

    public double getArea() {
        return area;
    }

    public java.util.Date getWhenBuilt() {
        return whenBuilt;
    }

    /**
     * Переопределяет protected-метод clone,
     * определенный в классе Object, и расширяет его доступность
     */
//    @Override
//    public Object clone() {
//        try {
//            // Сделать поверхностную копию
//            House houseClone = (House) super.clone();
//            // Сделать глубокую копию whenBuilt
//            houseClone.whenBuilt = (java.util.Date) (whenBuilt.clone());
//            return houseClone;
//        } catch (CloneNotSupportedException ex) {
//            return null;
//        }

    // или так
    @Override
    public Object clone() {
        try {
            // Сделать поверхностную копию
            House houseClone = (House)super.clone();
            // Сделать глубокую копию whenBuilt
            houseClone.whenBuilt = (java.util.Date)(whenBuilt.clone());
            return houseClone;
        }
        catch (CloneNotSupportedException ex) {
            return null;
        }
    }


    @Override // Реализует метод compareTo, определенный в Comparable
    public int compareTo(House o) {
        if (area > o.area)
            return 1;
        else if (area < o.area)
            return -1;
        else
            return 0;
    }

    public static void main(String[] args) {
//        House house1 = new House(1, 1750.50);
//        House house2 = (House) house1.clone();


        java.util.Date date0 = new java.util.Date();
        java.util.Date date1 = date0;
        java.util.Date date2 = (java.util.Date)(date0.clone());
        System.out.println(date0 == date1);
        System.out.println(date0 == date2);
        System.out.println(date0.equals(date2));



    }
}