package p3;

import java.util.ArrayList;

public class Array implements Cloneable{

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        list.add("Новосибирск");
        ArrayList<String> list1 = list;
        ArrayList<String> list2 = (ArrayList<String>) (list.clone());
        list.add("Астрахань");
        System.out.println(list == list1);
        System.out.println(list == list2);
        System.out.println("list равно " + list);
        System.out.println("list1 равно " + list1);
        System.out.println("list2.get(0) равно " + list2.get(0));
        System.out.println("list2.size() равно " + list2.size());

        House house1 = new House(1, 1750.50);
        House house2 = (House)house1.clone();
        System.out.println(house1.equals(house2));






        ArrayList<ArrayList<Integer>> matrix = new ArrayList();

        ArrayList<Integer> temp1 = new ArrayList();
        temp1.add(1);
        temp1.add(2);
        temp1.add(3);

        ArrayList<Integer> temp2 = new ArrayList();
        temp2.add(4);
        temp2.add(5);
        temp2.add(6);
        temp2.add(7);

        ArrayList<Integer> temp3 = new ArrayList();
        temp3.add(8);
        temp3.add(9);

        matrix.add(temp1);
        matrix.add(temp2);
        matrix.add(temp3);

        System.out.println(matrix);
    }
}