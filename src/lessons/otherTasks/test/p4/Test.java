package p4;

public class Test {
    public static void main(String[] args) {
        new Circle9();
    }
}

abstract class GeometricObject {
    protected GeometricObject() {
        System.out.print("A");
    }

    protected GeometricObject(String color, boolean filled) {
        System.out.print("B");
    }
}

class Circle9 extends GeometricObject {
    /** Безаргументный конструктор */
    public Circle9() {
        this(1.0);
        System.out.print("C");
    }

    /** Создает круг с указанным радиусом */
    public Circle9(double radius) {
        this(radius, "white", false);
        System.out.print("D");
    }

    /** Создает круг с указанным радиусом, заливкой и цветом */
    public Circle9(double radius, String color, boolean filled) {
        super(color, filled);
        System.out.print("E");
    }
}