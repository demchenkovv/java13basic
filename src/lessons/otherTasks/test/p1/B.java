package p1;

public class B implements IInterface {

    int i;
    int j;

    public B(){
    }

    public B(int i, int j) {
        this.i = i;
        this.j = j;
        System.out.println("Родительский конструктор B(int i, int y): i = " + i + ", j = " + j);
    }

    public static String getNoo(){
        return "Это статический метод, принадлежащий классу В";
    }
    @Override
    public void m2(){
        System.out.println("Переопределили метод m2 в классе В, объявленный в интерфейсе IInterface");
    }
}
