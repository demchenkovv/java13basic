package p1;

public interface IInterface {
    default void m1() {
        System.out.println("Привет из интерфейса IInterface");
    }

    abstract void m2(); // Модификатор 'abstract' является избыточным для методов интерфейса

    static String get() {
        return "Hello from IInterface";
    }
}
