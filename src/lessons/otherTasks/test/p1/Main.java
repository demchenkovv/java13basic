package p1;

import java.util.*;

public class Main {

    public static void main(String[] args) {


        A a = new A(5, 2);

        Short number = (short) 5;
        System.out.println(number.shortValue());

        Date date = new Date();
        System.out.println(date);
        date.setTime(16697765465409L);
        System.out.println(date);

        B b = new B();
        C c = new C();
        System.out.println(B.getNoo());
        b.m2();
        System.out.println(IInterface.get());
        System.out.println("b instanceof IInterface: " + (b instanceof IInterface));
        System.out.println("c instanceof IInterface: " + (c instanceof IInterface));
        System.out.println("\n");

        ArrayList<Integer> numbers = new ArrayList<>();

        // Добавляем в массив 20 случайных чисел до 50
        for (int i = 0; i < 20; i++) {
            numbers.add(new Random().nextInt(50));
            System.out.print(numbers.get(i) + " ");
        }
        System.out.println("\nРазмер ДО: " + numbers.size());

        // Метод удаления, если число больше 10
        for (Iterator<Integer> iterator = numbers.iterator(); iterator.hasNext(); ) {
            if (iterator.next() > 10) {
                iterator.remove();
            }
        }
        // Итератор проходит по каждому значению, если оно есть
        for (Iterator<Integer> iterator = numbers.iterator(); iterator.hasNext(); ) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println("\nРазмер ПОСЛЕ: " + numbers.size());

    }
}