package lessons.otherTasks;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Данный тест показывает насколько связанному списку LinkedList
 * требуется больше времени для выполнения такой же операции ArrayList
 */
public class ListTestTime {
    public static void main(String[] args) {
        List<Integer> myArray = new ArrayList();
        LinkedList<Integer> myLL = new LinkedList();

        for (int i = 0; i < 1000000; i++) {
            myArray.add(i, i);
            myLL.add(i, i);
        }

        // Тест ArrayList
        final long startA = System.nanoTime();
        myArray.remove(990000);

        final long endA = System.nanoTime();
        final long totalA = endA - startA;

        System.out.println("arraylist");
        System.out.println(totalA);


        // Тест LinkedList
        final long startLL = System.nanoTime();
        myLL.remove(990000);

        final long endLL = System.nanoTime();
        final long totalLL = endLL - startLL;
        System.out.println("linked list");
        System.out.println(totalLL);
    }
}
