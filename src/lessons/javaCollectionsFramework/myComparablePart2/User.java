package lessons.javaCollectionsFramework.myComparablePart2;

/**
 * Для возможности сравнения полей User и последующей сортировки
 * необходимо имплементировать интерфейс Comparable с типом <User> и
 * переопределить (@Override) метод int compareTo(User o)
 *
 * Сравнение текущего объекта с объектом, который передается в качестве параметра.
 * Можем реализовать только один метод для сравнения.
 */
public class User implements Comparable<User> {
    private String name;
    private int age;
    private String city;

    public User(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public int compareTo(User other) {
//        if (this.age == other.age) {
//            return 0;
//        }
//        if (this.age < other.age) {
//            return -1;
//        } else {
//            return 1;
//        }

        // если необходимо сравнить строки (например, city), то
        // можно воспользоваться встроенным методом compareTo()
        // а лучше compareToIgnoreCase(), чтобы искл. регистр
        return this.city.compareToIgnoreCase(other.city);
    }
}
