package lessons.javaCollectionsFramework.myComparablePart2;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

        List<User> list = new ArrayList<>();
        list.add(new User("Anton", 25, "Moscow"));
        list.add(new User("Denis", 31, "Omsk"));
        list.add(new User("Petr", 28, "Moscow"));

        System.out.println("list before sort: " + list);

        list.sort(User::compareTo);

        System.out.println("list after sort: " + list);
    }
}
