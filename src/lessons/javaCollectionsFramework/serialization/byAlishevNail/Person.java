package lessons.javaCollectionsFramework.serialization.byAlishevNail;

import java.io.Serial;
import java.io.Serializable;

// Сериализация - запись в файл (процесс сохранения состояния объекта в определенный формат представления)
// Десериализация - чтение из файла (процесс извлечения состояния объекта из определенного формата представления.
// Операция обратная операции сериализации.

// Если не хотим сериализовывать поле, то необходимо добавить
// ключевое слово transient, например, private transient String name;.
// У объекта с ключевым словом transient вернет null (для ссылочной)
// и 0 для примитивного типа.

// serialVersionUID = нужен для того, чтобы получать состояние класса.
// При изменении класса его номер изменится.
// При прочтении ReadObject объект не сможет прочитаться, если класс был изменен
// и serialVersionUID остался старым.

public class Person implements Serializable {
    @Serial
    private static final long serialVersionUID = 2749236514391118022L;

    private int id;
    private String name;
    private String name123;


    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + " : " + name;
    }
}
