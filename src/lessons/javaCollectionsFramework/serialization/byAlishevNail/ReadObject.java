package lessons.javaCollectionsFramework.serialization.byAlishevNail;

import java.io.*;

public class ReadObject {
    /**
     * Процесс ДЕсериализации (Файл (input) ---->>>> Программа)
     */
    public static void main(String[] args) {

        final String FILE_DIRECTORY = "C:\\Users\\HOME\\IdeaProjects\\Java13Basic\\src\\lessons\\profModule\\week1\\JavaCollectionsFramework\\objectSerializationMechanizm\\byAlishevNail\\";
        final String FILE_NAME = "people.bin";

        File file = new File(FILE_DIRECTORY + FILE_NAME);

        // Читаем (извлекаем) объекты из файла.
        // Используем try-with-resources (поток будет закрыт автоматически).
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){

            Person person1 = (Person) ois.readObject();
            System.out.println(person1);

//            // Для чтения единичных объектов из файла
//            Person person1 = (Person) ois.readObject();
//            Person person2 = (Person) ois.readObject();
//            System.out.println(person1);
//            System.out.println(person2);

//            // Работа с массивом
//            // Вариант 1
//            // Число объектов, которое мы ожидаем получить из файла
//            int personCount = ois.readInt();
//            Person[] people = new Person[personCount];
//            for (int i = 0; i < personCount; i++) {
//                people[i] = (Person) ois.readObject();
//            }

            // Работа с массивом
            // Вариант 2 - более удобный ! Достаточно просто записать массив
//            Person[] people = (Person[]) ois.readObject();
//            System.out.println("readObject: " + Arrays.toString(people));

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
