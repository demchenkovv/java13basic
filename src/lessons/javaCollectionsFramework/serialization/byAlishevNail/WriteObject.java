package lessons.javaCollectionsFramework.serialization.byAlishevNail;

import java.io.*;

// Сериализация - запись в файл
// Десериализация - чтение из файла
public class WriteObject {
    /**
     * Процесс сериализации (Программа (output) ---->>>> Файл)
     */
    public static void main(String[] args) {

        final String FILE_DIRECTORY = "C:\\Users\\HOME\\IdeaProjects\\Java13Basic\\src\\lessons\\profModule\\week1\\JavaCollectionsFramework\\objectSerializationMechanizm\\byAlishevNail\\";
        final String FILE_NAME = "people.bin";

        File file = new File(FILE_DIRECTORY + FILE_NAME);

        Person person1 = new Person(1, "Mike");
//        Person person2 = new Person(2, "Bob");

        // Вариант 2: Массив объектов
//        Person[] people = {
//                new Person(1, "Bob"),
//                new Person(2, "Mike"),
//                new Person(3, "Tom")
//        };

//        // Записываем объекты в файл (без try-with-resources, в конце поток ЗАКРЫТЬ!)
//        try {
//            FileOutputStream fos = new FileOutputStream(file);
//            ObjectOutputStream oos = new ObjectOutputStream(fos);

        // Записываем объекты в файл с try-with-resources (поток будет закрыт автоматически)
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {

//            // Используется для записи единичных файлов (метод writeInt и forEach использовать не надо)
            oos.writeObject(person1);
//            oos.writeObject(person2);

//            // Работа с массивом объектов
//            // Вариант 1
//            // Прежде, чем записать в файл массив объектов, необходимо
//            // записать в файл целое число - длину массива
//            oos.writeInt(people.length);

//            // Далее необходимо проитерироваться по массиву и записать в файл объекты
//            for (Person person : people) {
//                oos.writeObject(person);
//            }

            // Вариант 2
            // Запись объекта-массива объектов People[] в файл
//            oos.writeObject(people);
//            System.out.println("writeObject: " + Arrays.toString(people));

            // Если не используем try-with-resources, то:
            // ОБЯЗАТЕЛЬНО! Закрыть поток (стрим), иначе файлы не будут записаны!
            // либо СРАЗУ использовать try-with-resources
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
