package lessons.javaCollectionsFramework.myComparatorPart2;

/**
 * Иногда требуется сравнение исходя из текущей обстановки,
 * т.е. сперва по одному параметру (полю), потом по другому (т.е. несколькими способами).
 * Функциональный интерфейс, который содержит один метод (типизирован типом Т).
 * Благодаря ему можем сравнивать два объекта; метод compare, который принимает два объекта lhs и rhs.
 * Возвращает -1, 0, 1
 * Мы можем группировать элементы по категориям и сравнивать их по разным полям, т.е.
 * мы можем задать разные компараторы для объекта одного и того же класса, в отличие от интерфейса Comparable.
 * Здесь также типизированный интерфейс.
 * Можно создавать ссылку на компаратор с помощью лямбд с последующей реализацией.
 */
public class User {
    private String name;
    private int age;
    private String city;

    public User(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }
}
