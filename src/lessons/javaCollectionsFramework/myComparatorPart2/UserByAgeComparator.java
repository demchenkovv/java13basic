package lessons.javaCollectionsFramework.myComparatorPart2;

import java.util.Comparator;
/** Сортировка по возрасту (int) */
public class UserByAgeComparator implements Comparator<User> {
    @Override
    public int compare(User lhs, User rhs) {
        if (lhs.getAge() == rhs.getAge()) {
            return 0;
        }
        if (lhs.getAge() < rhs.getAge()) {
            return -1;
        } else {
            return 1;
        }
    }
}
