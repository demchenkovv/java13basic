package lessons.javaCollectionsFramework.myComparatorPart2;

import java.util.Comparator;
/** Сортировка по городу (String) */
public class UserByCityComparator implements Comparator<User> {
    @Override
    public int compare(User lhs, User rhs) {
        return lhs.getCity().compareToIgnoreCase(rhs.getCity());
    }
}
