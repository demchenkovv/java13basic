package lessons.javaCollectionsFramework.collectionsPart1;

import java.util.ArrayDeque;
import java.util.Queue;

public class MyQueue {
    public static void main(String[] args) {

        /** Queue - очередь. Все события происходят друг за другом, в хронологическом порядке.
         * Обращаться по индексу НЕЛЬЗЯ! Все элементы добавляются в начало очереди.
         * Если не надо обращаться к какому-то произвольному индексу,
         * то интерфейс очередь очень подходит
         *
         * Таким образом, можем добавлять в конец и извлекать из начало.
         * Добавлять в начало и извлекать из конца мы не можем */
        Queue<Integer> queue = new ArrayDeque<>();

        queue.offer(10);
        queue.offer(20);
        queue.offer(30);

        /** Пока коллекция не пустая - true
         * Выводит элемент и удаляет его - remove() или poll()
         * Метод poll() позволяет безопасно удалять элементы */
        while (!queue.isEmpty()) {
            System.out.println(queue.poll());
        }

        System.out.println("size: " + queue.size());

        Integer result = queue.poll();
        System.out.println("result: " + result);

        /** Метод element() кидает исключение, если очередь пустая
         * Метод peek() возвращает null, если очередь пустая (без исключений) */
        System.out.println(queue.peek());


//        Integer value;
//        while ((value = queue.peek()) != null) {
//            System.out.println(value);
//            queue.remove();
//        }


//        Integer value;
//        while ((value = queue.poll()) != null) {
//            System.out.println(value); // удаляем элемент из очереди
//        }


// // Демонстрация методов
//        // 20 10 (head)
//        System.out.println("headElem: " + queue.element());
//        System.out.println("headElem: " + queue.peek());
//
//        // 30 20 10 (head)
//        queue.offer(30);
//        System.out.println("headElem: " + queue.element());
//        System.out.println("headElem: " + queue.peek());
//
//        Integer value = queue.remove();
//        //Integer value = queue.poll(); // эквивалентно remove()
//        System.out.println("value: " + value);
//
//        // (tail) 30 20 (head)
//
//        System.out.println("headElem after remove: " + queue.element());
//        System.out.println("headElem after remove: " + queue.peek());

    }
}
