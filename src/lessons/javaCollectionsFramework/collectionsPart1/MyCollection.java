package lessons.javaCollectionsFramework.collectionsPart1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Predicate;

/**
 * Демонстрация базового интерфейса Collection
 * List отличает от Collection тем, что вводится понятие индексы элемента
 * Т.е. можем проходиться по каждому элементу индекса
 */
public class MyCollection {

    public static void main(String[] args) {

        Collection<Integer> list = new ArrayList<>();

        list.add(10);
        list.add(20);
        list.add(30);

        // new LinkedList
        Collection<Integer> anotherList = new LinkedList<>();
        anotherList.add(40);
        anotherList.add(50);

        // Добавление коллекции anotherList в list
        list.addAll(anotherList);

        System.out.println("Before: " + list);

        // Принимает какое-то число и возвращает boolean
        Predicate<Integer> testFunc = (Integer digit) -> {
            if (digit < 30) {
                return true;
            } else {
                return false;
            }
        };

//        System.out.println("testFunc.test(20): " + testFunc.test(20));
//        System.out.println("testFunc.test(25): " + testFunc.test(25));
//        System.out.println("testFunc.test(30): " + testFunc.test(30));

        // Выполняет удаление с условием Predicate
        list.removeIf(testFunc);

        System.out.println("After: " + list);

    }
}
