package lessons.javaCollectionsFramework.collectionsPart1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MyList {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(0);
        list.add(5);
        list.add(4);

        System.out.println("list: " + list);

        // Comparator необходим для сравнения двух чисел (compare)
        // lhs - левое значение, rhs - правое значение
        Comparator<Integer> cmp = (Integer lhs, Integer rhs) -> {
            if (lhs.equals(rhs)) {
                return 0;
            }
            if (lhs < rhs) {
                return -1;
            } else {
                return 1;
            }
        };

        list.sort(cmp);
        System.out.println("list after sort: " + list);

//        List<Integer> list = new ArrayList<>();
//
//        for (int i = 1; i <= 5; i++) {
//            list.add(i);
//        }
//        System.out.println("list: " + list);
//
//        // Увеличивает значения UnaryOperator (Унарный предикатор)
//        UnaryOperator<Integer> op = (Integer digit) -> {
//            return -1 * digit;
//        };


//        list.replaceAll(op);
//        System.out.println("list: " + list);


//        System.out.println("result: " + op.apply(10));
//        System.out.println("result: " + op.apply(20));
//        System.out.println("result: " + op.apply(-1));


//        Integer value = list.remove(2);
//
//        System.out.println("value: " + value);
//        System.out.println("list: " + list);


//        for (int i = list.size() - 1; i >= 0; i--) {
//            System.out.println("index=" + i + " item=" + list.get(i));
//        }


//        list.add(5, 32);
//        System.out.println("list: " + list);


//        List<Integer> anotherCollection = new LinkedList<>();
//        anotherCollection.add(40);
//        anotherCollection.add(50);
//
//        System.out.println("anotherCollection: " + anotherCollection);
//
//        list.addAll(2, anotherCollection);
//        System.out.println("list: " + list);


//        list.add(5, 32);
//        System.out.println("list: " + list);
    }
}
