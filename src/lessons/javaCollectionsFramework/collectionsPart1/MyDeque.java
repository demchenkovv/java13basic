package lessons.javaCollectionsFramework.collectionsPart1;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * interface Iterable<T>
 * interface Collection<E>
 * interface Queue<E>
 * interface Deque<E>
 * <p>
 * E — element, для элементов параметризованных коллекций;
 * K — key, для ключей map-структур;
 * V — value, для значений map-структур;
 * N — number, для чисел;
 * T — type, для обозначения типа параметра в произвольных классах;
 * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
 * <p>
 * E — element, для элементов параметризованных коллекций;
 * K — key, для ключей map-структур;
 * V — value, для значений map-структур;
 * N — number, для чисел;
 * T — type, для обозначения типа параметра в произвольных классах;
 * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
 * <p>
 * E — element, для элементов параметризованных коллекций;
 * K — key, для ключей map-структур;
 * V — value, для значений map-структур;
 * N — number, для чисел;
 * T — type, для обозначения типа параметра в произвольных классах;
 * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
 * <p>
 * E — element, для элементов параметризованных коллекций;
 * K — key, для ключей map-структур;
 * V — value, для значений map-структур;
 * N — number, для чисел;
 * T — type, для обозначения типа параметра в произвольных классах;
 * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
 */

/**
 * E — element, для элементов параметризованных коллекций;
 * K — key, для ключей map-структур;
 * V — value, для значений map-структур;
 * N — number, для чисел;
 * T — type, для обозначения типа параметра в произвольных классах;
 * S, U, V и так далее — применяются, когда в дженерик-классе несколько параметров.
 *  */

/** Deque - это двунаправленная очередь, которая расширяет очередь Queue.
 * Таким образом, в Deque мы можем:
 * Добавлять и удалять элементы возможно с двух сторон очереди
 * Но мы так же не имеем доступа к произвольным элементам
 * (т.е. по индексу обратиться НЕЛЬЗЯ)*/
public class MyDeque {
    public static void main(String[] args) {

        Deque<Integer> deque = new ArrayDeque<>();

        deque.addFirst(10);
        deque.addFirst(20);
        deque.addFirst(25);
        deque.addFirst(30);

        // (tail) 10 20 25 30 (head) addFirst - в хвост

//        System.out.println("head: " + deque.getFirst());
//        System.out.println("head: " + deque.peekFirst());
//
//        System.out.println("tail: " + deque.getLast());
//        System.out.println("tail: " + deque.peekLast());
//
//        // Проходимся по очереди
//        while (!deque.isEmpty()) {
//            Integer value = deque.getLast();
//            System.out.println(value);
//            deque.removeLast();
//        }


//        // Можно пройтись так, но это неправильно, т.к. null может содержаться в LinkedList
//        Integer value;
//        while ((value = deque.peekFirst()) != null) {
//            System.out.println(value);
//            deque.removeFirst();
//        }


//        // Демонстрация 1
//        deque.addLast(10);
//        deque.addLast(20);
//        deque.addLast(25);
//        deque.addLast(30);
//
//        // (tail) 30 25 20 10 (head)
//
//        System.out.println("head: " + deque.getFirst());
//        System.out.println("head: " + deque.peekFirst());
//
//        System.out.println("tail: " + deque.getLast());
//        System.out.println("tail: " + deque.peekLast());
//
//        System.out.println("remove first: " + deque.removeFirst());
//        System.out.println("remove last: " + deque.removeLast());
//
//        // (tail) 25 20 (head)
//
//        System.out.println("head: " + deque.getFirst());
//        System.out.println("head: " + deque.peekFirst());
//
//        System.out.println("tail: " + deque.getLast());
//        System.out.println("tail: " + deque.peekLast());
    }
}
