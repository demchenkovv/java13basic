Интерфейс Collection<E> имеет три наследника:
interface List<E> - список;
interface Queue<E> - очередь;
interface Set<E> - множество.


В свою очередь интерфейс Collection<E> наследуется от
interface Iterable<E> {
Iterator <E> iterator();
}


public interface Iterator<E> {
boolean hasNext(); - есть ли еще элементы в коллекции?
E next(); - возвращает значение и итератор автоматически сдвигается правее
void remove();
}


У интерфейса Iterator<E> есть наследник ListIterator<E>,
который обеспечивает двунаправленный обход коллекции, т.е.
возможно как с начала, так и с конца делать обход (итерироваться).

boolean hasPrevious();
E previous();

int nextIndex();
int previousIndex();
