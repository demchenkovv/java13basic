package lessons.javaCollectionsFramework.mapPart3;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * interface SortedMap<K,V> extends interface Map<K,V> определяет поведение отображения,
 * в котором ключи отсортированы в некотором порядке. K - ключ, V - значение.
 * Здесь порядок не относительно значений, а относительно ключей
 */
public class task3 {
    public static void main(String[] args) {

        SortedMap<Integer, String> map = new TreeMap<>();

        for (int i = 0; i <= 5; i++) {
            map.put(i, "value_" + i);
        }
        System.out.println("map: " + map);

        System.out.println("firstKey: " + map.firstKey());
        System.out.println("lastKey: " + map.lastKey());

        SortedMap<Integer, String> subMap = map.subMap(1, 3);
        System.out.println("subMap: " + subMap);

        SortedMap<Integer, String> headMap = map.headMap( 3);
        System.out.println("headMap: " + headMap);

        SortedMap<Integer, String> tailMap = map.tailMap( 3);
        System.out.println("tailMap: " + tailMap);
    }
}
