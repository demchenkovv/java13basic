package lessons.javaCollectionsFramework.mapPart3;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Interface NavigableMap<K,V> - определяет поведение отображения,
 * извлечение элементов из которого осуществляется на основании наиболее точного
 * совпадения с заданным ключом или несколькими ключами.
 * Наследуется от interface SortedMap<K,V>, interface Map<K,V>.
 * <p>
 * Например, найди ключ, который больше заданного. Т.е. можешь осуществлять об условиях,
 * равенствах, неравенствах.
 *
 * NavigableMap предоставляет более расширенный функционал, по сравнению с SortedMap, используя
 * различные условия, т.е. мы можем искать какие-то значения по определенным критериям.
 */
public class task4 {
    public static void main(String[] args) {
        NavigableMap<Integer, String> map = new TreeMap<>();

        for (int i = 0; i <= 5; ++i) {
            map.put(i, "Value_" + i);
        }
        System.out.println("map: " + map);


        // Метод lowerKey() используется для возврата наибольшего ключа,
        // который строго меньше заданного ключа, переданного в качестве параметра.
        // Проще говоря, этот метод используется для поиска следующего по величине
        // элемента после элемента, переданного в качестве параметра.

        // Метод lowerEntry() java.util.Класс TreeMap используется для возврата сопоставления ключ-значение,
        // связанного с наибольшим ключом, который строго меньше заданного ключа, или null, если такого ключа нет.
        // Параметры: Этот метод принимает ключ в качестве параметра, для которого необходимо найти нижнюю запись.
        // Возвращаемое значение: этот метод возвращает запись с наибольшим ключом, меньшим, чем key, или null, если такого ключа нет.

        // (строго меньше)
        Integer lowerKey = map.lowerKey(3); // < lowerKey() - ключ
        Map.Entry<Integer, String> lowerEntry = map.lowerEntry(3); // < lowerEntry() - значение

        // (меньше или равно)
        Integer floorKey = map.floorKey(3); // <= floorKey() - ключ
        Map.Entry<Integer, String> floorEntry = map.floorEntry(3); // <= floorEntry() - значение

        System.out.println("lowerKey: " + lowerKey);
        System.out.println("lowerEntry: " + lowerEntry);
        System.out.println("floorKey: " + floorKey);
        System.out.println("floorEntry: " + floorEntry);

        System.out.println("\n---------------\n");

        System.out.println("map: " + map);
        // Метод higherKey() java.util.Класс TreeMap используется для возврата наименьшего ключа, строго большего,
        // чем заданный ключ, или null, если такого ключа нет.
        // Параметры: Этот метод принимает ключ k в качестве параметра.
        // Возвращаемое значение: этот метод возвращает наименьший ключ, больший, чем key, или null, если такого ключа нет.

        // (строго больше)
        Integer higherKey = map.higherKey(3); // < lowerKey() - ключ
        Map.Entry<Integer, String> higherEntry = map.higherEntry(3); // < lowerEntry() - значение

        // (больше или равно)
        Integer ceilingKey = map.ceilingKey(3); // <= floorKey() - ключ
        Map.Entry<Integer, String> ceilingEntry = map.ceilingEntry(3); // <= floorEntry() - значение

        System.out.println("higherKey: " + higherKey);
        System.out.println("higherEntry: " + higherEntry);
        System.out.println("ceilingKey: " + ceilingKey);
        System.out.println("ceilingEntry: " + ceilingEntry);

        System.out.println("\n---------------\n");
        System.out.println("map: " + map);

        // Метод subMap - похоже на методы head и tail, но добавляют булевые значения, например:
        // subMap(2, false, 4, false); - значит (от 2, не включая 2, до 4, не включая 4);
        // subMap(2, true, 4, true); - значит (от 2, включая 2, до 4, включая 4);
        // map.headMap(4, true); - значит (до 4, включая 4);
        // map.headMap(4, false); - значит (до 4, НЕ включая 4);
        // map.tailMap(4, false); - значит (от 4, НЕ включая 4);
        // map.tailMap(4, true); - значит (от 4, включая 4);
        NavigableMap<Integer, String> tailMap = map.tailMap(4, true);
        System.out.println("tailMap: " + tailMap);

        System.out.println("\n---------------\n");
        System.out.println("map: " + map);

        // Метод descendingMap() - сортирует ключи по убыванию, т.е. в обратном порядке
        NavigableMap<Integer, String> descendingMap = map.descendingMap();
        System.out.println("descendingMap: " + descendingMap);
    }
}
