package lessons.javaCollectionsFramework.mapPart3;

import java.util.*;

public class task1 {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        for (int i = -2; i <= 5; i++) {
            map.put(i, "Value_" + i);
        }
        System.out.println("HashMap: " + map);

//        String result = map.put(4, "NEW VALUE-4");
//        System.out.println("result: " + result);
//        System.out.println("map: " + map);
//
//
//        boolean result1 = map.remove(1, "Value_1");
//        System.out.println("result1: " + result1);
//
//
//        System.out.println("containsValue(Value_2): " + map.containsValue("Value_2")); // проверка по значению
//        System.out.println("containsKey(-5): " + map.containsKey(-5)); // проверка по ключу
//
//
//        String result2 = map.replace(3, "replace Value");
//        System.out.println("result2: " + result2);
//        System.out.println("map: " + map);
//
//
//        boolean result3 = map.replace(3, "replace Value", "new replace Value");
//        System.out.println("result3: " + result2);
//        System.out.println("map: " + map);
//        System.out.println("");
//
//
//        Set<Integer> keys = map.keySet();
//        Collection<String> values = map.values();
//        for (Integer key : keys) {
//            System.out.println("key: " + key + ", value: " + map.get(key));
//        }
//        System.out.println("");
//
//        for (String value : values) {
//            System.out.println("value: " + value);
//        }
//        System.out.println("");
//
//
//        //
//        Set<Map.Entry<Integer, String>> entries = map.entrySet();
//        for (Map.Entry<Integer, String> entry : entries) {
//            Integer key = entry.getKey();
//            String value = entry.getValue();
//            System.out.println("{" + key + ";" + value + "}");
//        }
//
//
//        // Изменить значения с помощью метода .setValue()
//        Set<Map.Entry<Integer, String>> entries1 = map.entrySet();
//        for (Map.Entry<Integer, String> entry : entries1) {
//            Integer key = entry.getKey();
//            String value = entry.getValue();
//
//            entry.setValue("NEW" + value);
//        }
//
//        System.out.println("map: " + map);
//        System.out.println();


        // Поменяем реализацию с new HashMap<>() на new LinkedHashMap<>();
        // Теперь ключи идут по порядку добавления значений (аналогично add())
        // Link - связанный
        // было  HashMap       {-1=Value_-1, 0=Value_0, -2=Value_-2, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        // стало LinkedHashMap {-2=Value_-2, -1=Value_-1, 0=Value_0, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        Map<Integer, String> mapLinked = new LinkedHashMap<>();
        for (int i = -2; i <= 5; i++) {
            mapLinked.put(i, "Value_" + i);
        }
        System.out.println("LinkedHashMap: " + mapLinked);


        // TreeMap реализован на основе двоичного дерева
        // Теперь ключи идут в отсортированном возрастающем порядке (от меньшего к большему)
        // было  HashMap       {-1=Value_-1, 0=Value_0, -2=Value_-2, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        // было LinkedHashMap {-2=Value_-2, -1=Value_-1, 0=Value_0, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        // стало TreeMap: {-2=Value_-2, -1=Value_-1, 0=Value_0, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        Map<Integer, String> mapTreeMap = new TreeMap<>();
        for (int i = -2; i <= 5; i++) {
            mapTreeMap.put(i, "Value_" + i);
        }
        System.out.println("TreeMap: " + mapTreeMap);

        // Вывод: HashMap - порядка по ключам нет,
        // LinkedHashMap - элементы упорядочены в порядке добавления,
        // TreeMap - порядок, который задает Comparator от наменьшего к наибольшему


        // Метод добавления одной коллекции в другую
        Map<Integer, String> map111 = new TreeMap<>();
        map111.put(1, "v1");
        map111.put(2, "v2");
        System.out.println("map111: " + map111);

        Map<Integer, String> map222 = new HashMap<>();
        map222.put(5, "v5");
        map222.put(0, "v0");
        System.out.println("map222: " + map222);

        map111.putAll(map222);
        // Коллекции объединены и отсортированы по политике TreeMap
        System.out.println("map111 after putAll: " + map111);


    }
}
