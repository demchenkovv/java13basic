package lessons.javaCollectionsFramework.mapPart3;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/** BiFunction, Function, map.computeIfPresent(), map.computeIfAbsent(), map.merge()  */
public class task2 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        map.put("key1", 123);

        // computeFunction работает в купе с BiFunction. Если заданный ключ
        // в computeIfPresent есть, то метод отработает и в map будет добавлен
        // заданный ключ со значением oldValue + 5;
        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> {
            System.out.println("Inside compute function key=" + key + " oldValue=" + oldValue);
            return oldValue + 5;
        };

        // Метод computeIfPresent - Работает только в случае, если данный ключ ЕСТЬ в Map
        // В данном случае необходимо работать с BiFunction, а не Function
        Integer result = map.computeIfPresent("key1", computeFunction);
        System.out.println("result: " + result);
        System.out.println("map: " + map);





        // ---------------
        // computeIfAbsent работает в купе с Function. Если заданного ключа
        // в computeIfAbsent НЕТ, то метод отработает и в map будет добавлен
        // заданный ключ со значением 754
//        Function<String, Integer> computeFunction = (String key) -> {
//            System.out.println("Inside compute function key=" + key);
//            return 754;
//        };

        // Метод computeIfAbsent - Работает только в случае, если данного ключа НЕТ в Map
        // В данном случае необходимо работать с Function, а не BiFunction
//        Integer result = map.computeIfAbsent("key5", computeFunction);
//        System.out.println("result: " + result);
//        System.out.println("map: " + map);



// ------------
//        // BiFunction - с помощью лямбды. Принимаем ключ (String), принимает
//        // старое значение (Integer) и возвращает новое значение (Integer).
//        // Например, при ключе key5, oldValue == null и на выходе в map добавляется key5.
//        // В computeFunction мы заходим всегда.
//        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> {
//            System.out.println("Inside compute function key=" + key + " oldValue=" + oldValue);
//            return oldValue * 2;
//        };
//        // Метод compute - высчитывает значения на лету.
//        Integer result = map.compute("key1", computeFunction);
//        System.out.println("result: " + result);
//        System.out.println("map: " + map);

// //----------------
//        // Функциональный интерфейс interface BiFunction аналогичен Function.
//        // Он тоже имеет один метод apply, но функция принимает уже два аргумента
//        // и возвращает некоторое значение
//        // interface BiFunction содержит метод R apply (T t, U u), который принимает
//        // два параметра с РАЗНЫМИ типами T и U и возвращает некоторое третье значение.
//        BiFunction<Integer, Integer, Integer> mergeFunction = (Integer oldValue, Integer newValue) -> {
//            System.out.println("Inside mergeFunction: oldValue = " + oldValue + ", newValue = " + newValue);
//            return oldValue + newValue; // тут можем вернуть любое значение (1, 0, сумму и так далее)
//        };
//
//        // Задаем ключ key5, значение 500 и merge function, т.е. функцию, которая будет происходить,
//        // если произойдет коллизия, т.е. если есть значение и нужно принять выбор, какое
//        // значение принять, если произошла коллизия, но при данном примере коллизия не произойдет,
//        // т.к. элемент уже есть там.
//        // Если поменять ключ на key1, который есть, то произойдет коллизия мы можем мержить (merge),
//        // если ключи одинаковые
//
//        Integer result = map.merge("key1", 500, mergeFunction);
//        System.out.println("result: " + result);
//        System.out.println("map: " + map);

// // ----------------
//         Если элемента нет, он возвращает дефолтное значение, т.е.
//         мы просим найти ключ "key1" и если он не найден, пусть вернется -1
//         null уже не возвращается и это хорошо
//        Integer value = map.getOrDefault("key1", -1);
//        System.out.println("value: " + value);
    }
}
