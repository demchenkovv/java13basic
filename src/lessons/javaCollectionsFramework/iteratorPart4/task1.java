package lessons.javaCollectionsFramework.iteratorPart4;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class task1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("v1");
        list.add("v2");
        list.add("v3");
        list.add("v4");
        list.add("v5");

        ListIterator<String> it = list.listIterator();
        while (it.hasNext()) {
            String value = it.next();
            System.out.println("value: " + value);
        }

        System.out.println("and reversed order");

        while (it.hasPrevious()) {
            String value = it.previous();
            System.out.println("previous value: " + value);
        }


//        Iterator<String> it = list.iterator();
//        while (it.hasNext()) {
//            String value = it.next();
//            if ("v3".equals(value)) {
//                it.remove();
//                System.out.println("remove value: " + value);
//            } else {
//                System.out.println("value: " + value);
//            }
//        }
//        System.out.println("after list: " + list);
    }
}
