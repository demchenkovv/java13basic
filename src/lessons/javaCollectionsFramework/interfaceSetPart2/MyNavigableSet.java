package lessons.javaCollectionsFramework.interfaceSetPart2;

import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * Интерфейс NavigableSet<E> наследуется от интерфейса SortedSet<E>, от Set<E>, от Collection<E>;
 * Определяет поведение коллекции, извлечение элементов из которой осуществляется на
 * основании наиболее точного совпадения с заданным значением или несколькими значениями
 */
public class MyNavigableSet {
    public static void main(String[] args) {
        NavigableSet<Integer> set = new TreeSet<>();
        for (int i = 10; i >= 1; i--) {
            set.add(i);
        }

        System.out.println("Result: ");
        for (Integer digit : set) {
            System.out.print(digit);
            System.out.print(" ");
        }
        System.out.println();

        // должны найти элемент строго больше, чем заданный
        Integer higherValue = set.higher(5);
        System.out.println("higherValue: " + higherValue);

        // должны найти элемент строго меньше, чем заданный
        Integer lowerValue = set.lower(5);
        System.out.println("lowerValue: " + lowerValue);

        // Тестирование методов ceiling и floor
        set.remove(5);
        System.out.println("set after remove " + set);

        // найти больше или равно
        Integer ceilingValue = set.ceiling(5);
        System.out.println("ceilingValue: " + ceilingValue); // min(elem >= 5)

        // найти меньше или равно
        Integer floorValue = set.floor(5);
        System.out.println("floorValue: " + floorValue); // max(elem <= 5)


    }
}
