package lessons.javaCollectionsFramework.interfaceSetPart2;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Интерфейс NavigableSet<E> наследуется от интерфейса SortedSet<E>, от Set<E>, от Collection<E>;
 * Определяет поведение коллекции, извлечение элементов из которой осуществляется на
 * основании наиболее точного совпадения с заданным значением или несколькими значениями
 */

public class MySortedSet {
    public static void main(String[] args) {
        SortedSet<Integer> set = new TreeSet<>();
        for (int i = 10; i >= 1; i--) {
            set.add(i);
        }

        // set.remove(5);

        System.out.println("Result: ");
        for (Integer digit : set) {
            System.out.print(digit);
            System.out.print(" ");
        }
        System.out.println();

        Integer firstValue = set.first();
        Integer lastValue = set.last();

        System.out.println("firstValue: " + firstValue);
        System.out.println("lastValue: " + lastValue);


        // Метод subSet(from e, to e) - создает подмножество от элемента (НЕ индекса!) from
        // до элемента to (не включая to)
         SortedSet<Integer> subSet = set.subSet(2, 9);
         System.out.println("subSet: " + subSet);

        // Метод headSet(to e) - создает подмножество с начала (с головы) и до to (не включая to)
        SortedSet<Integer> headSet = set.headSet(5);
        System.out.println("headSet: " + headSet);

        // Метод tailSet(from e) - создает подмножество начиная с from и до конца (хвоста - tail)
        SortedSet<Integer> tailSet = set.tailSet(5);
        System.out.println("tailSet: " + tailSet);
    }
}
