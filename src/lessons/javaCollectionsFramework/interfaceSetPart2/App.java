package lessons.javaCollectionsFramework.interfaceSetPart2;

import java.util.Set;
import java.util.TreeSet;

/**
 * Интерфейс-маркер Set<E> наследуется от interface Collection<E>;
 * Определяет поведение коллекций,
 * не допускающих дублирования элементов, т.е., например, одинаковые числа добавить НЕЛЬЗЯ.
 * Множество НЕ поддерживает дубликаты.
 * Интерфейс - маркер. Не добавляет новых методов
 * по сравнению с интерфейсом Collection<E>;
 * <p>
 * Интерфейс SortedSet<E> наследуется от interface Set<E>;
 * Определяет поведение множеств,
 * отсортированных в некотором порядке.
 * <p>
 *     Классы:
 * new HashSet<>() - множ-во не отсортировано, порядок НЕ совпадает с порядком добавления в множество (произвольный).
 *
 * new LinkedHashSet<>() - множ-во не отсортировано, порядок совпадает с порядком добавления в множество.
 * LinkedHashSet наследуется от класса HashSet.
 *
 * new TreeSet<>() - множество сразу сортируется. Если используем Set типизированый числами (<Integer> и т.д.),
 * то множество будет отсортировано в порядке возрастания. Если используем Set типизированый строками, то
 * множество будет отсортировано в лексикографическом порядке <String>;
 *
 *
 * Интерфейс NavigableSet<E> наследуется от интерфейса SortedSet<E>, от Set<E>, от Collection<E>;
 * Определяет поведение коллекции, извлечение элементов из которой осуществляется на
 * основании наиболее точного совпадения с заданным значением или несколькими значениями
 */
public class App {
    public static void main(String[] args) {
        //Set<Integer> set = new HashSet<>();
        //Set<Integer> set = new LinkedHashSet<>();
        Set<Integer> set = new TreeSet<>();
        set.add(10);
        set.add(20);
        set.add(3);
        set.add(-1);

        System.out.println("set: " + set);

        // Пытаемся добавить значение, которое уже есть в множестве
        boolean result = set.add(20);
        System.out.println("result: " + result);

        System.out.println("set: " + set);

        set.remove(10);
        System.out.println("set after remove: " + set);

        System.out.println("set.contains(10): " + set.contains(10));
        System.out.println("set.contains(11): " + set.contains(11));
        System.out.println("set.contains(11): " + set.contains(3));


    }
}
