package lessons.javaCollectionsFramework.equalsAndHashCodePart2;

import java.util.Objects;

/**
 * Переопределяем метод equals() и метод hashCode()
 * согласно свойствам метода equals
 * Совместная концепция equals() и hashCode() нужна для
 * коллекции, которая использует хэширование
 */

public class User {

    private String login;
    private String name;
    private String city;

    public User(String login, String name, String city) {
        this.login = login;
        this.name = name;
        this.city = city;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!this.getClass().equals(obj.getClass())) {
            return false;
        }
        User other = (User) obj;
        return login.equals(other.login) && name.equals(other.name) && city.equals(other.city);
    }

    // Метод hashCode, благодаря методу hash() класса Objects вернет оптимальный hashCode
    // в параметры передаём объекты (поля)
    @Override
    public int hashCode() {
        return Objects.hash(login, name, city);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
