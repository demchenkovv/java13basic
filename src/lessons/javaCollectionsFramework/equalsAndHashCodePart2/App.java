package lessons.javaCollectionsFramework.equalsAndHashCodePart2;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        User user1 = new User("anton777", "Denis", "Omsk");
        User user2 = new User("anton777", "Anton", "Omsk");

//        boolean equalsValue = user1.equals(user2);
//        System.out.println("equalsValue: " + equalsValue);
//        System.out.println("user1.hashCode: " + user1.hashCode());
//        System.out.println("user2.hashCode: " + user2.hashCode());

        List<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        System.out.println(list.contains(new User("anton777", "Anton", "Omsk")));

    }
}
