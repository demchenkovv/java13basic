/*
 Создадим таблицу Reviews. У одной книги может быть несколько оценок.

 1 - 1
 1 - Many
 Many - Many

-- книги           авторы                   авторы_книг
-- id              id                  id    id_книги id_автора
-- 1               3                   1      1        3
-- 2               4                   2      2        3
-- 3               6                   3      1        6
 */
CREATE TABLE IF NOT EXISTS reviews
(
    id       bigserial primary key,
    book_id  integer REFERENCES books (id),
    reviewer varchar(100) NOT NULL,
    rating   integer      NOT NULL,
    comment  text         NULL
);

--Попробуем добавить отзыв к книге, айдишника которого не существует (foreign key не даст)
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (777, 'Петя', 9, 'отличная книга');

INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (1, 'Петя', 10, 'отличная книга');

select *
from reviews;

INSERT INTO reviews(book_id, reviewer, rating)
VALUES (1, 'Кирилл', 9);
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (3, 'Петя', 7, 'ок');
INSERT INTO reviews(book_id, reviewer, rating, comment)
VALUES (6, 'Иннокентий', 2, 'не понравилась');

--Достать только те записи reviews, у которых comment != null
select *
from reviews
where comment is not null;

--Посчитать сколько всего записей в reviews. Назвать столбец Количество отзывов
select count(*) as "Количество отзывов"
from reviews;

--Узнать количество уникальных id книг, по которым были оставлены отзывы:
select count(distinct book_id)
from reviews;

--Вывести сколько review по каждой id книги
select count(*), book_id
from reviews
group by book_id;

--Вывести все значения по books и по reviews (объединение столбцов результатов)
select *
from books b,
     reviews r
where b.id = r.book_id;

select *
from books b inner join reviews r on b.id = r.book_id;

--А через left join добъемся вывода книги, для которой нет отзывов
select *
from books b left join reviews r on b.id = r.book_id;
--where r.id is null;

--Вычислить среднюю оценку по каждой книге. Вывести столбцы Оценка, Название книги.
select avg(r.rating) as Оценка, b.title as "Название книги"
from reviews r inner join books b on b.id = r.book_id
group by b.title;
