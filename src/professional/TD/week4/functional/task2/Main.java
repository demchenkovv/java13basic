package professional.TD.week4.functional.task2;

/*
Задача 2
С помощью функционального интерфейса выполнить подсчет квадрата числа:
 */
public class Main {
    public static void main(String[] args) {
        Square square = new Square() {
            @Override
            public int calculate(int x) {
                return x * x;
            }
        };
        
        Square square1 = x -> x * x;
//        Square square1 = (int x) -> x * x;
        
        
        int value = 3;
        System.out.println(square.calculate(value));
        System.out.println(square1.calculate(value));
    }
    
}
