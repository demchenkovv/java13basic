package professional.TD.week2.task7;
/*
Простая задача: сохранить в мапе три элемента (1, “first”).
Вывести элемент значение по ключу 2
 */

/** Map - единственная структура данных, которая хранит ключ и значение.
 * НЕ является коллекцией.
 * Ключ ВСЕГДА уникален. */
import java.util.HashMap;
import java.util.Map;

public class MapSave {
    public static void main(String[] args) {
        Map <Integer, String> hm1 = new HashMap<>();
        hm1.put(1, "one");
        hm1.put(2, "two");
        hm1.put(3, "three");

        System.out.println("Элементы HashMap: " + hm1);
        System.out.println("Элементы элемента 2: " + hm1.get(2));
    }
}
