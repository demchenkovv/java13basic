package professional.TD.week2.task3;
/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором, т.е. пересекаются.
 */

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);

        System.out.println("set1: " + set1);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);

        System.out.println("set2: " + set2);

        // встроенный метод
        set1.retainAll(set2);

        // Вывод в консоль
        for (Integer elem : set1) {
            System.out.print(elem + " ");
        }
//        // Вывод в консоль через лямбду
//        set1.forEach(e -> System.out.print(e + " "));


        // Collections.disjoint - Метод, который вернет true/false, если есть хоть
        // один элемент в нашей текущей коллекции в другой коллекции
        // true - если нету совпадающих элементов
        // false - если есть совпадающие элементы
        boolean is = Collections.disjoint(set1, set2);
        System.out.println(is);

    }
}
