package professional.TD.week2.task1;
// 1
//public class Pair<T, U> {
//    public T first;
//    public U second;

// 2
//public class Pair<T> {
//    public T first;
//    public T second;

// 3
public class Pair<T extends String, U extends Number> {
    public T first;
    public U second;

    public void print() {
        System.out.println("First: " + first + "; second: " + second);
    }
}
