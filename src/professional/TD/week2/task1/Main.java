package professional.TD.week2.task1;
/*
Создать класс Pair, который умеет хранить два значения:
a) любого типа (T, U)
b) одинакового типа (T)
c) первый только String, второй только числовой <T extends String, U extends Number>
 */

public class Main {
    public static void main(String[] args) {
        // Задача 1
//        Pair<String, Integer> pair1 = new Pair<>();
//        pair1.first = "Test first field";
//        pair1.second = 123;
//        pair1.print();
//
//        Pair<Integer, Double> pair2 = new Pair<>();
//        pair2.first = 345;
//        pair2.second = 3.5;
//        pair2.print();

//        // Задача 2
//        Pair<String> pair = new Pair<>();
//        pair.first = "Hello";
//        pair.second = "world";
//        pair.print();

        // Задача 3
        Pair<String, Integer> pair = new Pair<>();
        pair.first = "Hello";
        pair.second = 4;
        pair.print();
    }
}
