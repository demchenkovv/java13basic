package professional.TD.week1.exceptions.task2;


import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileOutput {
    private static final String PKG_DIRECTORY = "\\Users\\HOME\\IdeaProjects\\Java13Basic\\src\\professional\\week1\\exceptions\\task2\\";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    
    public static void main(String[] args) {
//        Writer writer = null;
//        try {
//            writer = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME);
//            writer.write("Hello!");
//        }
//        catch (IOException e) {
//            throw new RuntimeException(e);
//        } finally {
//            writer.close();
//        }
        
        try (Writer wr = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME)) {
            wr.write("Hello!");
            System.out.println("Информация успешно внесена в файл " + OUTPUT_FILE_NAME);
        }
        catch (IOException e) {
            System.out.println("FileOutput#main!error: " + e.getMessage());
        }
    }
}
