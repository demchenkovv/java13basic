package week10.interf;

public interface Animal {
    
    String getColor();
    
    String getVoice();
    
    void eat(String food);
}
